<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 60)->index();
            $table->string('display_name', 60);
        });

        Schema::table('users', function ($table) {
            $table->foreign('user_type')->references('id')->on('users_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropForeign('users_user_type_foreign');
        });

        Schema::drop('users_types');
    }
}
