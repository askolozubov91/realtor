<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->index();
            $table->string('display_name', 255);
            $table->text('fields');
            $table->text('filters');
            $table->integer('parent_id')->nullable()->index();
            $table->integer('lft')->nullable()->index();
            $table->integer('rgt')->nullable()->index();
            $table->integer('depth')->nullable()->index();
        });

        Schema::create('ads_publications_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->index();
            $table->string('display_name', 255);
        });

        Schema::create('premium_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->index();
            $table->string('display_name', 255);
            $table->string('description', 255);
            $table->decimal('price',20,0)->unsigned()->default(0);
        });

        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->text('content');
            $table->integer('primary_type')->unsigned();
            $table->integer('primary_place')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('agency_id')->unsigned()->nullable();
            $table->integer('main_image_id')->unsigned()->nullable();
            $table->decimal('price',20,0)->unsigned()->nullable();
            $table->decimal('old_price',20,0)->unsigned()->nullable();
            $table->dateTime('upped_at')->nullable();
            $table->integer('publication_status_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('agency_id')->references('id')->on('agencies')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('publication_status_id')->references('id')->on('ads_publications_statuses')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('primary_type')->references('id')->on('ads_types')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('ads_premium_rates', function (Blueprint $table) {
            $table->integer('ad_id')->unsigned();
            $table->integer('premium_rate_id')->unsigned();
            $table->dateTime('off_datetime');

            $table->foreign('ad_id')->references('id')->on('ads')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('premium_rate_id')->references('id')->on('premium_rates')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['ad_id', 'premium_rate_id']);
        });

        Schema::create('ads_types_ads', function (Blueprint $table) {
            $table->integer('ad_id')->unsigned();
            $table->integer('type_id')->unsigned();

            $table->foreign('ad_id')->references('id')->on('ads')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('ads_types')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['ad_id', 'type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = "
            SET GROUP_CONCAT_MAX_LEN=10000;
            SET FOREIGN_KEY_CHECKS=0;
            SET @tbls = (SELECT GROUP_CONCAT(TABLE_NAME)
            FROM information_schema.TABLES
            WHERE TABLE_SCHEMA = '".DB::connection()->getDatabaseName()."'
            AND ( TABLE_NAME LIKE 'type_%' OR TABLE_NAME LIKE 'param_%' ));
            SET @delStmt = CONCAT('DROP TABLE ', @tbls);
            PREPARE stmt FROM @delStmt;
            EXECUTE stmt;
            DEALLOCATE PREPARE stmt;
            SET FOREIGN_KEY_CHECKS=1;
        ";
        DB::connection()->getPdo()->exec($sql);

        Schema::drop('ads_types_ads');
        Schema::drop('ads_premium_rates');
        Schema::drop('ads');
        Schema::drop('premium_rates');
        Schema::drop('ads_publications_statuses');
        Schema::drop('ads_types');
    }
}
