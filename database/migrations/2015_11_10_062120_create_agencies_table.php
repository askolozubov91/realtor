<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('logo_image_id')->unsigned()->nullable();
            $table->text('about');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('agencies_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agency_id')->unsigned()->nullable();
            $table->string('address', 255);
            $table->decimal('latitude', 19, 15);
            $table->decimal('longitude', 19, 15);
            $table->integer('primary_place')->unsigned();

            $table->foreign('agency_id')->references('id')->on('agencies')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('contacts_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('display_name', 100);
            $table->string('rules',255);
            $table->string('icon', 100);
        });

        Schema::create('agencies_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agency_id')->unsigned()->nullable();
            $table->integer('contact_type')->unsigned()->nullable();
            $table->string('value', 255);

            $table->foreign('agency_id')->references('id')->on('agencies')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('contact_type')->references('id')->on('contacts_types')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('users_agencies', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('agency_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('agency_id')->references('id')->on('agencies')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'agency_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agencies_addresses');
        Schema::drop('agencies_contacts');
        Schema::drop('contacts_types');
        Schema::drop('users_agencies');
        Schema::drop('agencies');
    }
}
