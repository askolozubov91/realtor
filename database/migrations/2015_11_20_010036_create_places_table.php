<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('places_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->index();
            $table->string('display_name', 255);
        });

        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->index();
            $table->string('display_name', 255);
            $table->integer('type_id')->unsigned();
            $table->integer('parent_id')->nullable()->index();
            $table->integer('lft')->nullable()->index();
            $table->integer('rgt')->nullable()->index();
            $table->integer('depth')->nullable()->index();

            $table->foreign('type_id')->references('id')->on('places_types')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('ads_places', function (Blueprint $table) {
            $table->integer('ad_id')->unsigned();
            $table->integer('place_id')->unsigned();

            $table->foreign('ad_id')->references('id')->on('ads')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('place_id')->references('id')->on('places')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['ad_id', 'place_id']);
        });

        Schema::create('agencies_places', function (Blueprint $table) {
            $table->integer('agency_id')->unsigned();
            $table->integer('place_id')->unsigned();

            $table->foreign('agency_id')->references('id')->on('agencies')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('place_id')->references('id')->on('places')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['agency_id', 'place_id']);
        });

        Schema::table('ads', function (Blueprint $table){
            $table->foreign('primary_place')->references('id')->on('places')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('agencies_addresses', function (Blueprint $table){
            $table->foreign('primary_place')->references('id')->on('places')
                ->onUpdate('cascade')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function (Blueprint $table){
            $table->dropForeign('ads_primary_place_foreign');
        });

        Schema::table('agencies_addresses', function (Blueprint $table){
            $table->dropForeign('agencies_addresses_primary_place_foreign');
        });

        Schema::drop('agencies_places');
        Schema::drop('ads_places');
        Schema::drop('places');
        Schema::drop('places_types');
    }
}
