<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 60);
            $table->string('email', 60)->index()->nullable();
            $table->decimal('phone',11,0)->index()->unsigned()->nullable();
            $table->string('password', 60);
            $table->integer('user_type')->unsigned();
            $table->string('company_name', 100)->nullable();
            $table->integer('agency_id')->index()->nullable()->unsigned();
            $table->decimal('money',10,2)->index()->default(0);
            $table->boolean('confirmed_email')->index()->default(0);
            $table->boolean('confirmed_phone')->index()->default(0);
            $table->boolean('send_notifications_email')->index()->default(0);
            $table->boolean('send_notifications_phone')->index()->default(0);
            $table->boolean('send_news')->index()->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
