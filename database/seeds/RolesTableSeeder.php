<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Roles
         */
        $owner = new Role();
        $owner->name         = 'owner';
        $owner->level        = 1;
        $owner->display_name = 'Владелец';
        $owner->description  = 'Может делать на сайте все, что угодно. В том числе назначать управляющих.';
        $owner->save();

        $manager = new Role();
        $manager->name         = 'manager';
        $manager->level        = 2;
        $manager->display_name = 'Управляющий';
        $manager->description  = 'Управляющий сайтом';
        $manager->save();

        $admin = new Role();
        $admin->name         = 'admin';
        $admin->level        = 3;
        $admin->display_name = 'Администратор';
        $admin->description  = 'Администратор сайта';
        $admin->save();

        $user = new Role();
        $user->name          = 'user';
        $user->level         = 4;
        $user->display_name  = 'Пользователь';
        $user->description   = 'Рядовой пользователь (эта роль назначается по-умолчанию новым пользователям)';
        $user->save();

        $locked = new Role();
        $locked->name          = 'locked';
        $locked->level         = 999;
        $locked->display_name  = 'Заблокированный';
        $locked->description   = 'Заблокированные пользователи могут авторизоваться в личном кабинете но не могуть использовать функционал сайта (для ботов и нарушителей правил сайта)';
        $locked->save();

        /**
         * Permissions
         */
        $login = new Permission();
        $login->name         = 'login';
        $login->display_name = 'Авторизация';
        $login->description  = 'Возможность авторизации пользователя';
        $login->save();

        $adminLogin = new Permission();
        $adminLogin->name         = 'adminLogin';
        $adminLogin->display_name = 'Доступ в админку';
        $adminLogin->description  = 'Возможность авторизоваться в админке';
        $adminLogin->save();

        $adminUsersControl = new Permission();
        $adminUsersControl->name         = 'adminUsersControl';
        $adminUsersControl->display_name = 'Управление пользователями';
        $adminUsersControl->description  = 'Возможность управления пользователями сайта';
        $adminUsersControl->save();

        $adminAdsControl = new Permission();
        $adminAdsControl->name         = 'adminAdsControl';
        $adminAdsControl->display_name = 'Управление объявлениями';
        $adminAdsControl->description  = 'Возможность управления объявлениями';
        $adminAdsControl->save();

        $adminAgenciesControl = new Permission();
        $adminAgenciesControl->name         = 'adminAgenciesControl';
        $adminAgenciesControl->display_name = 'Управление агентствами';
        $adminAgenciesControl->description  = 'Возможность управления агентствами';
        $adminAgenciesControl->save();

        $adminPagesControl = new Permission();
        $adminPagesControl->name         = 'adminPagesControl';
        $adminPagesControl->display_name = 'Управление страницами';
        $adminPagesControl->description  = 'Возможность управления текстовыми страницами';
        $adminPagesControl->save();

        $adminPostsControl = new Permission();
        $adminPostsControl->name         = 'adminPostsControl';
        $adminPostsControl->display_name = 'Управление статьями';
        $adminPostsControl->description  = 'Возможность управления статьями';
        $adminPostsControl->save();

        $adminSettingsControl = new Permission();
        $adminSettingsControl->name         = 'adminSettingsControl';
        $adminSettingsControl->display_name = 'Управление настройками';
        $adminSettingsControl->description  = 'Возможность управления настройками сайта';
        $adminSettingsControl->save();

        $createAds = new Permission();
        $createAds->name         = 'createAds';
        $createAds->display_name = 'Создание объявлений';
        $createAds->description  = 'Возможность создания объявлений на сайте';
        $createAds->save();


        /*
         * Attach permissions
         */
        $owner->attachPermissions([
            $login,
            $adminLogin,
            $adminUsersControl,
            $adminAdsControl,
            $adminAgenciesControl,
            $adminPagesControl,
            $adminPostsControl,
            $adminSettingsControl,
            $createAds,
        ]);

        $manager->attachPermissions([
            $login,
            $adminLogin,
            $adminUsersControl,
            $adminAdsControl,
            $adminAgenciesControl,
            $adminPagesControl,
            $adminPostsControl,
            $createAds,
        ]);

        $admin->attachPermissions([
            $login,
            $adminLogin,
            $adminAdsControl,
            $adminAgenciesControl,
            $createAds,
        ]);

        $user->attachPermissions([
            $login,
            $createAds,
        ]);

        $locked->attachPermissions([

        ]);
    }
}
