<?php

use Illuminate\Database\Seeder;
use App\AdType;

class AdsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * недвижимость
         */
        $realEstate = AdType::create([
            'name' => 'real_estate',
            'display_name' => 'Недвижимость',
            'fields' => json_encode([
                'address' => [
                    'type' => 'string',
                    'display_name' => 'Адрес',
                    'display_description' => '',
                    'rules' => 'max:255',
                    'editor' => 'text'
                ],
                'latitude' => [
                    'type' => 'decimal',
                    'type_params' => [19,15],
                    'display_name' => 'Широта',
                    'display_description' => '',
                    'rules' => '',
                    'editor' => false
                ],
                'longitude' => [
                    'type' => 'decimal',
                    'type_params' => [19,15],
                    'display_name' => 'Долгота',
                    'display_description' => '',
                    'rules' => '',
                    'editor' => false
                ],
            ]),
        ]);


            /*
             * недвижимость/квартиры
             */
            $apartments = $realEstate->children()->create([
                'name' => 'apartments',
                'display_name' => 'Квартиры',
                'fields' => json_encode([
                    'rooms' => [
                        'type' => 'string',
                        'values' => ['студии','1-комнатные','2-комнатные','3-комнатные','4-комнатные','5-комнатные','многокомнатные'],
                        'display_name' => 'Количество комнат',
                        'display_description' => 'Количество комнат в квартире',
                        'rules' => 'integer|exists:param_apartments_rooms,id',
                        'editor' => 'select',
                        'filter' => true,
                        'teaser' => true
                    ],
                    'area' => [
                        'type' => 'float',
                        'type_params' => [10,1],
                        'display_name' => 'Площадь м.кв.',
                        'display_description' => 'Площадь квартиры',
                        'rules' => 'numeric',
                        'editor' => 'text',
                        'filter' => true,
                        'teaser' => true
                    ],
                ]),
            ]);

                /*
                 * недвижимость/квартиры/Сдам
                 */
                $apartmentsRent = $apartments->children()->create([
                    'name' => 'apartments_rent',
                    'display_name' => 'Сдам',
                    'fields' => json_encode([
                        'house_material' => [
                            'type' => 'string',
                            'values' => ['панель','кирпич','монолит','блочный','деревянный'],
                            'display_name' => 'Тип дома',
                            'display_description' => '',
                            'rules' => 'integer|exists:param_apartments_rent_house_material,id',
                            'editor' => 'select',
                        ],
                        'level' => [
                            'type' => 'smallInteger',
                            'filter_type' => 'numeric',
                            'display_name' => 'Этаж',
                            'display_description' => 'На каком этаже находится квартира',
                            'rules' => 'integer|min:1|max:255',
                            'editor' => 'text',
                        ],
                        'house_level' => [
                            'type' => 'smallInteger',
                            'filter_type' => 'numeric',
                            'display_name' => 'Этажность дома',
                            'display_description' => 'Сколько этажей в доме',
                            'rules' => 'integer|min:1|max:255',
                            'editor' => 'text',
                        ],
                        'condition' => [
                            'type' => 'string',
                            'values' => ['Дизайнерский ремонт','Отличное','Хорошее', 'Соц. ремонт', 'Удовлетворительное', 'Требуется ремонт'],
                            'display_name' => 'Состояние',
                            'display_description' => 'Состояние квартиры',
                            'rules' => 'integer|exists:param_apartments_rent_condition,id',
                            'editor' => 'select',
                        ],
                        'children' => [
                            'type' => 'boolean',
                            'group' => 'Дополнительные условия',
                            'group_name' => 'additional_group',
                            'display_name' => 'Можно с детьми',
                            'display_description' => '',
                            'rules' => 'boolean',
                            'editor' => 'boolean',
                        ],
                        'pets' => [
                            'type' => 'boolean',
                            'group' => 'Дополнительные условия',
                            'group_name' => 'additional_group',
                            'display_name' => 'Можно с животными',
                            'display_description' => '',
                            'rules' => 'boolean',
                            'editor' => 'boolean',
                        ],
                        'smoke' => [
                            'type' => 'boolean',
                            'group' => 'Дополнительные условия',
                            'group_name' => 'additional_group',
                            'display_name' => 'Можно курить',
                            'display_description' => '',
                            'rules' => 'boolean',
                            'editor' => 'boolean',
                        ],
                        'tv' => [
                            'type' => 'boolean',
                            'group' => 'Бытовая техника',
                            'group_name' => 'house_tech_group',
                            'display_name' => 'Телевизор',
                            'display_description' => 'Наличие телевизора',
                            'rules' => 'boolean',
                            'editor' => 'boolean',
                        ],
                        'washer' => [
                            'type' => 'boolean',
                            'group' => 'Бытовая техника',
                            'group_name' => 'house_tech_group',
                            'display_name' => 'Стиральная машина',
                            'display_description' => 'Наличие стиральной машины',
                            'rules' => 'boolean',
                            'editor' => 'boolean',
                        ],
                        'conditioner' => [
                            'type' => 'boolean',
                            'group' => 'Бытовая техника',
                            'group_name' => 'house_tech_group',
                            'display_name' => 'Кондиционер',
                            'display_description' => 'Наличие кондиционера',
                            'rules' => 'boolean',
                            'editor' => 'boolean',
                        ],
                    ]),
                ]);

                    /*
                     * недвижимость/квартиры/Сдам/На длительный срок
                     */
                    $apartmentsRentLong = $apartmentsRent->children()->create([
                        'name' => 'apartments_rent_long',
                        'display_name' => 'На длительный срок',
                        'fields' => json_encode([
                            'price' => [
                                'type' => 'decimal',
                                'type_params' => [20,0],
                                'display_name' => 'Стоимость в месяц (руб.)',
                                'display_description' => 'Стоимость аренды в месяц',
                                'rules' => 'numeric',
                                'editor' => 'text',
                                'related' => 'price',
                                'filter' => true
                            ],
                        ]),
                    ]);

                    /*
                     * недвижимость/квартиры/Сдам/Посуточно
                     */
                    $apartmentsRentFast = $apartmentsRent->children()->create([
                        'name' => 'apartments_rent_fast',
                        'display_name' => 'Посуточно',
                        'fields' => json_encode([
                            'price' => [
                                'type' => 'decimal',
                                'type_params' => [20,0],
                                'display_name' => 'Стоимость в сутки (руб.)',
                                'display_description' => 'Стоимость аренды в сутки',
                                'rules' => 'numeric',
                                'editor' => 'text',
                                'related' => 'price',
                                'filter' => true
                            ],
                        ]),
                    ]);

                /*
                 * недвижимость/квартиры/Продам
                 */
                $apartmentsSale = $apartments->children()->create([
                    'name' => 'apartments_sale',
                    'display_name' => 'Продам',
                    'fields' => json_encode([]),
                ]);

                    /*
                     * недвижимость/квартиры/Продам/Вторичка
                     */
                    $apartmentsSaleResale = $apartmentsSale->children()->create([
                        'name' => 'apartments_sale_resale',
                        'display_name' => 'Вторичка',
                        'fields' => json_encode([]),
                    ]);

                    /*
                     * недвижимость/квартиры/Продам/Новостройка
                     */
                    $apartmentsSaleNew = $apartmentsSale->children()->create([
                        'name' => 'apartments_sale_new',
                        'display_name' => 'Новостройка',
                        'fields' => json_encode([]),
                    ]);

                /*
                 * недвижимость/квартиры/Куплю
                 */
                $apartmentsPurchase = $apartments->children()->create([
                    'name' => 'apartments_buy',
                    'display_name' => 'Куплю',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/квартиры/Сниму
                 */
                $apartmentsLease = $apartments->children()->create([
                    'name' => 'apartments_lease',
                    'display_name' => 'Сниму',
                    'fields' => json_encode([]),
                ]);




            /*
             * недвижимость/комнаты
             */
            $rooms = $realEstate->children()->create([
                'name' => 'rooms',
                'display_name' => 'Комнаты',
                'fields' => json_encode([]),
            ]);

                /*
                 * недвижимость/комнаты/Сдам
                 */
                $roomsRent = $rooms->children()->create([
                    'name' => 'rooms_rent',
                    'display_name' => 'Сдам',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/комнаты/Продам
                 */
                $roomsSale = $rooms->children()->create([
                    'name' => 'rooms_sale',
                    'display_name' => 'Продам',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/комнаты/Куплю
                 */
                $roomsPurchase = $rooms->children()->create([
                    'name' => 'rooms_purchase',
                    'display_name' => 'Куплю',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/комнаты/Сниму
                 */
                $roomsLease = $rooms->children()->create([
                    'name' => 'rooms_lease',
                    'display_name' => 'Сниму',
                    'fields' => json_encode([]),
                ]);

            /*
             * недвижимость/дома, коттеджи, дачи
             */
            $houses = $realEstate->children()->create([
                'name' => 'houses',
                'display_name' => 'Дома, коттеджи, дачи',
                'fields' => json_encode([]),
            ]);

                /*
                 * недвижимость/дома, коттеджи, дачи/Продам
                 */
                $housesSale = $houses->children()->create([
                    'name' => 'houses_sale',
                    'display_name' => 'Продам',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/дома, коттеджи, дачи/Сдам
                 */
                $housesRent = $houses->children()->create([
                    'name' => 'houses_rent',
                    'display_name' => 'Сдам',
                    'fields' => json_encode([]),
                ]);

                    /*
                     * недвижимость/дома, коттеджи, дачи/Сдам/На длительный срок
                     */
                    $housesRentLong = $housesRent->children()->create([
                        'name' => 'houses_rent_long',
                        'display_name' => 'На длительный срок',
                        'fields' => json_encode([]),
                    ]);

                    /*
                     * недвижимость/дома, коттеджи, дачи/Сдам/Посуточно
                     */
                    $housesRentFast = $housesRent->children()->create([
                        'name' => 'houses_rent_fast',
                        'display_name' => 'Посуточно',
                        'fields' => json_encode([]),
                    ]);


                /*
                 * недвижимость/дома, коттеджи, дачи/Куплю
                 */
                $housesPurchase = $houses->children()->create([
                    'name' => 'houses_purchase',
                    'display_name' => 'Куплю',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/дома, коттеджи, дачи/Сниму
                 */
                $housesLease = $houses->children()->create([
                    'name' => 'houses_lease',
                    'display_name' => 'Сниму',
                    'fields' => json_encode([]),
                ]);

            /*
             * недвижимость/Земельные участки
             */
            $land = $realEstate->children()->create([
                'name' => 'land',
                'display_name' => 'Земельные участки',
                'fields' => json_encode([]),
            ]);

                /*
                 * недвижимость/Земельные участки/Продам
                 */
                $landSale = $land->children()->create([
                    'name' => 'land_sale',
                    'display_name' => 'Продам',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/Земельные участки/Сдам
                 */
                $landRent = $land->children()->create([
                    'name' => 'land_rent',
                    'display_name' => 'Сдам',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/Земельные участки/Куплю
                 */
                $landPurchase = $land->children()->create([
                    'name' => 'land_purchase',
                    'display_name' => 'Куплю',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/Земельные участки/Сниму
                 */
                $landLease = $land->children()->create([
                    'name' => 'land_lease',
                    'display_name' => 'Сниму',
                    'fields' => json_encode([]),
                ]);


            /*
             * недвижимость/Гаражи и машиноместа
             */
            $garagesAndParkingPlaces = $realEstate->children()->create([
                'name' => 'garages_and_parking_places',
                'display_name' => 'Гаражи и машиноместа',
                'fields' => json_encode([]),
            ]);

                /*
                 * недвижимость/Гаражи и машиноместа/гаражи
                 */
                $garages = $garagesAndParkingPlaces->children()->create([
                    'name' => 'garages',
                    'display_name' => 'Гаражи',
                    'fields' => json_encode([]),
                ]);

                        /*
                         * недвижимость/Гаражи и машиноместа/гаражи/Продам
                         */
                        $garageSale = $garages->children()->create([
                            'name' => 'garage_sale',
                            'display_name' => 'Продам',
                            'fields' => json_encode([]),
                        ]);

                        /*
                         * недвижимость/Гаражи и машиноместа/гаражи/Сдам
                         */
                        $garageRent = $garages->children()->create([
                            'name' => 'garage_rent',
                            'display_name' => 'Сдам',
                            'fields' => json_encode([]),
                        ]);

                        /*
                         * недвижимость/Гаражи и машиноместа/гаражи/Куплю
                         */
                        $garagePurchase = $garages->children()->create([
                            'name' => 'garage_purchase',
                            'display_name' => 'Куплю',
                            'fields' => json_encode([]),
                        ]);

                        /*
                         * недвижимость/Гаражи и машиноместа/гаражи/Сниму
                         */
                        $garageLease = $garages->children()->create([
                            'name' => 'garage_lease',
                            'display_name' => 'Сниму',
                            'fields' => json_encode([]),
                        ]);

                /*
                 * недвижимость/Гаражи и машиноместа/машиноместа
                 */
                $parkingPlaces = $garagesAndParkingPlaces->children()->create([
                    'name' => 'parking_places',
                    'display_name' => 'Машиноместа',
                    'fields' => json_encode([]),
                ]);

                        /*
                         * недвижимость/Гаражи и машиноместа/машиноместа/Продам
                         */
                        $parkingPlaceSale = $parkingPlaces->children()->create([
                            'name' => 'parking_place_sale',
                            'display_name' => 'Продам',
                            'fields' => json_encode([]),
                        ]);

                        /*
                         * недвижимость/Гаражи и машиноместа/машиноместа/Сдам
                         */
                        $parkingPlaceRent = $parkingPlaces->children()->create([
                            'name' => 'parking_place_rent',
                            'display_name' => 'Сдам',
                            'fields' => json_encode([]),
                        ]);

                        /*
                         * недвижимость/Гаражи и машиноместа/машиноместа/Куплю
                         */
                        $parkingPlacePurchase = $parkingPlaces->children()->create([
                            'name' => 'parking_place_purchase',
                            'display_name' => 'Куплю',
                            'fields' => json_encode([]),
                        ]);

                        /*
                         * недвижимость/Гаражи и машиноместа/машиноместа/Сниму
                         */
                        $parkingPlaceLease = $parkingPlaces->children()->create([
                            'name' => 'parking_place_lease',
                            'display_name' => 'Сниму',
                            'fields' => json_encode([]),
                        ]);

            /*
             * недвижимость/Коммерческая недвижимость
             */
            $commercial = $realEstate->children()->create([
                'name' => 'commercial',
                'display_name' => 'Коммерческая недвижимость',
                'fields' => json_encode([]),
            ]);

                /*
                 * недвижимость/Коммерческая недвижимость/Продам
                 */
                $commercialSale = $commercial->children()->create([
                    'name' => 'commercial_sale',
                    'display_name' => 'Продам',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/Коммерческая недвижимость/Сдам
                 */
                $commercialRent = $commercial->children()->create([
                    'name' => 'commercial_rent',
                    'display_name' => 'Сдам',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/Коммерческая недвижимость/Куплю
                 */
                $commercialPurchase = $commercial->children()->create([
                    'name' => 'commercial_purchase',
                    'display_name' => 'Куплю',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/Коммерческая недвижимость/Сниму
                 */
                $commercialLease = $commercial->children()->create([
                    'name' => 'commercial_lease',
                    'display_name' => 'Сниму',
                    'fields' => json_encode([]),
                ]);

            /*
             * недвижимость/Недвижимость за рубежом
             */
            $overseas = $realEstate->children()->create([
                'name' => 'overseas',
                'display_name' => 'Недвижимость за рубежом',
                'fields' => json_encode([]),
            ]);

                /*
                * недвижимость/Недвижимость за рубежом/Продам
                */
                $overseasSale = $overseas->children()->create([
                    'name' => 'overseas_sale',
                    'display_name' => 'Продам',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/Недвижимость за рубежом/Сдам
                 */
                $overseasRent = $overseas->children()->create([
                    'name' => 'overseas_rent',
                    'display_name' => 'Сдам',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/Недвижимость за рубежом/Куплю
                 */
                $overseasPurchase = $overseas->children()->create([
                    'name' => 'overseas_purchase',
                    'display_name' => 'Куплю',
                    'fields' => json_encode([]),
                ]);

                /*
                 * недвижимость/Недвижимость за рубежом/Сниму
                 */
                $overseasLease = $overseas->children()->create([
                    'name' => 'overseas_lease',
                    'display_name' => 'Сниму',
                    'fields' => json_encode([]),
                ]);


        //Невижимость
        AdType::makeAdditionalFieldsTypeTable($realEstate);
            //квартиры
            AdType::makeAdditionalFieldsTypeTable($apartments);
                //сдам
                AdType::makeAdditionalFieldsTypeTable($apartmentsRent);
                    //на длительный срок
                    AdType::makeAdditionalFieldsTypeTable($apartmentsRentLong);
                    //посуточно
                    AdType::makeAdditionalFieldsTypeTable($apartmentsRentFast);
                //продам
                AdType::makeAdditionalFieldsTypeTable($apartmentsSale);
                    //вторичка
                    AdType::makeAdditionalFieldsTypeTable($apartmentsSaleResale);
                    //новостроки
                    AdType::makeAdditionalFieldsTypeTable($apartmentsSaleNew);
                //куплю
                AdType::makeAdditionalFieldsTypeTable($apartmentsPurchase);
                //сниму
                AdType::makeAdditionalFieldsTypeTable($apartmentsLease);

            //комнаты
            AdType::makeAdditionalFieldsTypeTable($rooms);
                //сдам
                AdType::makeAdditionalFieldsTypeTable($roomsRent);
                //продам
                AdType::makeAdditionalFieldsTypeTable($roomsSale);
                //куплю
                AdType::makeAdditionalFieldsTypeTable($roomsPurchase);
                //сниму
                AdType::makeAdditionalFieldsTypeTable($roomsLease);

            //дома, коттеджи, дачи
            AdType::makeAdditionalFieldsTypeTable($houses);
                //продам
                AdType::makeAdditionalFieldsTypeTable($housesSale);
                //сдам
                AdType::makeAdditionalFieldsTypeTable($housesRent);
                    //на длительный срок
                    AdType::makeAdditionalFieldsTypeTable($housesRentLong);
                    //посуточно
                    AdType::makeAdditionalFieldsTypeTable($housesRentFast);
                //куплю
                AdType::makeAdditionalFieldsTypeTable($housesPurchase);
                //сниму
                AdType::makeAdditionalFieldsTypeTable($housesLease);

            //Земельные участки
            AdType::makeAdditionalFieldsTypeTable($land);
                //продам
                AdType::makeAdditionalFieldsTypeTable($landSale);
                //сдам
                AdType::makeAdditionalFieldsTypeTable($landRent);
                //куплю
                AdType::makeAdditionalFieldsTypeTable($landPurchase);
                //сниму
                AdType::makeAdditionalFieldsTypeTable($landLease);

            //гаражи и машиноместа
            AdType::makeAdditionalFieldsTypeTable($garagesAndParkingPlaces);
                //гаражи
                AdType::makeAdditionalFieldsTypeTable($garages);
                    //продам
                    AdType::makeAdditionalFieldsTypeTable($garageSale);
                    //сдам
                    AdType::makeAdditionalFieldsTypeTable($garageRent);
                    //куплю
                    AdType::makeAdditionalFieldsTypeTable($garagePurchase);
                    //сниму
                    AdType::makeAdditionalFieldsTypeTable($garageLease);
                //машиноместа
                AdType::makeAdditionalFieldsTypeTable($parkingPlaces);
                    //продам
                    AdType::makeAdditionalFieldsTypeTable($parkingPlaceSale);
                    //сдам
                    AdType::makeAdditionalFieldsTypeTable($parkingPlaceRent);
                    //куплю
                    AdType::makeAdditionalFieldsTypeTable($parkingPlacePurchase);
                    //сниму
                    AdType::makeAdditionalFieldsTypeTable($parkingPlaceLease);

            //коммерческая недвижимость
            AdType::makeAdditionalFieldsTypeTable($commercial);
                //продам
                AdType::makeAdditionalFieldsTypeTable($commercialSale);
                //сдам
                AdType::makeAdditionalFieldsTypeTable($commercialRent);
                //куплю
                AdType::makeAdditionalFieldsTypeTable($commercialPurchase);
                //сниму
                AdType::makeAdditionalFieldsTypeTable($commercialLease);

            //недвижимость за рубежом
            AdType::makeAdditionalFieldsTypeTable($overseas);
                //продам
                AdType::makeAdditionalFieldsTypeTable($overseasSale);
                //сдам
                AdType::makeAdditionalFieldsTypeTable($overseasRent);
                //куплю
                AdType::makeAdditionalFieldsTypeTable($overseasPurchase);
                //сниму
                AdType::makeAdditionalFieldsTypeTable($overseasLease);
    }
}



