<?php

use Illuminate\Database\Seeder;
use App\PlaceType;
use App\Place;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * create places types
         */
        PlaceType::create([
            'name' => 'country',
            'display_name' => 'страна'
        ]);

        PlaceType::create([
            'name' => 'federal_district',
            'display_name' => 'федеральный округ'
        ]);

        PlaceType::create([
            'name' => 'region',
            'display_name' => 'регион'
        ]);

        PlaceType::create([
            'name' => 'city',
            'display_name' => 'город'
        ]);

        PlaceType::create([
            'name' => 'city_global_district',
            'display_name' => 'городской округ'
        ]);

        PlaceType::create([
            'name' => 'city_district',
            'display_name' => 'район города'
        ]);

        PlaceType::create([
            'name' => 'city_metro',
            'display_name' => 'станция метро'
        ]);

        /*
         * create places
         */

        $placesTypes = PlaceType::lists('id','name')->toArray();

        $russia = Place::create([
            'name' => 'russia',
            'display_name' => 'Россия',
            'type_id' => $placesTypes['country']
        ]);

        $DVFO = $russia->children()->create([
            'name' => 'dalnevostochniy-federalniy-okrug',
            'display_name' => 'Дальневосточный федеральный округ',
            'type_id' => $placesTypes['federal_district']
        ]);

        $khabarovskiyKray = $DVFO->children()->create([
            'name' => 'khabarovskiy-kray',
            'display_name' => 'Хабаровский край',
            'type_id' => $placesTypes['region']
        ]);

        $primorskiyKray = $DVFO->children()->create([
            'name' => 'primorskiy-kray',
            'display_name' => 'Приморский край',
            'type_id' => $placesTypes['region']
        ]);

        $khabarovsk = $khabarovskiyKray->children()->create([
            'name' => 'khabarovsk',
            'display_name' => 'Хабаровск',
            'type_id' => $placesTypes['city']
        ]);

        $komsomolsk = $khabarovskiyKray->children()->create([
            'name' => 'komsomolsk',
            'display_name' => 'Комсомольск-на-Амуре',
            'type_id' => $placesTypes['city']
        ]);

        $vladivostok = $primorskiyKray->children()->create([
            'name' => 'vladivostok',
            'display_name' => 'Владивосток',
            'type_id' => $placesTypes['city']
        ]);

        $centralniy = $vladivostok->children()->create([
            'name' => 'centralniy',
            'display_name' => 'Центральный район',
            'type_id' => $placesTypes['city_district']
        ]);

        $necentralniy = $vladivostok->children()->create([
            'name' => 'necentralniy',
            'display_name' => 'Не центральный район',
            'type_id' => $placesTypes['city_district']
        ]);

        $zheleznodorozhniy = $khabarovsk->children()->create([
            'name' => 'zheleznodorozhniy',
            'display_name' => 'Железнодорожный район',
            'type_id' => $placesTypes['city_district']
        ]);

        $industrialniy = $khabarovsk->children()->create([
            'name' => 'industrialniy',
            'display_name' => 'Индустриальный район',
            'type_id' => $placesTypes['city_district']
        ]);

        $kirovskiy = $khabarovsk->children()->create([
            'name' => 'kirovskiy',
            'display_name' => 'Кировский район',
            'type_id' => $placesTypes['city_district']
        ]);

        $krasnoflotskiy = $khabarovsk->children()->create([
            'name' => 'krasnoflotskiy',
            'display_name' => 'Краснофлотский район',
            'type_id' => $placesTypes['city_district']
        ]);

        $centralniy = $khabarovsk->children()->create([
            'name' => 'centralniy',
            'display_name' => 'Центральный район',
            'type_id' => $placesTypes['city_district']
        ]);
    }
}
