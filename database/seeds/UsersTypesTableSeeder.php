<?php

use Illuminate\Database\Seeder;
use App\UserType;

class UsersTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserType::create([
            'name' => 'owner',
            'display_name' => 'Собственник',
        ]);

        UserType::create([
            'name' => 'agent',
            'display_name' => 'Посредник',
        ]);
    }
}
