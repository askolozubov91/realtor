<?php

use Illuminate\Database\Seeder;
use App\Agency;
use App\User;
use App\ContactType;
use Faker\Factory as Faker;

class AgenciesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $phone = new ContactType();
        $phone->name = 'phone';
        $phone->display_name = 'телефон';
        $phone->icon = 'fa-phone';
        $phone->rules = 'required|string|max:30';
        $phone->save();

        $email = new ContactType();
        $email->name = 'email';
        $email->display_name = 'электронная почта';
        $email->icon = 'fa-at';
        $email->rules = 'required|email|max:50';
        $email->save();

        $site = new ContactType();
        $site->name = 'site';
        $site->display_name = 'сайт';
        $site->icon = 'fa-external-link';
        $site->rules = 'required|string|max:255';
        $site->save();

        $socVk = new ContactType();
        $socVk->name = 'soc_vk';
        $socVk->display_name = 'В контакте';
        $socVk->icon = 'fa-vk';
        $socVk->rules = 'required|string|max:255';
        $socVk->save();

        $socFb = new ContactType();
        $socFb->name = 'soc_fb';
        $socFb->display_name = 'Facebook';
        $socFb->icon = 'fa-facebook';
        $socFb->rules = 'required|string|max:255';
        $socFb->save();

        $socTw = new ContactType();
        $socTw->name = 'soc_tw';
        $socTw->display_name = 'Twitter';
        $socTw->icon = 'fa-twitter';
        $socTw->rules = 'required|string|max:255';
        $socTw->save();

        $socOk = new ContactType();
        $socOk->name = 'soc_ok';
        $socOk->display_name = 'Одноклассники';
        $socOk->icon = 'fa-odnoklassniki';
        $socOk->rules = 'required|string|max:255';
        $socOk->save();

        $socGPlus = new ContactType();
        $socGPlus->name = 'soc_gplus';
        $socGPlus->display_name = 'Google+';
        $socGPlus->icon = 'fa-google-plus';
        $socGPlus->rules = 'required|string|max:255';
        $socGPlus->save();

        $socInstagram = new ContactType();
        $socInstagram->name = 'soc_instagram';
        $socInstagram->display_name = 'Instagram';
        $socInstagram->icon = 'fa-instagram';
        $socInstagram->rules = 'required|string|max:255';
        $socInstagram->save();

        $faker = Faker::create('ru_RU');

        for($i=0; $i<150; $i++) {

            $user = User::orderByRaw("RAND()")->first();

            Agency::create([
                'name' => $faker->company,
            ])->users()->attach($user->id);
        }
    }
}
