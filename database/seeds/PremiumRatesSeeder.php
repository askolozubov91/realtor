<?php

use Illuminate\Database\Seeder;

class PremiumRatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colorMarker = new \App\AdPremiumRate();
        $colorMarker->name = 'color_marker';
        $colorMarker->display_name = 'Выделить цветом';
        $colorMarker->description = 'Объявление выделено цветом в списках.';
        $colorMarker->price = 100;
        $colorMarker->save();

        $publicationPremium = new \App\AdPremiumRate();
        $publicationPremium->name = 'publication_premium';
        $publicationPremium->display_name = 'Премиум размещение';
        $publicationPremium->description = 'Объявление размещается выше обычных объявлений.';
        $publicationPremium->price = 150;
        $publicationPremium->save();

        $publicationUp = new \App\AdPremiumRate();
        $publicationUp->name = 'publication_up';
        $publicationUp->display_name = 'Поднять объявление';
        $publicationUp->description = 'Объявление поднимается вначало списка.';
        $publicationUp->price = 50;
        $publicationUp->save();
    }
}
