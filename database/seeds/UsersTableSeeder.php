<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserType;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        $types = UserType::lists('id');

        User::createUser([
            'name'          => 'Just Test User',
            'company_name'  => 'Big-big Company LTD',
            'user_type'     => $types[rand(0, count($types) - 1)],
            'email'         => 'just_test_mail@mail.ru',
            'phone'         => '79241001020',
            'password'      => 'password'
        ], 'owner');

        $usersCount = 100;

        if(env('APP_ENV') == 'testing'){
            $usersCount = 10;
        }

        for($i=0; $i<$usersCount; $i++){

            $phone = '7' . rand(900,999) . rand(1000000,9999999);
            $email = $faker->email;

            if(User::where('email',$email)->orWhere('phone',$phone)->first()){
                continue;
            }

            User::createUser([
                'name'                      => $faker->name,
                'email'                     => $email,
                'phone'                     => $phone,
                'password'                  => $faker->password,
                'user_type'                 => $types[rand(0, count($types) - 1)],
                'company_name'              => $faker->company,
                'money'                     => $faker->randomFloat(2,0,10000),
                'confirmed_email'           => $faker->boolean(3),
                'confirmed_phone'           => $faker->boolean(3),
                'send_notifications_email'  => $faker->boolean(3),
                'send_notifications_phone'  => $faker->boolean(3),
                'send_news'                 => $faker->boolean(3),
            ], 'user');
        }
    }
}
