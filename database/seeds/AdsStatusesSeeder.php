<?php

use Illuminate\Database\Seeder;
use App\AdPublicationStatus;

class AdsStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdPublicationStatus::create([
            'name' => 'publicated',
            'display_name' =>'Опубликовано',
        ]);

        AdPublicationStatus::create([
            'name' => 'draft',
            'display_name' =>'Черновик',
        ]);
        AdPublicationStatus::create([
            'name' => 'moderation',
            'display_name' =>'На модерации',
        ]);
        AdPublicationStatus::create([
            'name' => 'rejected',
            'display_name' =>'Отклонено',
        ]);
        AdPublicationStatus::create([
            'name' => 'archive',
            'display_name' =>'Архив',
        ]);
    }
}
