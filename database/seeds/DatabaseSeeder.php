<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(RolesTableSeeder::class);
        $this->call(UsersTypesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AgenciesTableSeeder::class);

        $this->call(AdsStatusesSeeder::class);
        $this->call(AdsTypesTableSeeder::class);
        $this->call(PlacesTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(PremiumRatesSeeder::class);

        Model::reguard();
    }
}
