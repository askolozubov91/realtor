<?php

use Illuminate\Database\Seeder;
use App\Settings;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $siteEmail = new Settings();
        $siteEmail->name = 'site_email';
        $siteEmail->display_name = 'E-mail, с которого пользователям приходят сообщения.';
        $siteEmail->value = 'service@goodwork-studio.com';
        $siteEmail->save();

    }
}
