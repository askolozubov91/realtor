<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    protected $table = 'bookmarks';

    protected $fillable = [
        'user_id',
        'ad_id',
    ];

    public function ad(){
        return $this->hasOne('App\Ad', 'id', 'ad_id');
    }

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
