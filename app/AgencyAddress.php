<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyAddress extends Model
{
    protected $table = 'agencies_addresses';

    public $timestamps = false;

    protected $fillable = [
        'agency_id',
        'address',
        'latitude',
        'longitude',
        'primary_place',
    ];

    public function saveAddress($data){
        $this->fill($data)->save();
        return $this;
    }

}
