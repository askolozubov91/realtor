<?php

namespace App;

use App\Helpers\Traits\SlugTrait;
use Baum\Node;

class PostCategory extends Node
{
    use SlugTrait;

    protected $table = 'posts_categories';

    public $timestamps = false;

    private $categoriesArr = [];

    public $parents = [];

    public $slugBase = 'title';

    protected $fillable = [
        'title',
        'slug',
    ];

    public function saveCategory($data){
        if(!isset($data['slug'])){
            $data['slug'] = '';
        }
        $this->fill($data)->save();
        return $this;
    }

    public function oneLevelCategoriesList($categories, $arrayData = false, $offset = '---', $afterOffset = '')
    {
        foreach ($categories as $i => $category) {
            if($arrayData){
                $this->categoriesArr[$category->id]['id'] = $category->id;
                $this->categoriesArr[$category->id]['title'] = $category->title;
                $this->categoriesArr[$category->id]['slug'] = $category->slug;
                $this->categoriesArr[$category->id]['depth'] = $category->depth;
                $this->categoriesArr[$category->id]['offset'] = $offset;
                $this->categoriesArr[$category->id]['offset_string'] = str_repeat($offset,$category->depth);
                $this->categoriesArr[$category->id]['afterOffset'] = $afterOffset;

            }else{
                $this->categoriesArr[$category->id] = str_repeat($offset,$category->depth).' '.$afterOffset.' '.$category->title;
            }
            if (count($category->children)) {
                $this->oneLevelCategoriesList($category->children, $arrayData, $offset, $afterOffset);
            }
        }
    }

    public function getCategoriesHierarchy($mode = 'one_level_list'){
        $categories = $this->get()->toHierarchy();
        switch ($mode){
            case 'one_level_list':
                $this->oneLevelCategoriesList($categories);
                break;
            case 'space_offset_array':
                $this->oneLevelCategoriesList($categories, true, '&nbsp;');
                break;
        }
        return $this->categoriesArr;
    }

    private function getParentsRecursive($node, $parentObjects = false){
        $parent = $node->parent()->first();
        if(!empty($parent)){
            if($parentObjects){
                $this->parents[] = $parent;
            }else{
                $this->parents[] = $parent->id;
            }
            if(!$parent->isRoot()){
                $this->getParentsRecursive($parent, $parentObjects);
            }
        }
    }

    public function allParents($parentObjects = false){
        $this->getParentsRecursive($this,$parentObjects);
        return $this->parents;
    }
}
