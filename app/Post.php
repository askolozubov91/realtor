<?php

namespace App;

use App\Helpers\Traits\SlugTrait;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use SlugTrait;

    protected $table = 'posts';

    public $slugBase = 'title';

    protected $fillable = [
        'title',
        'slug',
        'image_id',
        'content',
    ];

    public function image(){
        return $this->hasOne('App\Image', 'id', 'image_id');
    }

    public function categories(){
        return $this->belongsToMany('App\PostCategory', 'posts_posts_categories', 'post_id', 'category_id');
    }

    public function scopeDefaultSelect($query){
        return $query->with('image')->with('categories');
    }

    public function savePost($data){
        if(!isset($data['slug'])){
            $data['slug'] = '';
        }
        if(empty($data['image_id'])){
            $data['image_id'] = null;
        }
        $this->fill($data)->save();
        return $this;
    }
}
