<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveSearch extends Model
{
    protected $table = 'save_search';

    protected $fillable = [
        'title',
        'search_string',
        'user_id',
    ];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
