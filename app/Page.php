<?php

namespace App;

use App\Helpers\Traits\SlugTrait;
use Baum\Node;
use App\Helpers\Traits;

class Page extends Node
{

    use SlugTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     *
     */
    protected $table = 'pages';

    private $pagesArr = [];

    public $parents = [];

    public $slugBase = 'title';

    /**
     * Fillable fields
     *
     * @var array
     *
     */
    protected $fillable = [
        'title',
        'slug',
        'content'
    ];

    public function savePage($data){
        if(!isset($data['slug'])){
            $data['slug'] = '';
        }
        $this->fill($data)->save();
        return $this;
    }

    public function oneLevelPagesList($pages, $arrayData = false, $offset = '---', $afterOffset = '')
    {
        foreach ($pages as $i => $page) {
            if($arrayData){
                $this->pagesArr[$page->id]['id'] = $page->id;
                $this->pagesArr[$page->id]['title'] = $page->title;
                $this->pagesArr[$page->id]['slug'] = $page->slug;
                $this->pagesArr[$page->id]['depth'] = $page->depth;
                $this->pagesArr[$page->id]['offset'] = $offset;
                $this->pagesArr[$page->id]['offset_string'] = str_repeat($offset,$page->depth);
                $this->pagesArr[$page->id]['afterOffset'] = $afterOffset;

            }else{
                $this->pagesArr[$page->id] = str_repeat($offset,$page->depth).' '.$afterOffset.' '.$page->title;
            }
            if (count($page->children)) {
                $this->oneLevelPagesList($page->children, $arrayData, $offset, $afterOffset);
            }
        }
    }

    public function getPagesHierarchy($mode = 'one_level_list'){
        $pages = $this->get()->toHierarchy();
        switch ($mode){
            case 'one_level_list':
                $this->oneLevelPagesList($pages);
                break;
            case 'space_offset_array':
                $this->oneLevelPagesList($pages, true, '&nbsp;');
                break;
        }
        return $this->pagesArr;
    }

    private function getParentsRecursive($node, $parentObjects = false){
        $parent = $node->parent()->first();
        if(!empty($parent)){
            if($parentObjects){
                $this->parents[] = $parent;
            }else{
                $this->parents[] = $parent->id;
            }
            if(!$parent->isRoot()){
                $this->getParentsRecursive($parent, $parentObjects);
            }
        }
    }

    public function allParents($parentObjects = false){
        $this->getParentsRecursive($this,$parentObjects);
        return $this->parents;
    }
}
