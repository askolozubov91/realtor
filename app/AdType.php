<?php

namespace App;

use Baum\Node;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AdType extends Node
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ads_types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public $parents = [];
    public $typeFields = [];
    public $fieldsGroups = [];

    private $typesArr = [];

    /**
     * The types that belong to the ads.
     */
    public function ads()
    {
        return $this->belongsToMany('App\Ad', 'ads_types_ads','type_id','ad_id');
    }

    public function adsCount()
    {
        return $this->ads()
            ->selectRaw('type_id, count(ad_id) as count')
            ->groupBy('type_id');
    }


    /**
     * Generate tables in DB for type
     *
     * @var App\AdType $type
     *
     * @return void
     */
    public static function makeAdditionalFieldsTypeTable($type){
        Schema::create('type_'.$type->name, function (Blueprint $table) use ($type) {
            $table->increments('id');
            $table->integer('ad_id')->unsigned();
            $table->foreign('ad_id')->references('id')->on('ads')
                ->onUpdate('cascade')->onDelete('cascade');

            $fields = json_decode($type->fields);
            foreach($fields as $nameField => $field){
                if(!empty($field->values)){

                    $paramSchema = 'param_'.$type->name.'_'.$nameField;

                    $tableField = call_user_func_array([$table, 'integer'], [$nameField]);
                    $tableField->unsigned()->nullable();

                    //create params schema
                    Schema::create($paramSchema, function (Blueprint $table) use ($field) {
                        $table->increments('id');
                        call_user_func_array([$table, $field->type], ['name']);
                    });

                    $table->foreign($nameField)->references('id')->on($paramSchema)
                        ->onUpdate('cascade')->onDelete('set null');

                    //write values in params schema
                    if(is_array($field->values)){
                        foreach($field->values as $value){
                            DB::table($paramSchema)->insert(['name' => $value]);
                        }
                    }

                    $typeFields = json_decode($type->fields, true);
                    $typeFields[$nameField]['table'] = $paramSchema;
                    $type->fields = json_encode($typeFields);
                    $type->save();

                }else{
                    $fieldParams = [];
                    if(!empty($field->type_params) and is_array($field->type_params)){
                        $fieldParams = $field->type_params;
                    }
                    $tableField = call_user_func_array([$table, $field->type], array_merge([$nameField],$fieldParams));
                    $tableField->nullable();
                }
            }
        });
    }

    public function getTypeFieldAllValues($params, $paramName){
        return DB::table($params[$paramName]['table'])->lists('name','id');
    }

    private function getParentsRecursive($node, $parentObjects = false){
        $parent = $node->parent()->first();
        if(!empty($parent)){
            if($parentObjects){
                $this->parents[] = $parent;
            }else{
                $this->parents[] = $parent->id;
            }
            if(!$parent->isRoot()){
                $this->getParentsRecursive($parent, $parentObjects);
            }
        }
    }

    public function getAllParents($parentObjects = false){
        $this->getParentsRecursive($this,$parentObjects);
        return $this;
    }


    public function oneLevelList($categories, $arrayData = false, $count = true, $offset = '---', $afterOffset = '')
    {
        foreach ($categories as $i => $type) {
            if($arrayData){
                $this->typesArr[$type->id]['id'] = $type->id;
                $this->typesArr[$type->id]['name'] = $type->name;
                $this->typesArr[$type->id]['display_name'] = $type->display_name;
                $this->typesArr[$type->id]['fields'] = $type->fields;
                $this->typesArr[$type->id]['depth'] = $type->depth;
                $this->typesArr[$type->id]['offset'] = $offset;
                $this->typesArr[$type->id]['offset_string'] = str_repeat($offset,$type->depth);
                $this->typesArr[$type->id]['afterOffset'] = $afterOffset;
                if($count){
                    $this->typesArr[$type->id]['count'] = ((!empty($type->adsCount[0]))?$type->adsCount[0]->count:0);
                }
            }else{
                $this->typesArr[$type->id] = str_repeat($offset,$type->depth).' '.$afterOffset.' '.$type->display_name;
                if($count){
                    $this->typesArr[$type->id] .= ' ('.((!empty($type->adsCount[0]))?$type->adsCount[0]->count:0).')';
                }
            }
            if (count($type->children)) {
                $this->oneLevelList($type->children, $arrayData, $count, $offset, $afterOffset);
            }
        }
    }

    public function getHierarchy($mode = 'one_level_list'){
        $types = $this->with('adsCount')->get()->toHierarchy();
        switch ($mode){
            case 'one_level_list':
                $this->oneLevelList($types);
                break;
            case 'space_offset_array':
                $this->oneLevelList($types, true, true, '&nbsp;');
                break;
        }
        return $this->typesArr;
    }


    public function getTypeFields(){
        $this->getAllParents(true);

        $this->typeFields = json_decode($this->fields,true);
        foreach($this->typeFields as $name => $data){
            $this->typeFields[$name]['type_table_name'] = 'type_'.$this->name;
        }
        foreach($this->parents as $parent){
            $fields = json_decode($parent->fields,true);
            foreach($fields as $name => $data){
                $fields[$name]['type_table_name'] = 'type_'.$parent->name;
            }
            $this->typeFields = array_merge($fields, $this->typeFields);
        }

        return $this->typeFields;
    }

    public function getFieldsGroups(){
        foreach($this->typeFields as $field){
            if(!empty($field['group']) and !in_array($field['group'], $this->fieldsGroups)){
                $this->fieldsGroups[] = $field['group'];
            }
        }
        return $this->fieldsGroups;
    }


}
