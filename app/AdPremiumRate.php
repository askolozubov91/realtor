<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPremiumRate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'premium_rates';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The ads that belong to the rate.
     */
    public function ads()
    {
        return $this->belongsToMany('App\Ad', 'ads_premium_rates', 'premium_rate_id', 'ad_id');
    }

    public static function getRateByName($name){
        return self::where('name', $name)->first();
    }
}
