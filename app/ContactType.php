<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactType extends Model
{
    protected $table = 'contacts_types';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'display_name',
        'rules',
        'icon',
    ];

    public function saveContactType($data){
        $this->fill($data)->save();
        return $this;
    }
}
