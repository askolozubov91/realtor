<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyContact extends Model
{
    protected $table = 'agencies_contacts';

    public $timestamps = false;

    protected $fillable = [
        'agency_id',
        'contact_type',
        'value',
    ];

    public function type(){
        return $this->hasOne('App\ContactType', 'id', 'contact_type');
    }

    public function saveContact($data){
        $this->fill($data)->save();
        return $this;
    }

}
