<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPublicationStatus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ads_publications_statuses';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the ads for the publication status.
     */
    public function ads()
    {
        return $this->hasMany('App\Ad','publication_status_id');
    }

    public function adsCount()
    {
        return $this->ads()
            ->selectRaw('publication_status_id, count(id) as count')
            ->groupBy('publication_status_id');
    }


    public function myAdsCount()
    {
        return $this->ads()->onlyMy()
            ->selectRaw('publication_status_id, count(id) as count')
            ->groupBy('publication_status_id');
    }

}
