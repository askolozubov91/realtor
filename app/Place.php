<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;

class Place extends Node
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'places';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    private $placesArr = [];
    public $parents = [];

    /**
     * Get the ads for the place.
     */
    public function ads()
    {
        return $this->belongsToMany('App\Ad', 'ads_places', 'place_id', 'ad_id');
    }

    public function adsCount()
    {
        return $this->ads()
            ->selectRaw('place_id, count(ad_id) as count')
            ->groupBy('place_id');
    }

    /**
     * Get the type for the place.
     */
    public function type()
    {
        return $this->belongsTo('App\PlaceType', 'type_id', 'id');
    }

    public function scopeTypeName($query, $typeName){
        return $query->whereHas('type', function ($q) use ($typeName) {
            $q->where('name', $typeName);
        });
    }

    private function getParentsRecursive($node, $parentObjects = false){
        $parent = $node->parent()->first();
        if(!empty($parent)){
            if($parentObjects){
                $this->parents[] = $parent;
            }else{
                $this->parents[] = $parent->id;
            }
            if(!$parent->isRoot()){
                $this->getParentsRecursive($parent, $parentObjects);
            }
        }
    }

    public function getAllParents($parentObjects = false){
        $this->getParentsRecursive($this,$parentObjects);
        return $this;
    }


    public function oneLevelList($places, $arrayData = false, $count = true, $offset = '---', $afterOffset = '')
    {
        foreach ($places as $i => $place) {
            if($arrayData){
                $this->placesArr[$place->id]['id'] = $place->id;
                $this->placesArr[$place->id]['name'] = $place->name;
                $this->placesArr[$place->id]['display_name'] = $place->display_name;
                $this->placesArr[$place->id]['type_id'] = $place->type_id;
                $this->placesArr[$place->id]['type_name'] = $place->type->name;
                $this->placesArr[$place->id]['type_display_name'] = $place->type->display_name;
                $this->placesArr[$place->id]['depth'] = $place->depth;
                $this->placesArr[$place->id]['offset'] = $offset;
                $this->placesArr[$place->id]['offset_string'] = str_repeat($offset,$place->depth);
                $this->placesArr[$place->id]['afterOffset'] = $afterOffset;
                if($count){
                    $this->placesArr[$place->id]['count'] = ((!empty($place->adsCount[0]))?$place->adsCount[0]->count:0);
                }
            }else{
                $this->placesArr[$place->id] = str_repeat($offset,$place->depth).' '.$afterOffset.' '.$place->display_name;
                if($count){
                    $this->placesArr[$place->id] .= ' ('.((!empty($place->adsCount[0]))?$place->adsCount[0]->count:0).')';
                }
            }
            if (count($place->children)) {
                $this->oneLevelList($place->children, $arrayData, $count, $offset, $afterOffset);
            }
        }
    }

    public function getHierarchy($mode = 'one_level_list', $adsCount = true){
        $places = $this->with('type')->with('adsCount')->get()->toHierarchy();
        switch ($mode){
            case 'one_level_list':
                $this->oneLevelList($places, false, $adsCount);
                break;
            case 'space_offset_array':
                $this->oneLevelList($places, true, $adsCount, '&nbsp;');
                break;
        }
        return $this->placesArr;
    }
}
