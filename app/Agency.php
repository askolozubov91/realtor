<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agencies';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    protected $fillable = [
        'name',
        'logo_image_id',
        'about',
    ];

    /**
     * The users that belong to the agency.
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'users_agencies', 'agency_id', 'user_id');
    }

    public function places()
    {
        return $this->belongsToMany('App\Place', 'agencies_places', 'agency_id', 'place_id');
    }

    public function contacts(){
        return $this->hasMany('App\AgencyContact', 'agency_id');
    }

    public function addresses(){
        return $this->hasMany('App\AgencyAddress', 'agency_id');
    }

    public function ads(){
        return $this->hasMany('App\Ad', 'agency_id');
    }

    public function ads_count()
    {
        return $this->ads()
            ->selectRaw('agency_id, count(id) as count')
            ->groupBy('agency_id');
    }

    public function image(){
        return $this->hasOne('App\Image', 'id', 'logo_image_id');
    }

    public function scopeWithDefaultRelations($query)
    {
        return $query->with('users')->with('places')->with('addresses')->with('contacts')->with('contacts.type')->with('image')->with('ads_count');
    }

    public function saveAgency($data){

        //сохраняем основные данные агентства
        $this->fill($data)->save();

        //получаем местоположения и сохраняем адреса
        $agencyPlaces = [];
        $agencyAddresses = [];


        if(!empty($data['places']) and is_array($data['places']) and !empty($data['places'][0]['selected_place'])){
            AgencyAddress::where('agency_id',$this->id)->delete();
            foreach($data['places'] as $place){
                $agencyPlaces[] = $place['primary_place'];
                $places = Place::where('id',$place['primary_place'])->first()->getAllParents();
                $agencyPlaces = array_merge($agencyPlaces, $places->parents);

                $address = new AgencyAddress();
                $agencyAddresses[] = $address->saveAddress(array_merge($place, ['agency_id' => $this->id]));
            }
        }

        //привязываем местоположения к агентству
        $this->places()->sync($agencyPlaces);

        //привязываем пользователей к агентству
        if(!empty($data['users']) and is_array($data['users'])){
            //привязываем пользователей к агентству
            $this->users()->sync($data['users']);
        }

        //сохраняем контакты агентства
        if(!empty($data['contacts']) and is_array($data['contacts'])){
            AgencyContact::where('agency_id',$this->id)->delete();
            foreach($data['contacts'] as $contact){
                $cont = new AgencyContact();
                $cont->saveContact(array_merge($contact, ['agency_id' => $this->id]));
            }
        }

    }

}