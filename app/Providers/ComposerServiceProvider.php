<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('backend.ads.parts.places_list', 'App\Http\Composers\PlacesListComposer');
        view()->composer('backend.ads.parts.categories_list', 'App\Http\Composers\TypesListComposer');
        view()->composer('frontend.agencies.parts.edit_agency', 'App\Http\Composers\AgencyComposer');
        view()->composer('frontend.parts.select_city_modal', 'App\Http\Composers\RegionsComposer');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}