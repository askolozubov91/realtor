<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\SMS;

class SMSServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Helpers\Contracts\SMSContract', function(){
            return new SMS();
        });
    }
}