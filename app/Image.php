<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    protected $fillable = [
        'path',
        'name',
        'user_id',
    ];

    public function saveImage($data){
        $this->fill($data)->save();
        return $this;
    }
}
