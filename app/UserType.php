<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'name',
        'display_name',
    ];


    /**
     * Users relationship
     *
     * @return
     */
    public function users()
    {
        return $this->hasMany('App\Users', 'id', 'user_type');
    }

}