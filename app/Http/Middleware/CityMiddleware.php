<?php

namespace App\Http\Middleware;

use App\Place;
use Closure;
use Illuminate\Support\Facades\App;

class CityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $host = explode('.', $request->getHost());
        $cityName = 'moscow';
        if(is_array($host) and isset($host[2])){
            $cityName =  $host[0];
        }

        $city = Place::where('name', $cityName)->with('parent')->first();
        if(empty($city->id)){
            App::abort(404);
        }
        $city->getAllParents();
        $request->attributes->add(['globalCity' => $city]);
        return $next($request);
    }
}
