<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/site-control/users/search',
        '/site-control/agencies/search',
        '/site-control/images/image-upload-to-editor',
        '/site-control/images/gallery',
        '/images/upload'
    ];

    public function handle($request, Closure $next)
    {
        if ('testing' !== app()->environment())
        {
            return parent::handle($request, $next);
        }

        return $next($request);
    }
}
