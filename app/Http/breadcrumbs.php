<?php

// Home
Breadcrumbs::register('getHome', function($breadcrumbs)
{
    $breadcrumbs->push('главная', route('getHome'));
});

// Home > About
Breadcrumbs::register('getProfile', function($breadcrumbs)
{
    $breadcrumbs->parent('getHome');
    $breadcrumbs->push('Личный кабинет', route('getProfile'));
});


Breadcrumbs::register('getCreateAd', function($breadcrumbs)
{
    $breadcrumbs->parent('getHome');
    $breadcrumbs->push('новое объявление', route('getCreateAd'));
});

Breadcrumbs::register('getCatalog', function($breadcrumbs, $type)
{
    $breadcrumbs->parent('getHome');
    $type->parents = array_reverse($type->parents);
    foreach($type->parents as $parent){
        $breadcrumbs->push($parent->display_name, route('getCatalog', $parent->name));
    }
    $breadcrumbs->push($type->display_name, route('getCatalog', $type->name));
});

Breadcrumbs::register('getAgenciesCatalog', function($breadcrumbs)
{
    $breadcrumbs->parent('getHome');
    $breadcrumbs->push('Агентства', route('getAgenciesCatalog'));
});

Breadcrumbs::register('getAgencySingle', function($breadcrumbs, $agency_title)
{
    $breadcrumbs->parent('getHome');
    $breadcrumbs->push('Агентства', route('getAgenciesCatalog'));
    $breadcrumbs->push($agency_title);
});