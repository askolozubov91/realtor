<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['city']], function() {
    Route::get('/', [
        'as' => 'getHome', 'uses' => 'Pages\HomePageController@getHome'
    ]);

    Route::get('/auth-user', [
        'as' => 'getAuthUser', 'uses' => 'Auth\AuthController@getAuthUser'
    ]);

    Route::get('/confirm-email/{id}/{token}', [
        'as' => 'getConfirmEmail', 'uses' => 'Profile\ProfileController@getConfirmEmail'
    ])->where('id', '[0-9]+');


    Route::group(['prefix' => 'places'], function () {

        Route::get('/by-type-{typeName}', [
            'as' => 'getPlacesByType', 'uses' => 'Places\PlacesController@getPlacesByType'
        ])->where('typeName', '[0-9A-z\-]+');

        Route::get('/by-parent-id', [
            'as' => 'getPlacesByParentId', 'uses' => 'Places\PlacesController@getPlacesByParentId'
        ]);

    });


    Route::group(['prefix' => 'offers'], function () {

        Route::post('/send-ad-response', [
            'as' => 'postSendSingleAdForm', 'uses' => 'Ads\AdsController@postSendSingleAdForm'
        ]);

        Route::get('/single-{id}', [
            'as' => 'getSingle', 'uses' => 'Ads\AdsController@getSingle'
        ])->where('id', '[0-9]+');

        Route::get('/catalog-{typeName}', [
            'as' => 'getCatalog', 'uses' => 'Ads\AdsController@getCatalog'
        ])->where('typeName', '[0-9A-z\-\_]+');

        Route::get('/type-{id}-fields', [
            'as' => 'getAdTypeFields', 'uses' => 'Ads\AdsController@getAdTypeFields'
        ])->where('id', '[0-9]+');

        Route::get('/create', [
            'as' => 'getCreateAd', 'uses' => 'Ads\AdsController@getEditAd'
        ]);

        Route::get('/get-phone-{id}', [
            'as' => 'getAdPhone', 'uses' => 'Ads\AdsController@getAdPhone'
        ])->where('id', '[0-9]+');

        Route::post('/save-search', [
            'as' => 'postSaveSearch', 'uses' => 'Ads\AdsController@postSaveSearch'
        ]);

        Route::group(['middleware' => ['auth', 'permission:login']], function () {

            Route::get('/edit-{id}', [
                'as' => 'getEditAd', 'uses' => 'Ads\AdsController@getEditAd'
            ])->where('id', '[0-9]+');

            Route::post('/create', [
                'as' => 'postCreateAd', 'uses' => 'Ads\AdsController@postSaveAd'
            ]);

            Route::post('/save-{id}', [
                'as' => 'postSaveAd', 'uses' => 'Ads\AdsController@postSaveAd'
            ])->where('id', '[0-9]+');

            Route::get('/{id}-set-status-{statusName}', [
                'as' => 'getAdStatus', 'uses' => 'Ads\AdsController@getChangeStatus'
            ])->where('id', '[0-9]+')
                ->where('statusName', 'draft|archive');

            Route::get('/add-bookmark-{id}', [
                'as' => 'getAddBookmark', 'uses' => 'Ads\AdsController@getAddToBookmark'
            ])->where('id', '[0-9]+');

            Route::get('/delete-bookmark-{id}', [
                'as' => 'getDeleteBookmark', 'uses' => 'Ads\AdsController@getDeleteBookmark'
            ])->where('id', '[0-9]+');

        });

    });

    Route::group(['prefix' => 'agencies'], function () {
        
        Route::get('/catalog', [
            'as' => 'getAgenciesCatalog', 'uses' => 'Agencies\AgenciesController@getCatalog'
        ]);
        
        Route::get('/single-{id}', [
            'as' => 'getAgencySingle', 'uses' => 'Agencies\AgenciesController@getSingle'
        ])->where('id', '[0-9]+');

        Route::group(['middleware' => ['auth', 'permission:login']], function () {

            Route::post('/create', [
                'as' => 'getCreateAgency', 'uses' => 'Agencies\AgenciesController@getEditAgency'
            ]);

            Route::post('/update-{id}', [
                'as' => 'getUpdateAgency', 'uses' => 'Agencies\AgenciesController@getEditAgency'
            ])->where('id', '[0-9]+');

        });

    });

    Route::group(['prefix' => 'payment', 'middleware' => ['auth', 'permission:login']], function () {

        Route::get('', [
            'as' => 'startPayment', 'uses' => 'Payment\PaymentController@getStartPayment'
        ]);

    });


    Route::group(['middleware' => 'guest'], function () {

        Route::get('/login', [
            'as' => 'getLogIn', 'uses' => 'Auth\AuthController@getLogIn'
        ]);

        Route::post('/login', [
            'as' => 'postLogIn', 'uses' => 'Auth\AuthController@postLogIn'
        ]);

        Route::get('/sign-up', [
            'as' => 'getSignUp', 'uses' => 'Auth\AuthController@getSignUp'
        ]);

        Route::post('/sign-up', [
            'as' => 'postSignUp', 'uses' => 'Auth\AuthController@postSignUp'
        ]);

    });


    Route::group(['middleware' => ['auth']], function () {

        Route::get('/logout', [
            'as' => 'getLogOut', 'uses' => 'Auth\AuthController@getLogOut'
        ]);

        Route::post('/confirm-contacts', [
            'as' => 'postConfirmContacts', 'uses' => 'Auth\AuthController@postConfirmContacts'
        ]);

        Route::get('/send-confirm-email', [
            'as' => 'getSendConfirmEmail', 'uses' => 'Auth\AuthController@getSendConfirmEmail'
        ]);

        Route::get('/send-confirm-phone', [
            'as' => 'getSendConfirmPhone', 'uses' => 'Auth\AuthController@getSendConfirmPhone'
        ]);

        Route::group(['middleware' => ['permission:login']], function () {

            Route::group(['prefix' => 'profile'], function () {

                Route::get('/', [
                    'as' => 'getProfile', 'uses' => 'Profile\ProfileController@getProfile'
                ]);

                Route::get('/offers/{statusName?}', [
                    'as' => 'getProfileAds', 'uses' => 'Profile\ProfileController@getProfileAds'
                ])->where('statusName', '[A-z]+');

                Route::get('/settings', [
                    'as' => 'getProfileSettings', 'uses' => 'Profile\ProfileController@getProfileSettings'
                ]);

                Route::post('/settings', [
                    'as' => 'postUpdateUser', 'uses' => 'Profile\ProfileController@postUpdateUser'
                ]);

                Route::get('/settings/agency-{id?}', [
                    'as' => 'getProfileAgencySettings', 'uses' => 'Profile\ProfileController@getProfileAgencySettings'
                ])->where('id', '[0-9]+');

                Route::get('/settings/agency-{id?}-delete', [
                    'as' => 'getProfileAgencyDelete', 'uses' => 'Profile\ProfileController@getDeleteAgency'
                ])->where('id', '[0-9]+');

                Route::get('/bookmarks', [
                    'as' => 'getProfileBookmarks', 'uses' => 'Profile\ProfileController@getProfileBookmarks'
                ]);

                Route::get('/saved-search', [
                    'as' => 'getProfileSavedSearch', 'uses' => 'Profile\ProfileController@getProfileSavedSearch'
                ]);

                Route::get('/saved-search/delete-{id}', [
                    'as' => 'getDeleteSavedSearch', 'uses' => 'Profile\ProfileController@getDeleteSavedSearch'
                ])->where('id', '[0-9]+');

            });

            Route::group(['prefix' => 'images'], function () {

                Route::post('/upload', [
                    'as' => 'postUploadImage', 'uses' => 'Images\ImagesController@postUploadImage'
                ]);

                Route::get('/delete/{id?}', [
                    'as' => 'getImageDelete', 'uses' => 'Images\ImagesController@getImageDelete'
                ])->where('id', '[0-9]+');

            });

            Route::group(['middleware' => ['permission:adminLogin'], 'prefix' => 'site-control'], function () {

                Route::get('/', [
                    'as' => 'getAdminCp', 'uses' => 'Admin\DashboardAdminController@getDashboard'
                ]);

                Route::group(['prefix' => 'images'], function () {

                    Route::post('/upload', [
                        'as' => 'postAdminUploadImage', 'uses' => 'Images\ImagesAdminController@postUploadImage'
                    ]);

                    Route::get('/delete/{id?}', [
                        'as' => 'getAdminImageDelete', 'uses' => 'Images\ImagesAdminController@getImageDelete'
                    ])->where('id', '[0-9]+');

                    Route::get('/gallery', [
                        'as' => 'postAdminImagesGallery', 'uses' => 'Images\ImagesAdminController@getImagesGallery'
                    ]);

                    Route::post('/image-upload-to-editor', [
                        'as' => 'postAdminPostImageUpload', 'uses' => 'Images\ImagesAdminController@postUploadImageToEditor'
                    ]);

                });


                Route::group(['prefix' => 'users'], function () {

                    Route::post('/search', [
                        'as' => 'postAdminSearchUsers', 'uses' => 'Users\UsersAdminController@postSearchUsers'
                    ]);

                    Route::group(['middleware' => ['permission:adminUsersControl']], function () {

                        Route::get('/', [
                            'as' => 'getAdminUsers', 'uses' => 'Users\UsersAdminController@getUsers'
                        ]);

                        Route::get('/deleted', [
                            'as' => 'getAdminUsersDeleted', 'uses' => 'Users\UsersAdminController@getDeletedUsers'
                        ]);

                        Route::get('/datatable/{type?}', [
                            'as' => 'getAdminUsersDatatable', 'uses' => 'Users\UsersAdminController@getUsersDatatable'
                        ])->where(['type' => 'deleted']);

                        Route::get('/{id}', [
                            'as' => 'getAdminUser', 'uses' => 'Users\UsersAdminController@getUser'
                        ]);

                        Route::get('/{id}/delete', [
                            'as' => 'getAdminDeleteUser', 'uses' => 'Users\UsersAdminController@getDeleteUser'
                        ])->where('id', '[0-9]+');

                        Route::get('/{id}/delete-forever', [
                            'as' => 'getAdminDeleteUserForever', 'uses' => 'Users\UsersAdminController@getDeleteUserForever'
                        ])->where('id', '[0-9]+');

                        Route::get('/{id}/restore', [
                            'as' => 'getAdminRestoreUser', 'uses' => 'Users\UsersAdminController@getRestoreUser'
                        ])->where('id', '[0-9]+');

                        Route::post('/{id}/update', [
                            'as' => 'postAdminUpdateUser', 'uses' => 'Users\UsersAdminController@postUpdateUser'
                        ])->where('id', '[0-9]+');

                        Route::post('/selected-action', [
                            'as' => 'postAdminUsersSelectedAction', 'uses' => 'Users\UsersAdminController@postUserSelectedAction'
                        ]);

                    });

                });

                Route::group(['prefix' => 'agencies'], function () {

                    Route::post('/search', [
                        'as' => 'postAdminSearchAgencies', 'uses' => 'Agencies\AgenciesAdminController@postSearchAgencies'
                    ]);


                    Route::group(['middleware' => ['permission:adminAgenciesControl']], function () {

                        Route::get('/', [
                            'as' => 'getAdminAgencies', 'uses' => 'Agencies\AgenciesAdminController@getAgencies'
                        ]);

                        Route::get('/create', [
                            'as' => 'getAdminCreateAgency', 'uses' => 'Agencies\AgenciesAdminController@getAgency'
                        ]);

                        Route::get('/{id}', [
                            'as' => 'getAdminAgency', 'uses' => 'Agencies\AgenciesAdminController@getAgency'
                        ])->where('id', '[0-9]+');

                        Route::post('/save/{id?}', [
                            'as' => 'postAdminSaveAgency', 'uses' => 'Agencies\AgenciesAdminController@postSaveAgency'
                        ])->where('id', '[0-9]+');

                        Route::get('/datatable/{type?}', [
                            'as' => 'getAdminAgenciesDatatable', 'uses' => 'Agencies\AgenciesAdminController@getAgenciesDatatable'
                        ])->where(['type' => 'deleted']);

                        Route::get('/{id}/delete', [
                            'as' => 'getAdminDeleteAgency', 'uses' => 'Agencies\AgenciesAdminController@getDeleteAgency'
                        ])->where('id', '[0-9]+');

                        Route::post('/selected-action', [
                            'as' => 'postAdminAgenciesSelectedAction', 'uses' => 'Agencies\AgenciesAdminController@postAgenciesSelectedAction'
                        ]);

                        Route::get('/deleted', [
                            'as' => 'getAdminAgenciesDeleted', 'uses' => 'Agencies\AgenciesAdminController@getDeletedAgencies'
                        ]);

                        Route::get('/{id}/restore', [
                            'as' => 'getAdminRestoreAgency', 'uses' => 'Agencies\AgenciesAdminController@getRestoreAgency'
                        ])->where('id', '[0-9]+');

                        Route::get('/{id}/delete-forever', [
                            'as' => 'getAdminDeleteAgencyForever', 'uses' => 'Agencies\AgenciesAdminController@getDeleteAgencyForever'
                        ])->where('id', '[0-9]+');

                    });
                });

                Route::group(['middleware' => ['permission:adminAdsControl'], 'prefix' => 'offers'], function () {

                    Route::get('/', [
                        'as' => 'getAdminAds', 'uses' => 'Ads\AdsAdminController@getAds'
                    ]);

                    Route::get('/types-control/{id?}', [
                        'as' => 'getAdminAdsTypes', 'uses' => 'Ads\AdsTypesAdminController@getAdsTypes'
                    ])->where('id', '[0-9]+');

                    Route::get('/datatable/{type?}', [
                        'as' => 'getAdminAdsDatatable', 'uses' => 'Ads\AdsAdminController@getAdsDatatable'
                    ])->where(['type' => 'deleted']);

                    Route::get('/create-{typeId}', [
                        'as' => 'getAdminCreateAd', 'uses' => 'Ads\AdsAdminController@getAd'
                    ])->where('typeId', '[0-9]+');

                    Route::get('/{id}', [
                        'as' => 'getAdminAd', 'uses' => 'Ads\AdsAdminController@getAdSingle'
                    ])->where('id', '[0-9]+');

                    Route::post('/save-{typeId}/{id?}', [
                        'as' => 'postAdminSaveAd', 'uses' => 'Ads\AdsAdminController@postSaveAd'
                    ])
                        ->where('typeId', '[0-9]+')
                        ->where('id', '[0-9]+');

                    Route::post('/selected-action', [
                        'as' => 'postAdminAdsSelectedAction', 'uses' => 'Ads\AdsAdminController@postAdsSelectedAction'
                    ]);

                    Route::get('/{id}/delete', [
                        'as' => 'getAdminDeleteAd', 'uses' => 'Ads\AdsAdminController@getDeleteAd'
                    ])->where('id', '[0-9]+');

                    Route::get('/deleted', [
                        'as' => 'getAdminAdsDeleted', 'uses' => 'Ads\AdsAdminController@getDeletedAds'
                    ]);

                    Route::get('/{id}/restore', [
                        'as' => 'getAdminRestoreAds', 'uses' => 'Ads\AdsAdminController@getRestoreAd'
                    ])->where('id', '[0-9]+');

                    Route::get('/{id}/delete-forever', [
                        'as' => 'getAdminDeleteAdForever', 'uses' => 'Ads\AdsAdminController@getDeleteAdForever'
                    ])->where('id', '[0-9]+');
                });


                Route::group(['middleware' => ['permission:adminPagesControl'], 'prefix' => 'pages'], function () {

                    Route::get('/', [
                        'as' => 'getAdminPages', 'uses' => 'Pages\PagesAdminController@getPages'
                    ]);

                    Route::get('/datatable', [
                        'as' => 'getAdminPagesDatatable', 'uses' => 'Pages\PagesAdminController@getPagesDatatable'
                    ]);

                    Route::get('/create', [
                        'as' => 'getAdminCreatePage', 'uses' => 'Pages\PagesAdminController@getPage'
                    ]);

                    Route::post('/save/{id?}', [
                        'as' => 'postAdminSavePage', 'uses' => 'Pages\PagesAdminController@postSavePage'
                    ])->where('id', '[0-9]+');

                    Route::get('/{id}', [
                        'as' => 'getAdminPage', 'uses' => 'Pages\PagesAdminController@getPage'
                    ])->where('id', '[0-9]+');

                    Route::get('/{id}/delete', [
                        'as' => 'getAdminDeletePage', 'uses' => 'Pages\PagesAdminController@getDeletePage'
                    ])->where('id', '[0-9]+');

                });

                Route::group(['middleware' => ['permission:adminPostsControl'], 'prefix' => 'posts'], function () {

                    Route::get('/', [
                        'as' => 'getAdminPosts', 'uses' => 'Posts\PostsAdminController@getPosts'
                    ]);

                    Route::get('/datatable', [
                        'as' => 'getAdminPostsDatatable', 'uses' => 'Posts\PostsAdminController@getPostsDatatable'
                    ]);

                    Route::get('/create', [
                        'as' => 'getAdminCreatePost', 'uses' => 'Posts\PostsAdminController@getPost'
                    ]);

                    Route::get('/{id}', [
                        'as' => 'getAdminPost', 'uses' => 'Posts\PostsAdminController@getPost'
                    ])->where('id', '[0-9]+');

                    Route::post('/save/{id?}', [
                        'as' => 'postAdminSavePost', 'uses' => 'Posts\PostsAdminController@postSavePost'
                    ])->where('id', '[0-9]+');

                    Route::get('/{id}/delete', [
                        'as' => 'getAdminPostDelete', 'uses' => 'Posts\PostsAdminController@getDeletePost'
                    ])->where('id', '[0-9]+');

                    Route::group(['prefix' => 'categories'], function () {

                        Route::get('/', [
                            'as' => 'getAdminPostsCategories', 'uses' => 'Posts\AdminPostsCategoriesController@getCategories'
                        ]);

                        Route::get('/create', [
                            'as' => 'getAdminCreatePostCategory', 'uses' => 'Posts\AdminPostsCategoriesController@getCategory'
                        ]);

                        Route::get('/{id}', [
                            'as' => 'getAdminPostCategory', 'uses' => 'Posts\AdminPostsCategoriesController@getCategory'
                        ])->where('id', '[0-9]+');

                        Route::post('/save/{id?}', [
                            'as' => 'postAdminSavePostCategory', 'uses' => 'Posts\AdminPostsCategoriesController@postSaveCategory'
                        ])->where('id', '[0-9]+');

                        Route::get('/{id}/delete', [
                            'as' => 'getAdminPostCategoryDelete', 'uses' => 'Posts\AdminPostsCategoriesController@getDeleteCategory'
                        ])->where('id', '[0-9]+');

                    });

                });

                Route::group(['middleware' => ['permission:adminSettingsControl'], 'prefix' => 'settings'], function () {

                    Route::get('/', [
                        'as' => 'getAdminSettings', 'uses' => 'Settings\SettingsAdminController@getSettings'
                    ]);

                    Route::get('/datatable', [
                        'as' => 'getAdminSettingsDatatable', 'uses' => 'Settings\SettingsAdminController@getSettingsDatatable'
                    ]);

                    Route::get('/{id}', [
                        'as' => 'getAdminParameter', 'uses' => 'Settings\SettingsAdminController@getParameter'
                    ])->where('id', '[0-9]+');

                    Route::post('/save/{id}', [
                        'as' => 'getAdminSaveParameter', 'uses' => 'Settings\SettingsAdminController@postSaveParameter'
                    ])->where('id', '[0-9]+');

                });

            });

        });
    });

});

//this route to dynamically images resize (see App\Helpers\ImageResize)

Route::get('/image/{url?}', ['as' => 'image', function (Illuminate\Http\Request $request, $url = null){
    return ImageResize::renderImage($url, $request->all());
}]);