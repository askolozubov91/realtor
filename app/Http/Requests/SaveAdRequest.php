<?php

namespace App\Http\Requests;
use App\AdType;
use Illuminate\Http\Request as R;

class SaveAdRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //TODO проверка полномочий на сохранение объявления
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(R $request)
    {
        //находим все типы, к которым принадлежит объявление
        $adType = AdType::where('id',$request->route('typeId'))->first()->getAllParents();

        //выбираем все родительские типы объявления и текущий тип
        $adTypes = AdType::whereIn('id',$adType->parents)->orWhere('id',$adType->id)->get();

        //выдергиваем правила валидации для этого типа объявлений и кладем их в $validationRules
        $validationRules = [];

        foreach($adTypes as $type){
            $adTypeFields = json_decode($type->fields,true);

            foreach($adTypeFields as $fieldName => $field){
                if(!empty($field['rules'])){
                    $validationRules[$fieldName] = $field['rules'];
                }
            }
        }

        return array_merge($validationRules,[
            'title' => 'required|string|max:255',
            'content' => '',
            'primary_place' => 'required|exists:places,id',
            'user_id' => 'required|exists:users,id',
            'agency_id' => 'exists:agencies,id',
            'main_image_id' => 'exists:images,id',
            'price' => 'numeric',
            'old_price' => 'numeric',
            'upped_at' => 'date',
            'publication_status_id' => 'required|exists:ads_publications_statuses,id'
        ]);
    }
}
