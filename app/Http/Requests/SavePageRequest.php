<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SavePageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_id' => 'integer|exists:pages,id',
            'title' => 'required|string|max:255',
            'slug' => 'string|max:255',
        ];
    }
}
