<?php

namespace App\Http\Requests;

use App\Helpers\Contracts\ClearDataContract;
use App\Http\Requests\Request;

class SignUpUserPostRequest extends Request
{

    private $clearData;

    public function __construct(ClearDataContract $clearData){
        $this->clearData = $clearData;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function getValidatorInstance()
    {
        $data = $this->all();
        $data['phone'] = $this->clearData->clearPhone($data['phone']);
        if(empty($data['phone'])){
            unset($data['phone']);
        }
        $this->getInputSource()->replace($data);

        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:60',
            'company_name' => 'max:100',
            'email' => 'email|unique:users|max:60|required_without:phone',
            'phone' => 'numeric|between:70000000000,79999999999|required_without:email|unique:users',
            'password' => 'required|same:passwordConfirm|max:60|min:8',
            'passwordConfirm' => 'required|same:password|max:60|min:8',
            'rulesAgree' => 'required|boolean',
            'user_type' => 'required|in:owner,agent',
            'g-recaptcha-response' => 'required|captcha',
        ];
    }
}
