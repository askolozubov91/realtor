<?php

namespace App\Http\Requests;
use App\AdType;
use Illuminate\Http\Request as R;
use Illuminate\Support\Facades\Auth;

class SaveAdUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user()->can('createAds')){
            if(!empty(Auth::user()->confirmed_email) or !empty(Auth::user()->confirmed_phone)){
                return true;
            }
        }
        return false;
    }


    protected function getValidatorInstance()
    {
        $data = $this->all();
        if(!empty($data['price'])){
            $data['price'] = preg_replace("/[^0-9]/","",$data['price']);
        }
        $this->getInputSource()->replace($data);

        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(R $request)
    {
        //находим все типы, к которым принадлежит объявление
        $adType = AdType::where('id',$request->get('type_id'))->first()->getAllParents();

        //выбираем все родительские типы объявления и текущий тип
        $adTypes = AdType::whereIn('id',$adType->parents)->orWhere('id',$adType->id)->get();

        //выдергиваем правила валидации для этого типа объявлений и кладем их в $validationRules
        $validationRules = [];

        foreach($adTypes as $type){
            $adTypeFields = json_decode($type->fields,true);

            foreach($adTypeFields as $fieldName => $field){
                if(!empty($field['rules'])){
                    $validationRules[$fieldName] = $field['rules'];
                }
            }
        }

        return array_merge($validationRules,[
            'type_id' => 'required|exists:ads_types,id',
            'title' => 'required|string|max:255',
            'content' => 'required',
            'primary_place' => 'required|exists:places,id',
            'agency_id' => 'exists:agencies,id',
            'main_image_id' => 'exists:images,id',
            'price' => 'numeric',
            'old_price' => 'numeric',
            'upped_at' => 'date',
        ]);
    }
}
