<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use App\User;

class DeleteUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // if user exist and have permissions
        if(User::where('id', $this->route('id'))->withTrashed()->exists() and (Auth::user()->id === $this->route('id') or Auth::user()->can('adminUsersControl')) and User::myLevelUp($this->route('id'))){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}

