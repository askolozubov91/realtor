<?php

namespace App\Http\Requests;

use App\ContactType;
use App\Http\Requests\Request;

class SaveAgencyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name' => 'required|string|max:255',
            'logo_image_id' => 'integer|exists:images,id',
            'users' => 'required',
            'about' => 'required',
        ];

        //пользователи
        $users = $this->request->get('users');
        if(is_array($users)){
            foreach($users as $key => $val)
            {
                $rules['users.'.$key] = 'required|integer|exists:users,id';
            }
        }

        //контакты
        $contacts = $this->request->get('contacts');
        if(is_array($contacts)){
            $contactsRules = ContactType::lists('rules','id')->toArray();
            foreach($contacts as $key => $val)
            {
                $rules['contacts.'.$key.'.contact_type'] = 'required|integer|exists:contacts_types,id';
                $rules['contacts.'.$key.'.value'] = $contactsRules[$val['contact_type']];
            }
        }


        return $rules;
    }
}
