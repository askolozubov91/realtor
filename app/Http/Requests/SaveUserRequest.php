<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use App\User;

class SaveUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // if user exist and have permissions
        if(User::where('id', $this->route('id'))->exists() and (Auth::user()->id === $this->route('id') or Auth::user()->can('adminUsersControl')) and User::myLevelUp($this->route('id'))){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:60',
            'email' => 'email|unique:users,email,'.$this->route('id').'|max:60|required',
            'phone' => 'numeric|between:70000000000,79999999999|required|unique:users,phone,'.$this->route('id'),
            'password' => 'required_with:password_confirm|same:password_confirm|max:60|min:8',
            'password_confirm' => 'required_with:password|max:60|min:8',
            'user_type' => 'integer|exists:users_types,id',
            'company_name' => 'max:100',
            'agency_id' => 'integer|exists:agencies,id',
            'money' => 'numeric',
            'confirmed_email' => 'boolean',
            'confirmed_phone' => 'boolean',
            'send_notifications_email' => 'boolean',
            'send_notifications_phone' => 'boolean',
            'send_news' => 'boolean',
            'role' => 'exists:roles,id'
        ];
    }
}
