<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use App\User;

class SaveUserProfileRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:60',
            'email' => 'email|unique:users,email,'.Auth::user()->id.'|max:60|required',
            'phone' => 'numeric|between:70000000000,79999999999|required|unique:users,phone,'.Auth::user()->id,
            'password' => 'required_with:password_confirm|same:password_confirm|max:60|min:8',
            'password_confirm' => 'required_with:password|max:60|min:8',
            'user_type' => 'integer|exists:users_types,id',
            'company_name' => 'max:100',
            'agency_id' => 'integer|exists:agencies,id',
            'money' => 'numeric',
            'confirmed_email' => 'boolean',
            'confirmed_phone' => 'boolean',
            'send_notifications_email' => 'boolean',
            'send_notifications_phone' => 'boolean',
            'send_news' => 'boolean'
        ];
    }

    protected function getValidatorInstance()
    {
        $data = $this->all();
        if(!empty($data['phone'])){
            $data['phone'] = preg_replace("/[^0-9]/","",$data['phone']);
        }
        $this->getInputSource()->replace($data);

        /*modify data before send to validator*/

        return parent::getValidatorInstance();
    }
}
