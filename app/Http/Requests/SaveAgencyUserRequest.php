<?php

namespace App\Http\Requests;

use App\ContactType;
use App\Http\Requests\Request;

class SaveAgencyUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name' => 'required|string|max:255',
            'logo_image_id' => 'integer|exists:images,id',
            'about' => 'required',
        ];

        //местоположение
        $places = $this->request->get('places');
        if(is_array($places)){
            foreach($places as $key => $val)
            {
                $rules['places.'.$key.'.primary_place'] = 'required|integer|exists:places,id';
            }
        }

        //контакты
        $contacts = $this->request->get('contacts');
        $havePhone = false;
        if(is_array($contacts)){
            $contactsRules = ContactType::lists('rules','id')->toArray();
            foreach($contacts as $key => $val)
            {
                $rules['contacts.'.$key.'.contact_type'] = 'required|integer|exists:contacts_types,id';
                $rules['contacts.'.$key.'.value'] = $contactsRules[$val['contact_type']];
                if($val['contact_type'] == 1){
                    $havePhone = true;
                }
            }
        }

        if(!$havePhone){
            $rules['phone.need'] = 'required';
        }

        return $rules;
    }
}
