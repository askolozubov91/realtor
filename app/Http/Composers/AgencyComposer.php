<?php

namespace App\Http\Composers;

use App\ContactType;
use App\Place;
use Illuminate\View\View;

class AgencyComposer
{

    protected $types;
    protected $places;


    public function __construct(Place $places, ContactType $types)
    {
        $this->places = $places;
        $this->types = $types;
    }


    public function compose(View $view)
    {
        $view->with('places_regions', $this->places->typeName('region')->orderBy('display_name','ASC')->get());
        $view->with('contactsTypes', $this->types->lists('display_name','id')->toArray());
    }
}