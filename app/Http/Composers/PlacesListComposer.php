<?php

namespace App\Http\Composers;

use App\Place;
use Illuminate\View\View;

class PlacesListComposer
{

    protected $places;


    public function __construct(Place $places)
    {
        $this->places = $places;
    }


    public function compose(View $view)
    {
        $view->with('places', $this->places->getHierarchy('space_offset_array'));
    }
}