<?php

namespace App\Http\Composers;

use App\Place;
use Illuminate\View\View;

class RegionsComposer
{

    protected $places;


    public function __construct(Place $places)
    {
        $this->places = $places;
    }


    public function compose(View $view)
    {
        $regions = $this->places->typeName('region')->orderBy('display_name', 'ASC')->get();
        $regionsGrouped = $regions->groupBy(function($item,$key) {
            return mb_substr($item->display_name, 0, 1);
        })
        ->sortBy(function($item,$key){
            return $key;
        });
        $view->with('regions', $regionsGrouped);
    }
}