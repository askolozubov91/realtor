<?php

namespace App\Http\Composers;

use App\AdType;
use Illuminate\View\View;

class TypesListComposer
{

    protected $types;


    public function __construct(AdType $types)
    {
        $this->types = $types;
    }


    public function compose(View $view)
    {
        $view->with('types', $this->types->getHierarchy('space_offset_array'));
    }
}