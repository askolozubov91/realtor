<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Admin\AdminController;
use App\Page;
use App\PostCategory;
use Illuminate\Http\Request;

use App\Http\Requests\SavePostCategoryRequest;
use Krucas\Notification\Facades\Notification;

class AdminPostsCategoriesController extends AdminController
{
    public function getCategories(PostCategory $categories)
    {
        view()->share('pageTitle', 'Управление категориями статей');
        view()->share('pageDescription', 'Ниже представлен список всех категорий статей.');

        return view('backend.posts.categories.index')
            ->with('categories',$categories->getCategoriesHierarchy('space_offset_array'));
    }

    public function getCategory(PostCategory $categories, $id = null){

        $category = PostCategory::find($id);

        if(empty($category->id)){
            if(!empty($id)){
                App::abort(404);
            }
            view()->share('pageTitle','Создание категории статей');
            view()->share('pageDescription','Вы создаете новую категорию.');

            $category = new PostCategory();
        }else{
            view()->share('pageTitle','Категория №'.$category->id);
            view()->share('pageDescription','Вы редактируете категорию статей.');
        }

        return view('backend.posts.categories.single')
            ->with('category', $category)
            ->with('categories', $categories->getCategoriesHierarchy('one_level_list'));
    }

    public function postSaveCategory(SavePostCategoryRequest $request, PostCategory $category, $id = null){
        $parent = $request->get('parent_id');
        $category = $category->findOrNew($id);
        $category->saveCategory($request->all());
        if(!empty($parent)){
            $category->makeChildOf(PostCategory::findOrFail($parent));
        }
        Notification::success('Категория сохранена');
        return redirect()->route('getAdminPostsCategories');
    }

    public function getDeleteCategory($id)
    {
        if(PostCategory::destroy($id)){
            Notification::success('Категория успешно удалена.');
        }
        return redirect()->route('getAdminPostsCategories');
    }
}
