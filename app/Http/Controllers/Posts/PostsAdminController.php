<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Admin\AdminController;
use App\Post;
use App\PostCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Krucas\Notification\Facades\Notification;
use yajra\Datatables\Datatables;

class PostsAdminController extends AdminController
{
    public function getPosts(PostCategory $categories)
    {
        view()->share('pageTitle', 'Управление статьями');
        view()->share('pageDescription', 'Ниже представлен список всех статей');

        return view('backend.posts.index')
            ->with('categories', $categories->getCategoriesHierarchy());
    }


    public function getPostsDatatable(Datatables $datatables, Request $request){
        if ($request->ajax()) {
            $posts = Post::select('*')->defaultSelect();

            if($request->get('category')){
                $posts->whereHas('categories', function($query) use ($request){
                    $query->where('id', $request->get('category'));
                });
            }

            return $datatables->usingEloquent($posts)
                ->editColumn('title', '<a href="{{ route(\'getAdminPost\',$id) }}">{{ $title }}</a>')
                ->addColumn('image', '<a href="{{route(\'getAdminPost\', $id)}}" ><img src="{{ ImageResize::make((!empty($image->path))?$image->path:null, [\'w\' => 50, \'h\' => 50, \'c\' => true]) }}" width="50" height="50"></a>')
                ->editColumn('content', '{{str_limit(strip_tags($content), 255)}}')
                ->addColumn('categories', '{{(!empty($categories[0]))?implode($categories->lists(\'title\')->toArray(), \', \'):\'нет\'}}')
                ->addColumn('action', '<a href="{{ route(\'getAdminPost\',$id) }}" class="btn btn-xs btn-primary edit-price-margin-btn" ><i class="fa fa-pencil"></i></a>
                                       <a href="#" class="btn btn-xs btn-danger"  data-toggle="modal" data-url="{{route(\'getAdminPostDelete\',$id)}}" data-target="#deletePostModal"><i class="fa fa-times"></i></a>')
                ->make(true);
        }
        abort(404);
    }

    public function getPost(PostCategory $categories, $id = null){
        $post = Post::defaultSelect()->find($id);

        if(!empty($post->id)){
            view()->share('pageTitle', 'Редактирование статьи №'.$post->id);
            view()->share('pageDescription', 'Отредактируйте статью');
        }else{
            if(!empty($id)){
                App::abort(404);
            }
            view()->share('pageTitle', 'Новая статья');
            view()->share('pageDescription', 'Создайте новую статью');
            $post = new Post();
        }

        return view('backend.posts.single')
            ->with('post', $post)
            ->with('categories', $categories->getCategoriesHierarchy('one_level_list'))
            ->with('postCategoriesIds',$post->categories->lists('id')->toArray());
    }


    public function postSavePost(Request $request, $id = null){
        $post = Post::findOrNew($id);
        $post->savePost($request->all());
        $categories = $request->get('categories');
        if(!empty($categories) and is_array($categories)){
            $newCategories = [];

            foreach($categories as $newCategoryId){
                $newCategories[] = $newCategoryId;
                $parents = PostCategory::find($newCategoryId)->allParents();
                foreach($parents as $parent){
                    $newCategories[] = $parent;
                }
            }

            $post->categories()->sync($newCategories);
        }
        Notification::success('Статья сохранена.');
        return redirect()->route('getAdminPost', $post->id);
    }

    public function getDeletePost($id)
    {
        if(Post::destroy($id)){
            Notification::success('Статья успешно удалена.');
        }
        return redirect()->route('getAdminPosts');
    }

}
