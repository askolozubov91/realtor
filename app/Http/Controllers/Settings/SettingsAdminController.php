<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Admin\AdminController;
use App\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;
use Krucas\Notification\Facades\Notification;
use yajra\Datatables\Datatables;

class SettingsAdminController extends AdminController
{

    public function getSettings()
    {
        view()->share('pageTitle', 'Настройки сайта');
        view()->share('pageDescription', 'Ниже расположены глобальные настройки сайта');

        return view('backend.settings.index');
    }


    public function getSettingsDatatable(Datatables $datatables, Request $request){
        if(!$request->ajax()){
            abort(404);
        }

        $settings = Settings::select('*');
        $table = $datatables->usingEloquent($settings)
            ->addColumn('action', '<a href="{{ route(\'getAdminParameter\',$id) }}" class="btn btn-xs btn-primary edit-price-margin-btn" ><i class="fa fa-pencil"></i></a>');

        return $table->make(true);
    }

    public function getParameter($id){

        $param = Settings::find($id);

        view()->share('pageTitle','Параметр "'.$param->name.'"');
        view()->share('pageDescription','Вы редактируете параметр.');

        return view('backend.settings.single')
            ->with('param',$param);
    }

    public function postSaveParameter(Request $request, $id){
        $param = Settings::find($id);
        $param->saveSettings($request->all());

        Notification::success('Параметр сохранен.');
        return redirect()->route('getAdminParameter', $param->id);
    }


}
