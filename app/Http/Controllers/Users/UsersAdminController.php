<?php

namespace App\Http\Controllers\Users;
use App\Http\Controllers\Admin\AdminController;
use Datatables;
use App\User;
use App\Role;
use App\UserType;
use Illuminate\Http\Request;
use Form;
use App\Http\Requests\SaveUserRequest;
use App\Http\Requests\DeleteUserRequest;
use Krucas\Notification\Facades\Notification;

class UsersAdminController extends AdminController
{

    /*
     * Setup controller
     *
     * @return void
     */
    public function __construct(Request $request){
        parent::__construct($request);

        view()->share('roles', Role::lists('display_name', 'id')->toArray());
        view()->share('users_types', UserType::lists('display_name', 'id')->toArray());
    }

    /**
     * Спиок пользователей
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUsers(){
        view()->share('pageTitle','Пользователи');
        view()->share('pageDescription','Ниже находится список всех пользователей системы');

        return view('backend.users.index');
    }

    /**
     * Ajax-поиск пользователей
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postSearchUsers(Request $request){
        if($request->ajax()){
            $users = User::where('id', 'like', '%'.$request->get('q').'%')->orWhere('name', 'like', '%'.$request->get('q').'%')->orWhere('email', 'like', '%'.$request->get('q').'%')->orWhere('phone', 'like', '%'.$request->get('q').'%')->limit(100)->get();
            $response = [];
            foreach($users as $user){
                $response[] = [
                    'value' => $user->id,
                    'text' => 'ID'.$user->id.' '.$user->name,
                ];
            }
            return response()->json($response);
        }
        abort(404);
    }

    /*
     * Get all deleted users page
     *
     * @return Illuminate\View\View
     */
    public function getDeletedUsers(){

        view()->share('pageTitle','Удаленные пользователи');
        view()->share('pageDescription','Ниже находится список всех пользователей, которые были удалены');

        return view('backend.users.index');
    }

    /*
     * Get single users page
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function getUser($id){

        $user = User::withDefaultRelations()->findOrFail($id);

        $disabledForm = false;

        if(!User::myLevelUp($user->id)){
            $disabledForm = 'disabled';
        }

        view()->share('pageTitle','Пользователь №'.$user->id);
        view()->share('pageDescription','Отредактируйте данные пользователя и нажмите кнопку "Сохранить"');

        return view('backend.users.single')
            ->with('user', $user)
            ->with('disabledForm', $disabledForm);
    }


    /*
    * Get users for datatable
    *
    * @param yajra\Datatables\Datatables $datatables
    * @param Illuminate\Http\Request $request
    *
    * @return Illuminate\Http\JsonResponse
    */
    public function getUsersDatatable(Datatables $datatables, Request $request, $type = null){
        if(!$request->ajax()){
            abort(404);
        }

        $users = User::withDefaultRelations();

        if($type === 'deleted'){
            $users->onlyTrashed();
        }

        //user_type filter
        if($request->get('type')){
            $users->where('user_type', $request->get('type'));
        }
        //roles filter
        if($request->get('role')){
            $users->whereHas('roles', function($query) use ($request){
                $query->where('id', $request->get('role'));
            });
        }
        //confirmed email filer
        if($request->get('confirmed_email')){
            $users->where('confirmed_email', true);
        }
        //confirmed phone filer
        if($request->get('confirmed_phone')){
            $users->where('confirmed_phone', true);
        }
        //have agency filter
        if($request->get('have_agency')){
            $users->has('agencies');
        }


        $table = $datatables->usingEloquent($users)
                            ->addColumn('select', '{!! Form::checkbox("select_row[]", 1, false, ["data-select-id" => $id, "class" => "select-row"]) !!}')
                            ->editColumn('user_type', function($user){
                                return $user->userType->display_name;
                            })
                            ->addColumn('contacts', '@if(!empty($phone)) <i class="fa fa-phone"></i>&nbsp;+{{$phone}} <br> @endif
                                                     @if(!empty($email)) <i class="fa fa-envelope-o"></i>&nbsp;<a href="mailto:{{$email}}">{{$email}}</a><br> @endif
                                                     @if(!empty($company_name)) <i class="fa fa-briefcase"></i>&nbsp;{{$company_name}} @endif')
                            ->editColumn('money', '{{$money}}&nbsp;руб.')
                            ->editColumn('roles', '{{$roles[0]["display_name"]}}')
                            ->addColumn('ads', rand(0,100)); //TODO показывать количество объявлений пользователя

        //actions links for deleted and not deleted users
        if($type === 'deleted'){
            $table->addColumn('action', '<a href="{{ route(\'getAdminRestoreUser\', $id) }}">восстановить</a>
                                         <a href="#" class="text-danger" data-toggle="modal" data-url="{{ route(\'getAdminDeleteUserForever\', $id) }}" data-target="#deleteUserModal">удалить&nbsp;навсегда</a>');
        }else{
            $table->editColumn('name', '<a href="{{ route(\'getAdminUser\', $id) }}">{{$name}}</a>')
                  ->addColumn('action', '<a href="{{ route(\'getAdminUser\', $id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                                         <a href="#" class="btn btn-xs btn-danger" data-toggle="modal" data-url="{{ route(\'getAdminDeleteUser\', $id) }}" data-target="#deleteUserModal"><i class="fa fa-close"></i></a>');
        }
        return $table->make(true);
    }


    /*
    * Update user
    *
    * @param SaveUserRequest $request
    * @param int $id
    *
    * @return Illuminate\Http\RedirectResponse
    */
    public function postUpdateUser(SaveUserRequest $request, $id){
        if(User::updateUser($id, $request->all())){
            Notification::success('Пользователь №'.$id.' успешно обновлен.');
        }
        return redirect()->route('getAdminUser',$id);
    }

    /*
     * Datatables actions
     *
     * @param Request $request
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function postUserSelectedAction(Request $request){
        $action = $request->get('action');
        switch($action){
            case 'delete':
                User::destroy($request->get('actionId'));
                Notification::success('Выбранные пользователи успешно удалены.');
                break;
            case 'block':
                $users = User::whereIn('id',$request->get('actionId'))->withDefaultRelations()->get();
                $role = Role::where('name','locked')->first();
                foreach($users as $user){
                    $user->roles()->sync([$role->id]);
                }
                Notification::success('Выбранные пользователи успешно заблокированы.');
                break;
            case 'unblock':
                $users = User::whereIn('id',$request->get('actionId'))->withDefaultRelations()->get();
                $role = Role::where('name','user')->first();
                foreach($users as $user){
                    $user->roles()->sync([$role->id]);
                }
                Notification::success('Выбранные пользователи успешно разблокированы.');
                break;
            case 'deleteForever':
                User::onlyTrashed()->whereIn('id',$request->get('actionId'))->forceDelete();
                Notification::success('Выбранные пользователи успешно удалены навсегда.');
                break;
            case 'restore':
                User::onlyTrashed()->whereIn('id',$request->get('actionId'))->restore();
                Notification::success('Выбранные пользователи успешно восстановлены.');
                break;
        }
        return redirect()->back();
    }

    /*
     * Delete user
     *
     * @param DeleteUserRequest $request
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function getDeleteUser(DeleteUserRequest $request, $id){
        if(User::destroy($id)){
            Notification::success('Пользователь №'.$id.' успешно удален. <a href="'.route('getAdminRestoreUser',$id).'">восстановить</a>');
        }
        return redirect()->route('getAdminUsers');
    }

    /*
     * Delete user forever
     *
     * @param DeleteUserRequest $request
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function getDeleteUserForever(DeleteUserRequest $request, $id){
        User::onlyTrashed()->findOrFail($id)->forceDelete();
        Notification::success('Пользователь №'.$id.' успешно удален навсегда.');
        return redirect()->route('getAdminUsersDeleted');
    }


    /*
     * Restore deleted user
     *
     * @param DeleteUserRequest $request
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function getRestoreUser(DeleteUserRequest $request, $id){
        User::onlyTrashed()->findOrFail($id)->restore();
        Notification::success('Пользователь №'.$id.' восстановлен.');
        return redirect()->route('getAdminUser',$id);
    }
}