<?php

namespace App\Http\Controllers\Places;

use App\Place;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PlacesController extends Controller
{

    public function getPlacesByType(Request $request, $typeName)
    {
        if(!$request->ajax()){
            abort(404);
        }
        $places = Place::typeName($typeName);
        if($request->get('parent_id', false)){
            $places->where('parent_id', $request->get('parent_id'))->orderBy('display_name','ASC');
        }
        return response()->json($places->get()->toArray());
    }

    public function getPlacesByParentId(Request $request)
    {
        if(!$request->ajax()){
            abort(404);
        }
        return response()->json(Place::where('parent_id', $request->get('parent_id'))->orderBy('display_name','ASC')->get());
    }

}
