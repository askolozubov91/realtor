<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $pageTitle;
    protected $pageDescription;

    public function __construct(Request $request){

        $this->pageTitle        = 'domberem.ru';
        $this->pageDescription  = 'Бесплатные объявления о продаже и аренде недвижимости';
        $this->globalCity = $request->get('globalCity');

        view()->share('pageTitle',$this->pageTitle );
        view()->share('pageDescription',$this->pageDescription);
        view()->share('globalCity',$this->globalCity);
    }
}
