<?php

namespace App\Http\Controllers\Agencies;

use App\Agency;
use App\ContactType;
use App\Place;
use App\PlaceType;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests\SaveAgencyRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Krucas\Notification\Facades\Notification;
use yajra\Datatables\Datatables;

class AgenciesAdminController extends Controller
{

    public function getAgencies(Request $request, Place $places){
        view()->share('pageTitle','Агентства');
        view()->share('pageDescription','Ниже находится список всех агентств');

        $userSelectedInFilter = [];
        if($request->get('user')){
            $userSelectedInFilter = User::find($request->get('user'));
        }

        return view('backend.agencies.index')
            ->with('userSelectedInFilter', $userSelectedInFilter)
            ->with('places', $places->getHierarchy('one_level_list', false));
    }

    public function getDeletedAgencies(Request $request, Place $places){
        view()->share('pageTitle','Удаленные агентства');
        view()->share('pageDescription','Ниже находится список всех агентств, которые были удалены.');

        $userSelectedInFilter = [];
        if($request->get('user')){
            $userSelectedInFilter = User::find($request->get('user'));
        }

        return view('backend.agencies.index')
            ->with('userSelectedInFilter', $userSelectedInFilter)
            ->with('places', $places->getHierarchy('one_level_list', false));
    }


    public function getAgenciesDatatable(Datatables $datatables, Request $request, $type=null){
        if(!$request->ajax()){
            abort(404);
        }

        $agencies = Agency::withDefaultRelations();

        if($type === 'deleted'){
            $agencies->onlyTrashed();
        }

        //фильтр по местоположению
        if($request->get('place')){
            $agencies->whereHas('places', function($query) use ($request){
                $query->where('id', $request->get('place'));
            });
        }

        //фильтр по пользователю
        if($request->get('user')){
            $agencies->whereHas('users', function($query) use ($request){
                $query->where('id', $request->get('user'));
            });
        }

        $table = $datatables->usingEloquent($agencies)
            ->addColumn('select', '{!! Form::checkbox("select_row[]", 1, false, ["data-select-id" => $id, "class" => "select-row"]) !!}')
            ->editColumn('users', '@if(!empty($users[0])) <ul class="fa-ul">@foreach($users as $user) <li><i class="fa-li fa fa-user"></i><a href="{{route(\'getAdminUser\', $user->id)}}">ID{{$user->id}} {{$user->name}}</a></li> @endforeach</ul> @else нет @endif')
            ->editColumn('ads', '@if(!empty($ads_count[0])) <a href="{{route(\'getAdminAds\')}}?agency={{$id}}">{{$ads_count[0]->count}}</a> @else 0 @endif')
            ->addColumn('contacts', '@if(!empty($contacts[0])) <ul class="fa-ul">@foreach($contacts as $contact) <li><i class="fa-li fa {{$contact->type->icon}}"></i>{{$contact->value}}</li> @endforeach</ul> @else нет @endif')
            ->addColumn('image', '<a href="{{ route(\'getAdminAgency\', $id) }}"><img class="img-rounded" src="{{ImageResize::make((!empty($image->path))?$image->path:null, [\'w\'=>50, \'h\'=>50, \'c\'=>true ])}}" width="50" height="50"></a>');

        if($type === 'deleted'){
            $table->addColumn('action', '<a href="{{ route(\'getAdminRestoreAgency\',$id) }}">восстановить</a>
                                         <a href="#" class="text-danger" data-toggle="modal" data-url="{{ route(\'getAdminDeleteAgencyForever\',$id) }}" data-target="#deleteAgencyModal">удалить&nbsp;навсегда</a>');
        }else{
            $table->editColumn('name', '<a href="{{ route(\'getAdminAgency\', $id) }}">{{$name}}</a>')
                ->addColumn('action', '<a href="{{ route(\'getAdminAgency\', $id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                                         <a href="#" class="btn btn-xs btn-danger" data-toggle="modal" data-url="{{ route(\'getAdminDeleteAgency\',$id) }}" data-target="#deleteAgencyModal"><i class="fa fa-close"></i></a>');
        }

        return $table->make(true);
    }

    public function postAgenciesSelectedAction(Request $request){
        $action = $request->get('action');
        switch($action){
            case 'delete':
                Agency::destroy($request->get('actionId'));
                Notification::success('Выбранные агентства успешно удалены.');
                break;
            case 'restore':
                Agency::onlyTrashed()->whereIn('id',$request->get('actionId'))->restore();
                Notification::success('Выбранные агентства успешно восстановлены.');
                break;
            case 'deleteForever':
                Agency::onlyTrashed()->whereIn('id',$request->get('actionId'))->forceDelete();
                Notification::success('Выбранные агентства успешно удалены навсегда.');
                break;
        }
        return redirect()->back();
    }

    public function postSearchAgencies(Request $request)
    {
        if($request->ajax()){
            $agencies = Agency::where('id', 'like', '%'.$request->get('q').'%')->orWhere('name', 'like', '%'.$request->get('q').'%')->limit(100)->get();
            $response = [];
            foreach($agencies as $agency){
                $response[] = [
                    'value' => $agency->id,
                    'text' => 'ID'.$agency->id.' '.$agency->name,
                ];
            }
            return response()->json($response);
        }
        abort(404);
    }

    public function getAgency($id = null){

        $agency = Agency::WithDefaultRelations()->find($id);

        if(empty($agency->id)){
            if(!empty($id)){
                App::abort(404);
            }
            view()->share('pageTitle','Создание агентства');
            view()->share('pageDescription','Вы создаете новое агентство или компанию.');

            $agency = new Agency();
        }else{
            view()->share('pageTitle','Агентство №'.$agency->id);
            view()->share('pageDescription','Вы редактируете агентство или компанию.');

            //формируем строки для виджета выбора местоположения
            $placesTypes = PlaceType::lists('name','id');
            foreach($agency->addresses as $i => $address){
                $primaryPlace = Place::find($address->primary_place);
                $primaryPlace->getAllParents(true);

                $searchPlaceData = [];
                $selected_place = [];

                $selected_place[] = $primaryPlace->display_name;

                if($placesTypes[$primaryPlace->type_id] == 'country' or  $placesTypes[$primaryPlace->type_id] == 'city'){
                    $searchPlaceData[] =  $primaryPlace->display_name;
                }

                foreach($primaryPlace->parents as $place){
                    if($placesTypes[$place->type_id] == 'country' or  $placesTypes[$place->type_id] == 'city'){
                        $searchPlaceData[] =  $place->display_name;
                    }
                    $selected_place[] =  $place->display_name;
                }

                $address->search_place_data = implode(', ',array_reverse($searchPlaceData));
                $address->selected_place = implode(' / ',$selected_place);
            }
        }

        return view('backend.agencies.single')
            ->with('agency',$agency)
            ->with('contactsTypes', ContactType::lists('display_name','id')->toArray());
    }

    public function postSaveAgency(SaveAgencyRequest $request, $id=null){
        $agency = Agency::findOrNew($id);
        $agency->saveAgency($request->all());
        Notification::success('Агентство №'.$agency->id.' сохранено.');
        return redirect()->route('getAdminAgency',$agency->id);
    }

    public function getDeleteAgency($id){
        Agency::destroy($id);
        Notification::success('Агентство №'.$id.' удалено.');
        return redirect()->route('getAdminAgencies');
    }

    public function getDeleteAgencyForever($id){
        Agency::onlyTrashed()->findOrFail($id)->forceDelete();
        Notification::success('Агентство №'.$id.' успешно удалено навсегда.');
        return redirect()->route('getAdminAgenciesDeleted');
    }

    public function getRestoreAgency($id){
        Agency::onlyTrashed()->findOrFail($id)->restore();
        Notification::success('Агентство №'.$id.' восстановлено.');
        return redirect()->route('getAdminAgency',$id);
    }
}
