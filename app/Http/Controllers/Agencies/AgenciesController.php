<?php

namespace App\Http\Controllers\Agencies;


use App\Ad;
use App\AdType;
use App\Agency;
use App\Bookmark;
use App\Place;
use App\Http\Requests\SaveAgencyUserRequest;
use Illuminate\Http\Request;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class AgenciesController extends Controller
{
    public function getEditAgency(SaveAgencyUserRequest $request, $id = null)
    {
        $data = $request->all();
        $agency = Agency::findOrNew($id);
        if(empty($agency->id)){
            $data['users'][] = Auth::user()->id;
        }
        if(empty($data['angecyLogoId']) and empty($data['logo_image_id'])){
            $data['logo_image_id'] = null;
        }
        $agency->saveAgency($data);
        return response()->json(['agencySaved' => true, 'id' => $agency->id]);
    }
    
    public function getCatalog(Request $request)
    {
        $agenciesParams = $request->get('agencies_params');
        $data = $request->all();

        // agencies filter
        $place_agency = $request->input( 'place_agency', null );
        $place_agency = $place_agency ? $place_agency : $this->globalCity->id;
        $agencies = Agency::whereHas('places', function($q) use($place_agency){
            $q->where('id', $place_agency);
        });
        
        $agencyParams = $request->input( 'agency_params' );
        if(!empty($agencyParams) and is_array($agencyParams)){
            foreach($agencyParams as $param => $value){
                $table = 'agencies';
                if(is_array($value)){
                    if(count($value) === 2){
                        if(!empty($value[1])){
                            $agencies->whereBetween($table.'.'.$param,$value);
                        }
                        else{
                            $agencies->where($table.'.'.$param,'>=',$value[0]);
                        }
                    }
                }elseif(!empty($value)){
                    switch( $param ) {
                        case 'name':
                            
                            if( mb_strlen( $value ) < 3 ) {
                                $compare = null;
                                $value   = null;
                            }
                            else {
                                $compare = 'LIKE';
                                $value   = "%$value%";
                            }
                            
                            break;
                        default:
                            $compare = '=';
                    }
                    if( null !== $compare && null !== $value ) {
                        $agencies = $agencies->where( $table.'.'.$param, $compare, $value );
                    }
                }
            }
        }
        
        // agencies count
        $agenciesCount  = $agencies->count();
        
        // agencies sort
        switch( $request->input( 'catalog_sort' ) ) {
            case 'ads_count':
                $agencies = $agencies
                    ->join( 'ads', 'ads.agency_id', '=', 'agencies.id' )
                    ->select( 'agencies.*', DB::raw( 'count(ads.id) as ads_count' ) )
                    ->groupBy( 'agencies.id' )
                    ->orderBy( 'ads_count', 'desc' );
                
                break;
                
            default:
                $agencies = $agencies->orderBy( 'agencies.id', 'desc' );
        }
        
        // agencies paginate
        $agencies = $agencies->paginate(30);

        return view('frontend.agencies.catalog_list')
            ->with('districts',Place::where('parent_id', $this->globalCity->id)->typeName('city_district')->orderBy('display_name','ASC')->get())
            ->with('metro',Place::where('parent_id', $this->globalCity->id)->typeName('city_metro')->orderBy('display_name','ASC')->get())
            ->with('agenciesCount',$agenciesCount)
            ->with('agencies',$agencies);
    }

    public function getSingle(Request $request, $id)
    {
        $adParams = $request->get('ad_params');
        $data = $request->all();
        if(!empty($data['catalog_view'])){
            Session::set('catalogView', $data['catalog_view']);
        }

        if(!empty($data['show_catalog_map'])){
            Session::set('showCatalogMap', ($data['show_catalog_map'] === 'on')?true:false);
        }
        
        $ads = Ad::with('images')
            ->with('image')
            ->with('places')
            ->with('agency')
            ->with('user')
            ->with('user.userType')
            ->with('places.type')
            ->with('premium')
            ->whereHas('places', function($q){
                $q->where('id', $this->globalCity->id);
            })
            ->orderBy('ads.id', 'DESK');
        if(!empty($adParams) and is_array($adParams)){
            foreach($adParams as $param => $value){
                $table = 'ads';
                if(is_array($value)){
                    if(count($value) === 2){
                        if(!empty($value[1])){
                            $ads->whereBetween($table.'.'.$param,$value);
                        }
                        else{
                            $ads->where($table.'.'.$param,'>=',$value[0]);
                        }
                    }
                }elseif(!empty($value)){
                    $ads = $ads->where($table.'.'.$param,$value);
                }
            }
        }
        $adsCount  = $ads->count();
        if(session('catalogView') === 'map'){
            $ads = $ads->get();
        }else {
            $ads = $ads->paginate(30);
        }

        $userBookmarks = [];
        if(Auth::check()){
            $userBookmarks = Bookmark::where('user_id', Auth::user()->id)->lists('ad_id')->toArray();
        }

        return view('frontend.agencies.single')
            ->with( 'agency', Agency::find( $id ) )
            ->with('districts',Place::where('parent_id', $this->globalCity->id)->typeName('city_district')->orderBy('display_name','ASC')->get())
            ->with('adsCount',$adsCount)
            ->with('ads',$ads)
            ->with('userBookmarks', $userBookmarks);
    }
    
}
