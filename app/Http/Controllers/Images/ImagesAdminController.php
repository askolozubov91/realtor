<?php

namespace App\Http\Controllers\Images;

use App\Helpers\Facades\ImageResize;
use App\Http\Controllers\Admin\AdminController;
use App\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests\UploadImageRequest;
use Illuminate\Support\Facades\Storage;

class ImagesAdminController extends AdminController
{

    public function postUploadImage(UploadImageRequest $request, Image $image)
    {
        $image = $this->uploadImage($request, $image);
        return response()->json([
            'id'            =>  $image->id,
            'url'           =>  ImageResize::make($image->path, ['w'=>1920, 'h'=>1920]),
            'thumbnail_url' =>  ImageResize::make($image->path, ['w'=>150, 'h'=>150, 'c'=>true ]),
            'name'          =>  $image->name,
            'delete_url'    =>  route('getAdminImageDelete', $image->id),
            'delete_type'   =>  'GET'
        ]);
    }

    public function postUploadImageToEditor(UploadImageRequest $request, Image $image)
    {
        $image = $this->uploadImage($request, $image);
        return response()->json([
            'link' => ImageResize::make($image->path, ['w'=>1920, 'h'=>1920]),
        ]);
    }

    public function getImagesGallery(){
        $images = Image::where('user_id',$this->myUser->id)->get();

        $imagesGallery = [];

        foreach($images as $image){
            $imagesGallery[] = [
                'url'=> ImageResize::make($image->path),
                'thumb' => ImageResize::make($image->path, ['w'=>100, 'h'=>100]),
                'id' => $image->id,
            ];
        }

        return response()->json($imagesGallery);
    }


    private function uploadImage($request, $image){
        $now = Carbon::now();
        $file = $request->file('file');

        $id = $this->myUser->id;
        $userFolder = (floor((($id%500 == 0)?$id-1:$id)/500)*500).'-'.(ceil((($id%500 == 0)?$id-1:$id)/500)*500);

        $newFilePath = '/app/images/'.$userFolder.'/'.$id.'/'.$now->year.'/'.$now->month.'/'.$now->day.'/';
        $newFileName = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
        $file->move(storage_path().$newFilePath, $newFileName);

        $image->saveImage(['path'=>$newFilePath.$newFileName, 'name'=>$newFileName, 'user_id' => $id]);

        return $image;
    }

    public function getImageDelete(Request $request, $id = null){
        if(empty($id)){
            $id = $request->get('id');
        }
        $img = Image::find($id);
        if(!empty($img->id)){
            if(file_exists(storage_path().$img->path)){
                unlink(storage_path().$img->path);
            }
            $img->delete();
        }
    }

}
