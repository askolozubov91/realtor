<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomePageController extends Controller
{
    public function getHome()
    {
        view()->share('pageTitle','Объявления о недвижимости | '. $this->pageTitle);
        view()->share('pageDescription',$this->pageDescription);

        return view('frontend.home');
    }
}
