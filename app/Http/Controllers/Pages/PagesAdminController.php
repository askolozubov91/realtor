<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Admin\AdminController;
use App\Page;
use Illuminate\Http\Request;

use App\Http\Requests\SavePageRequest;
use Illuminate\Support\Facades\App;
use Krucas\Notification\Facades\Notification;
use yajra\Datatables\Datatables;

class PagesAdminController extends AdminController
{

    public function getPages()
    {
        view()->share('pageTitle','Страницы');
        view()->share('pageDescription','Ниже находится список всех страниц.');

        return view('backend.pages.index');
    }

    public function getPagesDatatable(Datatables $datatables, Request $request){
        if(!$request->ajax()){
            abort(404);
        }

        $pages = Page::with('parent');

        $table = $datatables->usingEloquent($pages)
                ->editColumn('title', '<a href="{{ route(\'getAdminPage\',$id) }}">{{ $title }}</a>')
                ->editColumn('parent_id', '@if(!empty($parent->id)) <a href="{{ route(\'getAdminPage\',$parent->id) }}">{{ $parent->title }}</a> @else нет @endif')
                ->editColumn('content', '{{str_limit(strip_tags($content), 255)}}')
                ->addColumn('action', '<a href="{{ route(\'getAdminPage\',$id) }}" class="btn btn-xs btn-primary edit-price-margin-btn" ><i class="fa fa-pencil"></i></a>
                                       <a href="#" class="btn btn-xs btn-danger"  data-toggle="modal" data-url="{{route(\'getAdminDeletePage\',$id)}}" data-target="#deletePageModal"><i class="fa fa-times"></i></a>');

        return $table->make(true);
    }

    public function getPage(Page $pages, $id = null){

        $page = Page::find($id);

        if(empty($page->id)){
            if(!empty($id)){
                App::abort(404);
            }
            view()->share('pageTitle','Создание страницы');
            view()->share('pageDescription','Вы создаете новую страницу.');

            $page = new Page();
        }else{
            view()->share('pageTitle','Страница №'.$page->id);
            view()->share('pageDescription','Вы редактируете страницу.');
        }

        return view('backend.pages.single')
            ->with('page',$page)
            ->with('pages', $pages->getPagesHierarchy('one_level_list'));
    }

    public function postSavePage(SavePageRequest $request, $id = null){
        $page = Page::findOrNew($id);
        $page->savePage($request->all());
        $parent = $request->get('parent_id');
        if(!empty($parent)){
            $page->makeChildOf(Page::findOrFail($parent));
        }else{
            $page->makeRoot();
        }
        Notification::success('Страница сохранена.');
        return redirect()->route('getAdminPage', $page->id);
    }

    public function getDeletePage($id)
    {
        if(Page::destroy($id)){
            Notification::success('Страница успешно удалена.');
        }
        return redirect()->route('getAdminPages');
    }
}
