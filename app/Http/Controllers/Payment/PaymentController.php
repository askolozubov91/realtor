<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

    public function getStartPayment(Request $request)
    {
        $sum = $request->get('payment_sum');
        $user = Auth::user();
        $user->money += $sum;
        $user->save();
        return view('frontend.info.info')
            ->with('title','Баланс пополнен')
            ->with('message','Отлично, баланс пополнен на сумму '.$sum.' руб.');
    }

}
