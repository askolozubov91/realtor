<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Contracts\ClearDataContract;
use App\Http\Requests\LogInUserRequest;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\SignUpUserPostRequest;
use App\UserType;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;

class AuthController extends Controller
{

    private $user;

    public function __construct(Request $request){
        parent::__construct($request);
        $this->user = Auth::user();
    }
    /*
     * login
     */
    public function getLogIn(){
        return view('frontend.auth.login');
    }


    private function authUserWithRelations(){
        if(!empty($this->user->id)){
            return User::withDefaultRelations()->find($this->user->id);
        }
        return false;
    }


    public function postLogIn(LogInUserRequest $request, Guard $auth, ClearDataContract $clearData){
        //clear phone number string
        $phone = $clearData->clearPhone($request->get('emailOrPhone'));
        //try auth
        if($auth->attempt(['email' => $request->get('emailOrPhone'), 'password' => $request->get('password')], $request->get('rememberMe')) or $auth->attempt(['phone' => $phone, 'password' => $request->get('password')], $request->get('rememberMe'))){
            $this->user = $auth->user();
            if(!empty($this->user->email) and empty($this->user->confirmed_email)){
                $this->sendConfirmEmail($this->user);
            }
            if(!empty($this->user->phone) and empty($this->user->confirmed_phone)){
                $this->sendConfirmPhone($this->user);
            }
            if($request->ajax()){
                return response()->json(['success' => true, 'user' => $this->authUserWithRelations()]);
            }
            return redirect()->route('getProfile');
        }
        if($request->ajax()){
            return response()->json(['success' => false, 'user' => null]);
        }
        //generate error notification
        Notification::error('Ошибка авторизации. Проверьте правильность ввода данных.');
        //redirect to login page
        return redirect()->route('getLogIn')->withInput();
    }

    /*
     * registration
     */
    public function getSignUp(){
        return view('frontend.auth.sign_up');
    }

    public function postSignUp(SignUpUserPostRequest $request, Guard $auth){
        $data = $request->all();
        $data['user_type'] = UserType::where('name',$data['user_type'])->first()->id;
        $this->user = User::createUser($data);
        if(!empty($this->user->id)){
            $auth->login($this->user);
            if(!empty($this->user->email)){
                $this->sendConfirmEmail($this->user);
            }
            if(!empty($this->user->phone)){
                $this->sendConfirmPhone($this->user);
            }
        }
        if($request->ajax()){
            $response = ['success' => false, 'user' => null];
            if($auth->check()){
                $response = ['success' => true, 'user' => $this->authUserWithRelations()];
            }
            return response()->json($response);
        }
        return redirect()->route('getProfile');
    }

    public function getAuthUser(Request $request){
        if($request->ajax()){
            return response()->json($this->authUserWithRelations());
        }
        return redirect()->route('getProfile');
    }

    /*
     * contacts confirm
     */
    public function postConfirmContacts(Request $request){
        $errors = [];
        $success = [];
        if($request->get('email_confirm_code', false)){
            $code = mb_strtoupper($request->get('email_confirm_code'));
            if($code === $this->user->getSecretEmailcode()){
                $this->user->confirmEmail($this->user->getSecretEmailToken());
                $success[] = 'Ваш адрес электронной почты подтвержден.';
            }else{
                $errors[] = 'Неправильный код подтверждения адреса электронной почты.';
            }
        }
        if($request->get('phone_confirm_code', false)){
            $code = mb_strtoupper($request->get('phone_confirm_code'));
            if($code === $this->user->getSecretPhoneCode()){
                $this->user->confirmPhone($code);
                $success[] = 'Ваш номер телефона подтвержден.';
            }else{
                $errors[] = 'Неправильный код подтверждения номера телефона.';
            }
        }
        if($request->ajax()){
            return response()->json(['errors' => $errors, 'user' => $this->authUserWithRelations(), 'success' => $success]);
        }
    }

    /*
     * logout
     */
    public function getLogOut(Guard $auth){
        $auth->logout();
        return redirect()->route('getHome');
    }


    /**
     * Отправить письмо с данными для для подтверждения адреса почты
     */
    public function getSendConfirmEmail(){
        if(empty($this->user->confirmed_email)){
            $this->sendConfirmEmail($this->user);
        }
    }

    /**
     * Отправить смс с кодом подтверждения
     */
    public function getSendConfirmPhone(){
        if(empty($this->user->confirmed_phone)){
            $this->sendConfirmPhone($this->user);
        }
    }


    private function sendConfirmPhone($user){
        $user->sendSMS('Код подтверждения номера телефона: '.$user->getSecretPhoneCode());
    }


    private function sendConfirmEmail($user){
        $user->sendEmail(
            'emails.confirm_email',
            [
                'confirmEmailCode' => $user->getSecretEmailcode(),
                'confirmEmailLink' => route('getConfirmEmail', ['id' => $user->id, 'token' => $user->getSecretEmailToken()])
            ],
            'Подтверждение адреса электронной почты'
        );
    }
}
