<?php

namespace App\Http\Controllers\Profile;

use App\Ad;
use App\AdPublicationStatus;
use App\Agency;
use App\Bookmark;
use App\ContactType;
use App\Http\Requests\SaveUserProfileRequest;
use App\Place;
use App\SaveSearch;
use App\User;
use App\Http\Controllers\Controller;
use App\UserType;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;


class ProfileController extends Controller
{

    //main profile page
    public function getProfile(){
        view()->share('pageTitle','Личный кабинет | '. $this->pageTitle);
        view()->share('pageDescription','Личный кабинет');

        return view('frontend.profile.index')
            ->with('statuses', AdPublicationStatus::with('myAdsCount')->get())
            ->with('ads', Ad::with('status')->onlyMy()->take(3)->get())
            ->with('bookmarks', Bookmark::where('user_id', Auth::user()->id)->with('ad')->orderBy('id', 'DESK')->limit(3)->get())
            ->with('bookmarksCount', Bookmark::where('user_id', Auth::user()->id)->count())
            ->with('savedSearch', SaveSearch::where('user_id', Auth::user()->id)->orderBy('id', 'DESK')->limit(3)->get())
            ->with('savedSearchCount', SaveSearch::where('user_id', Auth::user()->id)->count());
    }

    public function getProfileAds($statusName = null){
        view()->share('pageTitle','Мои объявления | '. $this->pageTitle);
        view()->share('pageDescription','Мои объявления в личном кабинете');

        $adStatus = AdPublicationStatus::where('name', (!empty($statusName))?$statusName:'publicated')->first();

        if(empty($adStatus->id)){
            App::abort(404);
        }

        $ads = Ad::with('status')->whereHas('status', function($query) use ($adStatus){
            $query->where('id', $adStatus->id);
        })->onlyMy()->withDefaultRelations()->orderBy('id','DESk');

        return view('frontend.profile.ads')
            ->with('adStatus',$adStatus)
            ->with('statuses', AdPublicationStatus::with('myAdsCount')->get())
            ->with('ads', $ads->paginate(10));
    }

    public function getProfileSettings(){
        view()->share('pageTitle','Настройки');
        view()->share('pageDescription','Настройки вашего аккаунта');
        return view('frontend.profile.settings')
            ->with('user', Auth::user())
            ->with('user_types', UserType::lists('display_name','id')->toArray());
    }

    public function getProfileAgencySettings($agencyId){
        view()->share('pageTitle','Настройки агентства');
        view()->share('pageDescription','Настройки вашего агентства');
        $user = Auth::user();
        $agency = Agency::withDefaultRelations()->where('id',$agencyId)->whereHas('users', function($query) use ($user){
            $query->where('id', $user->id);
        })->first();
        return view('frontend.profile.settings_agency')
            ->with('user', $user)
            ->with('agency', $agency)
            ->with('places', Place::whereIn('parent_id',$agency->places->keyBy('id')->keys())->with('type')->get())
            ->with('agency_place', empty($agency->addresses)?null:$agency->addresses->first())
            ->with('contactsTypes',ContactType::lists('display_name','id')->toArray());
    }


    public function getDeleteAgency($agencyId){

        $user = Auth::user();
        $agency = Agency::whereHas('users', function($query) use ($user){
            $query->where('id', $user->id);
        })->where('id',$agencyId)->first();
        if($agency){
            $archiveStatusId = AdPublicationStatus::where('name','archive')->first()->id;
            Ad::where('agency_id', $agencyId)->update([
                'publication_status_id' => $archiveStatusId,
                'agency_id' => null
            ]);
            $agency->delete();
        }
        Notification::success('Агентство удалено. Объявления, опубликованные от его имени, перенесены в архив.');
        return redirect()->route('getProfileSettings');
    }

    public function getProfileBookmarks(){
        view()->share('pageTitle','Закладки');
        view()->share('pageDescription','Ваши закладки');
        return view('frontend.profile.bookmarks')
            ->with('bookmarks', Bookmark::where('user_id', Auth::user()->id)->with('ad')->with('ad.image')->orderBy('id', 'DESK')->paginate(10));
    }

    public function getProfileSavedSearch(){
        view()->share('pageTitle','Закладки');
        view()->share('pageDescription','Ваши закладки');
        return view('frontend.profile.saved_search')
            ->with('savedSearch', SaveSearch::where('user_id', Auth::user()->id)->orderBy('id', 'DESK')->paginate(10));
    }

    public function getDeleteSavedSearch($id){
        SaveSearch::where('user_id',Auth::user()->id)->where('id',$id)->delete();
        Notification::success('Сохраненный поиск удален.');
        return redirect()->route('getProfileSavedSearch');
    }

    public function postUpdateUser(SaveUserProfileRequest $request){
        $data = $request->all();
        $user = Auth::user();
        if($data['email'] != $user->email){
            $data['confirmed_email'] = false;
        }
        if($data['phone'] != $user->phone){
            $data['confirmed_phone'] = false;
        }
        if(User::updateUser($user->id, $data)){
            Notification::success('Изменения сохранены.');
        }
        return redirect()->route('getProfileSettings');
    }

    public function getConfirmEmail($id, $token)
    {
        $user = User::findOrFail($id);
        if ($user->confirmEmail($token)) {
            Notification::success('Спасибо, ваш адрес электронной почты успешно подтвержден.');
        }
        return redirect()->route('getHome');
    }

}
