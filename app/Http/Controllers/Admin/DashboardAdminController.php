<?php

namespace App\Http\Controllers\Admin;

class DashboardAdminController extends AdminController
{
    public function getDashboard(){

        view()->share('pageTitle','Панель управления');
        view()->share('pageDescription','Добро пожаловать в панель управления');

        return view('backend.dashboard.index');
    }
}