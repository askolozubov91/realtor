<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

abstract class AdminController extends Controller
{

    protected $myUser;

    public function __construct(Request $request){

        parent::__construct($request);

        $this->myUser = Auth::user();

        view()->share('myUser',$this->myUser);

    }
}