<?php

namespace App\Http\Controllers\Ads;

use App\Ad;
use App\AdPremiumRate;
use App\AdPublicationStatus;
use App\AdType;
use App\Bookmark;
use App\Http\Requests\SaveSearchRequest;
use App\Http\Requests\SendAdMessageRequest;
use App\Place;
use App\SaveSearch;
use App\Settings;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests\SaveAdUserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Krucas\Notification\Facades\Notification;

class AdsController extends Controller
{
    public function getEditAd(AdType $adTypeId, $id = 0)
    {
        $type = AdType::findOrNew($adTypeId);
        //если это новое объявление
        if(empty($id)) {
            $ad = new Ad();
            $view = 'frontend.ads.create';
            view()->share('places_regions', Place::typeName('region')->orderBy('display_name','ASC')->get());
        }
        else{
            $ad = Ad::withDefaultRelations()->onlyMy()->find($id);
            $view = 'frontend.ads.edit';
            if(empty($ad)){
                App::abort(404);
            }else{
                $adTypeId = $ad->primary_type;
                $ad->getAdditionalFieldsValues();
                $type = AdType::findOrFail($adTypeId);
                view()->share('typeFields', $this->typeFields($type));
                view()->share('places', Place::whereIn('parent_id',$ad->places->keyBy('id')->keys())->with('type')->get());
            }
        }

        return view()->make($view)
            ->with('ad',$ad)
            ->with('types', $type->getHierarchy('space_offset_array'));
    }


    public function postSaveAd(SaveAdUserRequest $request, $id = null){
        $formData = $request->all();
        $ad = Ad::onlyMy()->findOrNew($id);
        $adData = [];
        $premiumPrice = 0;
        $premiumRatesData = [];
        $publicationStatuses = AdPublicationStatus::lists('id', 'name');
        $user = Auth::user();

        foreach($formData as $key => $value){
            if($key === 'group_field' and is_array($value)){
                foreach($value as $group => $fields){
                    if(is_array($fields)){
                        foreach($fields as $val){
                            $adData[$val] = true;
                        }
                    }
                }
            }else{
                $adData[$key] = $value;
            }
        }
        $adData['user_id'] = Auth::user()->id;
        if(!empty($adData['nullPrice'])){
            $adData['price'] = 0;
        }
        //костыль ебаный, в прочем, их тут и так много
        if(isset($adData['place_address'])){
            $adData['address'] = $adData['place_address'];
        }
        if($adData['ad_action'] == 'publish'){
            $adData['publication_status_id'] = $publicationStatuses['moderation'];
        }
        if($adData['ad_action'] == 'just_save'){
            $adData['publication_status_id'] = $publicationStatuses['draft'];
        }
        //считаем стоимость премиальных плюшек к объявлению
        if(!empty($adData['ad_promo']) and is_array($adData['ad_promo'])){
            $premiumRates = AdPremiumRate::all();
            foreach($premiumRates as $premiumRate){
                if(isset($adData['ad_promo'][$premiumRate->name])){
                    $premiumPrice += $premiumRate->price *  $adData['ad_promo'][$premiumRate->name];
                    if($adRate = $ad->premium->where('name', $premiumRate->name)->first()){
                        $premiumRatesData[$premiumRate->id] = Carbon::parse($adRate->pivot->off_datetime)->addDays($adData['ad_promo'][$premiumRate->name]);
                    }else{
                        $premiumRatesData[$premiumRate->id] = Carbon::now()->addDays($adData['ad_promo'][$premiumRate->name]);
                    }
                }
            }
        }
        $ad->saveAd($adData['type_id'], $adData);
        if($adData['ad_action'] == 'just_save'){
            return response()->json(['url' => route('getProfileAds', 'draft')]);
        }elseif($premiumPrice == 0){
            return response()->json(['url' => route('getProfileAds', 'moderation')]);
        }elseif($premiumPrice > $user->money){
            return response()->json(['error' => 'На вашем балансе недостаточно средств, <a href="#" data-toggle="modal" data-target="#paymentModal">пополните баланс</a> на сумму <strong>'.($premiumPrice - $user->money).'руб.</strong>, что бы опубликовать объявление с текущими настройками,<br> либо измените набор дополнительных опций объявления.']);
        }else{
            $ad->premium()->detach();
            foreach($premiumRatesData as $id => $date){
                $ad->premium()->attach([$id => ['off_datetime' => $date]]);
            }
            $user->money = $user->money - $premiumPrice;
            $user->save();
        }

        return response()->json(['url' => route('getProfileAds', 'moderation')]);
    }


    private function typeFields($type){
        $type->getTypeFields();
        $typeFields = [];
        foreach($type->typeFields as $name => $field){
            if(!empty($field['group'])){
                $typeFields[$field['group_name']]['editor'] = 'group';
                $typeFields[$field['group_name']]['display_name'] = $field['group'];
                $typeFields[$field['group_name']]['values'][$name] = $field;
            }else{
                if(!empty($field['table'])){
                    $field['values'] = $type->getTypeFieldAllValues($type->typeFields, $name);
                }
                $typeFields[$name] = $field;
            }
        }
        return $typeFields;
    }

    public function getAdTypeFields(Request $request, $adTypeId){
        if($request->ajax()){
            $type = AdType::findOrFail($adTypeId);
            $typeFields = $this->typeFields($type);
            return response()->json(['fields' => $typeFields]);
        }
        App::abort(404);
    }

    public  function getChangeStatus($id, $statusName){
        $statusId = AdPublicationStatus::where('name',$statusName)->first()->id;
        $ad = Ad::onlyMy()->find($id);
        $ad->publication_status_id = $statusId;
        $ad->save();
        Notification::success('Статус публикации объявления изменен.');
        return redirect()->route('getProfileAds', $statusName);
    }

    public function getSingle(Request $request, $id)
    {
        $ad = Ad::withDefaultRelations()->find($id);
        if(empty($ad)){
            App::abort(404);
        }
        if($ad->status->name != 'publicated' and Auth::user()->id != $ad->user->id){
            App::abort(404);
        }
        $ad->getAdditionalFieldsValues();
        $adTypeId = $ad->primary_type;
        $type = AdType::findOrFail($adTypeId);
        view()->share('pageTitle', $ad->title);
        view()->share('pageDescription',$ad->title);

        $userBookmarks = [];
        if(Auth::check()){
            $userBookmarks = Bookmark::where('user_id', Auth::user()->id)->lists('ad_id')->toArray();
        }


        return view('frontend.catalog.single')
            ->with('ad', $ad)
            ->with('typeFields', $this->typeFields($type))
            ->with('relatedAds', Ad::where('primary_type',$ad->primary_type)->whereHas('places', function($q) use ($ad){
                $q->whereIn('id', $ad->places->lists('id'));
            })->whereHas('status', function($q){
                $q->where('name', 'publicated');
            })->where('id', '!=', $ad->id)->with('image')->with('images')->limit(24)->get())
            ->with('userBookmarks', $userBookmarks);
    }

    public function postSendSingleAdForm(SendAdMessageRequest $request)
    {
        $data = $request->all();
        $user = User::find($request->get('user_id'));
        $ad = Ad::find($request->get('ad_id'));
        $data['ad'] = $ad;
        Mail::queue('emails.ad_response', $data, function ($m) use ($user) {
            $m->from(Settings::getParameter('site_email'), 'domberem.ru');
            $m->to($user->email, $user->name)->subject('Отклик на ваше объявление');
        });
        Notification::success('Сообщение отправлено.');
        return redirect()->back();
    }


    public function getCatalog(Request $request, $typeName)
    {
        $adParams = $request->get('ad_params');
        $data = $request->all();
        if(!empty($data['catalog_view'])){
            Session::set('catalogView', $data['catalog_view']);
        }

        if(!empty($data['show_catalog_map'])){
            Session::set('showCatalogMap', ($data['show_catalog_map'] === 'on')?true:false);
        }

        $type = AdType::where('name',$typeName)->first();
        if(empty($type)){
            App::abort(404);
        }
        $typeFields = $this->typeFields($type);
        $ads = Ad::joinTypesTables($type)
            ->with('images')
            ->with('image')
            ->with('places')
            ->with('agency')
            ->with('user')
            ->with('user.userType')
            ->with('places.type')
            ->with('premium')
            ->whereHas('places', function($q){
                $q->where('id', $this->globalCity->id);
            })
            ->whereHas('types', function($q) use ($type){
                $q->where('id', $type->id);
            })
            ->orderBy('ads.id', 'DESK');
        if(!empty($adParams) and is_array($adParams)){
            foreach($adParams as $param => $value){
                $table = 'ads';
                if(!empty($type->typeFields[$param])){
                    $table = $type->typeFields[$param]['type_table_name'];
                }
                if(is_array($value)){
                    if(count($value) === 2){
                        if(!empty($value[1])){
                            $ads->whereBetween($table.'.'.$param,$value);
                        }
                        else{
                            $ads->where($table.'.'.$param,'>=',$value[0]);
                        }
                    }
                }elseif(!empty($value)){
                    $ads = $ads->where($table.'.'.$param,$value);
                }
            }
        }
        $adsCount  = $ads->count();
        if(session('catalogView') === 'map'){
            $ads = $ads->get();
        }else {
            $ads = $ads->paginate(30);
        }

        $userBookmarks = [];
        if(Auth::check()){
            $userBookmarks = Bookmark::where('user_id', Auth::user()->id)->lists('ad_id')->toArray();
        }

        return view('frontend.catalog.catalog_list')
            ->with('typeFields',$typeFields)
            ->with('districts',Place::where('parent_id', $this->globalCity->id)->typeName('city_district')->orderBy('display_name','ASC')->get())
            ->with('metro',Place::where('parent_id', $this->globalCity->id)->typeName('city_metro')->orderBy('display_name','ASC')->get())
            ->with('typeName', $typeName)
            ->with('type', $type)
            ->with('typeChildren', AdType::where('parent_id', $type->id)->get())
            ->with('adsCount',$adsCount)
            ->with('ads',$ads)
            ->with('userBookmarks', $userBookmarks);
    }

    public function getAddToBookmark($adId){
        $bookmark = Bookmark::create(['user_id' => Auth::user()->id, 'ad_id' => $adId]);
        if(!empty($bookmark->id)){
            return response()->json(['created' => true]);
        }
        return response()->json(['created' => false]);
    }

    public function getDeleteBookmark(Request $request, $adId){
        Bookmark::where('user_id',Auth::user()->id)->where('ad_id',$adId)->delete();
        if($request->ajax()){
            return response()->json(['deleted' => true]);
        }
        Notification::success('Закладка удалена.');
        return redirect()->route('getProfileBookmarks');
    }

    public function getAdPhone($adId){
        $ad = Ad::where('id', $adId)->with('user')->with('agency')->with('agency.contacts')->with('agency.contacts.type')->first();
        $phone = '+'.$ad->user->phone;
        if(!empty($ad->agency->id)){
            foreach($ad->agency->contacts as $contact){
                if($contact->type->name == 'phone'){
                    $phone = $contact->value;
                    break;
                }
            }
        }
        return response()->json(['phone' => $phone]);
    }


    public function postSaveSearch(SaveSearchRequest $request){
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $saveSearch = new SaveSearch();
        $saveSearch->fill($data)->save();

        Notification::success('Поиск успешно сохранен.');

        return redirect()->back();
    }

}
