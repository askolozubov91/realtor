<?php

namespace App\Http\Controllers\Ads;

use App\AdType;
use App\Http\Controllers\Admin\AdminController;


class AdsTypesAdminController extends AdminController
{

    public function getAdsTypes(AdType $types, $id = null){
        view()->share('pageTitle','Типы объявлений');
        view()->share('pageDescription','Управление типами объявлений.');

        $view = view('backend.ads_types.index')
                ->with('types', $types->getHierarchy('one_level_list'))
                ->with('currentTypeId', $id);

        if(!empty($id)){
            $view->with('currentType', AdType::findOrFail($id));
        }

        return $view;
    }

}