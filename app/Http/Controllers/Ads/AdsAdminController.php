<?php

namespace App\Http\Controllers\Ads;
use App\Ad;
use App\AdPublicationStatus;
use App\AdType;
use App\Agency;
use App\Helpers\Facades\ImageResize;
use App\Http\Controllers\Admin\AdminController;

use App\Http\Requests\SaveAdRequest;
use App\Place;
use App\PlaceType;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Krucas\Notification\Facades\Notification;
use yajra\Datatables\Datatables;


class AdsAdminController extends AdminController
{

    /**
     * Все объявления
     *
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAds(Request $request, Place $places, AdType $types, AdPublicationStatus $statuses){
        view()->share('pageTitle','Объявления');
        view()->share('pageDescription','Ниже находится список всех объявлений.');

        $userSelectedInFilter = [];
        if($request->get('user')){
            $userSelectedInFilter = User::find($request->get('user'));
        }

        $agencySelectedInFilter = [];
        if($request->get('agency')){
            $agencySelectedInFilter = Agency::find($request->get('agency'));
        }

        return view('backend.ads.index')
            ->with('places', $places->getHierarchy('one_level_list'))
            ->with('types', $types->getHierarchy('one_level_list'))
            ->with('userSelectedInFilter', $userSelectedInFilter)
            ->with('agencySelectedInFilter', $agencySelectedInFilter)
            ->with('statuses', $statuses->lists('display_name','id')->toArray());
    }

    /**
     * Объявления, которые были удалены
     *
     * @param Request $request
     * @param Place $places
     * @param AdType $types
     * @param AdPublicationStatus $statuses
     * @return mixed
     */
    public function getDeletedAds(Request $request, Place $places, AdType $types, AdPublicationStatus $statuses){

        view()->share('pageTitle','Удаленные объявления');
        view()->share('pageDescription','Ниже находится список всех объявлений, которые были удалены.');

        $userSelectedInFilter = [];
        if($request->get('user')){
            $userSelectedInFilter = User::find($request->get('user'));
        }

        $agencySelectedInFilter = [];
        if($request->get('agency')){
            $agencySelectedInFilter = Agency::find($request->get('agency'));
        }

        return view('backend.ads.index')
            ->with('places', $places->getHierarchy('one_level_list'))
            ->with('types', $types->getHierarchy('one_level_list'))
            ->with('userSelectedInFilter', $userSelectedInFilter)
            ->with('agencySelectedInFilter', $agencySelectedInFilter)
            ->with('statuses', $statuses->lists('display_name','id')->toArray());

    }


    /**
     * Datatable для объявлений
     *
     * @param Datatables $datatables
     * @param Request $request
     * @param null $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdsDatatable(Datatables $datatables, Request $request, $type=null){
        if(!$request->ajax()){
            abort(404);
        }

        $ads = Ad::withDefaultRelations();

        if($type === 'deleted'){
            $ads->onlyTrashed();
        }

        //фильтр по местоположению
        if($request->get('place')){
            $ads->whereHas('places', function($query) use ($request){
                $query->where('id', $request->get('place'));
            });
        }

        //фильтр по типу объявления
        if($request->get('type')){
            $ads->whereHas('types', function($query) use ($request){
                $query->where('id', $request->get('type'));
            });
        }

        //фильтр по статусу объявления
        if($request->get('status')){
            $ads->where('publication_status_id', $request->get('status'));
        }

        //фильтр по пользователю
        if($request->get('user')){
            $ads->where('user_id', $request->get('user'));
        }

        //фильтр по агентству
        if($request->get('agency')){
            $ads->where('agency_id', $request->get('agency'));
        }

        $table = $datatables->usingEloquent($ads)
                    ->addColumn('select', '{!! Form::checkbox("select_row[]", 1, false, ["data-select-id" => $id, "class" => "select-row"]) !!}')
                    ->editColumn('user_id', '<a href="{{ route(\'getAdminUser\', $user->id) }}">{{$user->name}}</a> <br> {{$user->userType->display_name}} @if(!empty($user->company_name)) <br><i>({{$user->company_name}})</i> @endif')
                    ->addColumn('type', '<ul>@foreach($types as $type)<li>{{$type->display_name}}</li>@endforeach</ul>')
                    ->addColumn('image', '<a href="{{ route(\'getAdminAd\', $id) }}"><img class="img-rounded" src="{{ImageResize::make((!empty($image->path))?$image->path:null, [\'w\'=>50, \'h\'=>50, \'c\'=>true ])}}" width="50" height="50"></a>')
                    ->editColumn('publication_status_id', '{{$status->display_name}}');

        if($type === 'deleted'){
            $table->addColumn('action', '<a href="{{ route(\'getAdminRestoreAds\',$id) }}">восстановить</a>
                                         <a href="#" class="text-danger" data-toggle="modal" data-url="{{ route(\'getAdminDeleteAdForever\',$id) }}" data-target="#deleteAdModal">удалить&nbsp;навсегда</a>');
        }else{
            $table->editColumn('title', '<a href="{{ route(\'getAdminAd\', $id) }}">{{$title}}</a>')
                ->addColumn('action', '<a href="{{ route(\'getAdminAd\', $id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                                         <a href="#" class="btn btn-xs btn-danger" data-toggle="modal" data-url="{{ route(\'getAdminDeleteAd\',$id) }}" data-target="#deleteAdModal"><i class="fa fa-close"></i></a>');
        }

        return $table->make(true);
    }

    /**
     * Применить к выбранным объявлениям
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAdsSelectedAction(Request $request){
        $action = $request->get('action');
        switch($action){
            case 'delete':
                Ad::destroy($request->get('actionId'));
                Notification::success('Выбранные объявления успешно удалены.');
                break;
            case 'setPublicatedStatus':
                $status = AdPublicationStatus::where('name','publicated')->first();
                if(!empty($status)){
                    Ad::whereIn('id',$request->get('actionId'))->update(['publication_status_id' => $status->id]);
                }
                Notification::success('Выбранные объявления успешно опубликованы.');
                break;
            case 'setRejectedStatus':
                $status = AdPublicationStatus::where('name','rejected')->first();
                if(!empty($status)){
                    Ad::whereIn('id',$request->get('actionId'))->update(['publication_status_id' => $status->id]);
                }
                Notification::success('Выбранные объявления отклонены.');
                break;
            case 'setDraftStatus':
                $status = AdPublicationStatus::where('name','draft')->first();
                if(!empty($status)){
                    Ad::whereIn('id',$request->get('actionId'))->update(['publication_status_id' => $status->id]);
                }
                Notification::success('Выбранные объявления перемещены в черновики.');
                break;
            case 'setModerationStatus':
                $status = AdPublicationStatus::where('name','moderation')->first();
                if(!empty($status)){
                    Ad::whereIn('id',$request->get('actionId'))->update(['publication_status_id' => $status->id]);
                }
                Notification::success('Выбранные объявления отправлены на модерацию.');
                break;
            case 'setArchiveStatus':
                $status = AdPublicationStatus::where('name','archive')->first();
                if(!empty($status)){
                    Ad::whereIn('id',$request->get('actionId'))->update(['publication_status_id' => $status->id]);
                }
                Notification::success('Выбранные объявления отправлены в архив.');
                break;
            case 'blockAuthor':
                $authorsIds = Ad::whereIn('id',$request->get('actionId'))->lists('user_id');
                $role = Role::where('name', 'locked')->first();
                DB::table('role_user')->whereIn('user_id',$authorsIds)->update(['role_id' => $role->id]);
                Notification::success('Авторы выбранных объявлений заблокированы.');
                break;
            case 'restore':
                Ad::onlyTrashed()->whereIn('id',$request->get('actionId'))->restore();
                Notification::success('Выбранные объявления успешно восстановлены.');
                break;
            case 'deleteForever':
                Ad::onlyTrashed()->whereIn('id',$request->get('actionId'))->forceDelete();
                Notification::success('Выбранные объявления успешно удалены навсегда.');
                break;

        }
        return redirect()->back();
    }

    /**
     * Одно объвяление или новое объявление
     *
     * @param null $typeId
     * @param null $id
     * @return mixed
     */
    public function getAd($typeId = null, $id = null){

        $ad = Ad::withDefaultRelations()->find($id);

        //если это новое объявление
        if(empty($id)) {
            $ad = new Ad();
        }
        //если объявление не найдено
        elseif(empty($ad)){
            App::abort(404);
        }
        //если объявление существует и все хорошо
        else{
            $typeId = $ad->primary_type;
            $ad->getAdditionalFieldsValues();
        }

        $type = AdType::find($typeId);
        $type->getAllParents(true);

        $types = [$type->display_name];
        foreach($type->parents as $parent){
            $types[] = $parent->display_name;
        }

        $typesString = implode(" / ", array_reverse($types));

        if(empty($ad->id)){
            view()->share('pageTitle','Создание объявления');
            view()->share('pageDescription','Вы создаете объявление в рубрике: "'.$typesString.'"');
        }else{
            view()->share('pageTitle','Объявление №'.$ad->id);
            view()->share('pageDescription','Вы редактируете объявление в рубрике: "'.$typesString.'"');
        }

        $searchPlaceData = [];
        $placesTypes = PlaceType::lists('name','id');
        foreach($ad->places as $place){
            if($placesTypes[$place->type_id] == 'country' or  $placesTypes[$place->type_id] == 'city'){
                $searchPlaceData[] =  $place->display_name;
            }
        }

        return view('backend.ads.single')
            ->with('ad',$ad)
            ->with('type',$type)
            ->with('regions', Place::typeName('region')->lists('display_name','id')->toArray())
            ->with('typeFields', $type->getTypeFields())
            ->with('typeFieldsGroups', $type->getFieldsGroups())
            ->with('statuses', AdPublicationStatus::lists('display_name','id')->toArray())
            ->with('places', implode(' / ',array_reverse($ad->places->lists('display_name')->toArray())))
            ->with('searchPlaceData', implode(', ',$searchPlaceData));
    }

    /**
     * Получить объявление по ID
     *
     * @param $id
     * @return mixed
     */
    public function getAdSingle($id){
        return $this->getAd(null, $id);
    }

    /**
     * Сохранить объявление
     *
     * @param SaveAdRequest $request
     * @param $adType
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSaveAd(SaveAdRequest $request, $adType, $id = null){
        $ad = Ad::findOrNew($id);
        $ad->saveAd($adType, $request->all());
        Notification::success('Объявление №'.$ad->id.' сохранено.');
        return redirect()->route('getAdminAd',$ad->id);
    }

    public function getDeleteAd($id){
        Ad::destroy($id);
        Notification::success('Объявление №'.$id.' удалено.');
        return redirect()->route('getAdminAds');
    }

    public function getDeleteAdForever($id){
        Ad::onlyTrashed()->findOrFail($id)->forceDelete();
        Notification::success('Объявление №'.$id.' успешно удалено навсегда.');
        return redirect()->route('getAdminAdsDeleted');
    }

    public function getRestoreAd($id){
        Ad::onlyTrashed()->findOrFail($id)->restore();
        Notification::success('Объявление №'.$id.' восстановлено.');
        return redirect()->route('getAdminAd',$id);
    }

}