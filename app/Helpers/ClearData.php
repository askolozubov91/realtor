<?php

namespace App\Helpers;

use App\Helpers\Contracts\ClearDataContract;

class ClearData implements ClearDataContract{

    public function clearPhone($phoneNum){
        $phoneNum = preg_replace("/[^0-9,.]/", "", $phoneNum);
        if(strlen($phoneNum)!=11){
            return false;
        }
        if($phoneNum[0] == 8){
            $phoneNum[0] = 7;
        }
        return $phoneNum;
    }
}