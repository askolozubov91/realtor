<?php

namespace App\Helpers;

use App\Helpers\Contracts\MenuContract;
use Illuminate\Support\Facades\Route;

class Menu implements MenuContract{

    /*
    * Return active string if route name in $routeNames array
    *
    * @param array $routeNames
    *
    * @return string
    */
    public function activeLink($routeNames = []){
        if(in_array(Route::getCurrentRoute()->getName(), $routeNames)){
            return 'active';
        }
        return '';
    }
}