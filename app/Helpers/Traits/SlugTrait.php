<?php

namespace App\Helpers\Traits;
use App\Helpers\Facades\SlugGenerator;

trait SlugTrait {

    public function uniqueSlug($slug, $num = 0){
        $finalSlug = $slug;
        if($num > 0){
            $finalSlug .= '-'.$num;
        }
        $query = $this->where('slug',$finalSlug);
        if(!empty($this->id)){
            $query->where('id','!=',$this->id);
        }
        $result = $query->first();
        if(!empty($result)){
            return $this->uniqueSlug($slug, ++$num);
        }
        return $finalSlug;
    }

    public function setSlugAttribute($value = null)
    {
        if(empty($value)){
            $value = $this->attributes[$this->slugBase];
        }
        $slug = SlugGenerator::url_slug($value, ['transliterate' => true]);
        $this->attributes['slug'] = $this->uniqueSlug($slug);
    }

}