<?php

namespace App\Helpers;

use App\Helpers\Contracts\ImageResizeContract;
use Intervention\Image\Facades\Image;

class ImageResize implements ImageResizeContract{

    /**
     * Путь до картинки-заглушки
     *
     * @var string
     */
    private $noImagePath;

    public function __construct(){
        $this->noImagePath = public_path().'/images/no_image.jpg';
    }

    /*
    * Создание URL-адреса изображения
    *
    * @param string $imageUrl
    *
    * @return string
    */
    public function make($imageUrl, $params = []){
        $params['url'] = base64_encode($imageUrl);
        return route('image', $params);
    }

    /**
     * Создание изображения, если изображения не существует, то отдает картинку-заглушку
     *
     * @param null $imageUrl
     * @param array $params
     * @return mixed
     */
    public function renderImage($imageUrl = null, $params = []){
        try{
            return $this->makeImageCache($imageUrl, $params);
        }catch(\Intervention\Image\Exception\NotReadableException $e){
            $imageUrl = base64_encode($this->noImagePath);
            return $this->makeImageCache($imageUrl, $params);
        }
    }

    /**
     * Масштабирование изображения и кеш
     *
     * @param $imageUrl
     * @param $params
     * @return mixed
     */
    public function makeImageCache($imageUrl, $params){
        $cacheImage = Image::cache(function($image) use ($imageUrl, $params) {
            $imageUrl = base64_decode($imageUrl);
            //if is local file
            if(file_exists(storage_path().$imageUrl)){
                $imageUrl = storage_path().$imageUrl;
            }
            $image->make($imageUrl);
            //if the received image width or height
            if(!empty($params['w']) or !empty($params['h'])){
                //resize or fit
                if(empty($params['c'])){
                    $image->resize(array_get($params,'w',null),array_get($params,'h',null), function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                }else{
                    $image->fit(array_get($params,'w',null),array_get($params,'h',null), function ($constraint) {
                        $constraint->upsize();
                    });
                }
            }
            return $image;
        }, 3600, true);
        return $cacheImage->response();
    }
}
