<?php

namespace App\Helpers;

use App\Helpers\Contracts\SMSContract;
use Ixudra\Curl\Facades\Curl;

class SMS implements SMSContract{

    private $apiKey = null;

    public function __construct(){
        $this->apiKey = env('SMS_API_KEY', null);
    }

    public function send($to, $message) {
            Curl::to('http://sms.ru/sms/send')
            ->withData([
                'api_id' => $this->apiKey,
                'to' => $to,
                'text' => $message
            ])
            ->post();
    }
}