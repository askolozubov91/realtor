<?php

namespace App\Helpers\Contracts;

Interface ImageResizeContract
{

    public function make($imageUrl, $params);

    public function renderImage($imageUrl, $params = []);
}