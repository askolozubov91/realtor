<?php

namespace App\Helpers\Contracts;

Interface SlugGeneratorContract
{

    public function url_slug($str, $options);

}
