<?php

namespace App\Helpers\Contracts;

Interface SMSContract
{

    public function send($to, $message);

}
