<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;

class Ad extends Model
{

    use SoftDeletes;

    /**
     * Имя таблицы в БД
     *
     * @var string
     */
    protected $table = 'ads';

    public $validator;

    /**
     * Атрибуты, которые должны быть пркобразованы в даты
     *
     * @var array
     */
    protected $dates = ['upped_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * Атрибуты для массового заполнения
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'primary_type',
        'primary_place',
        'user_id',
        'agency_id',
        'main_image_id',
        'price',
        'old_price',
        'upped_at',
        'publication_status_id',
    ];

    /**
     * Статус публикации объявления
     */
    public function status()
    {
        return $this->belongsTo('App\AdPublicationStatus', 'publication_status_id');
    }

    /**
     * Типы, к которым принадлежит объявление
     */
    public function types()
    {
        return $this->belongsToMany('App\AdType', 'ads_types_ads', 'ad_id', 'type_id');
    }

    /**
     * Местоположения, к которым принадлежит объявление
     */
    public function places()
    {
        return $this->belongsToMany('App\Place', 'ads_places', 'ad_id', 'place_id');
    }

    /**
     * Картинки, связанные с объявлением
     */
    public function images()
    {
        return $this->belongsToMany('App\Image', 'ads_images', 'ad_id', 'image_id');
    }


    /**
     * Пользователь, с которым связано объявление
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Главное изображение
     */
    public function image()
    {
        return $this->belongsTo('App\Image', 'main_image_id');
    }

    /**
     * Агентство, с которым связано объявление
     */
    public function agency()
    {
        return $this->belongsTo('App\Agency', 'agency_id');
    }

    /**
     * Премиум опции, связанные с объявлением
     */
    public function premium()
    {
        return $this->belongsToMany('App\AdPremiumRate', 'ads_premium_rates', 'ad_id', 'premium_rate_id')->withPivot('off_datetime');
    }

    /**
     *
     */
    public function scopeOnlyMy($query){
        return $query->where('user_id', Auth::user()->id);
    }

    /**
     * Скоп для выборки связей объявления, которые требуются в 99% случаев
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithDefaultRelations($query)
    {
        return $query->with('status')->with('types')->with('premium')->with('user')->with('agency')->with('places')->with('images')->with('image');
    }

    public function scopeJoinTypesTables($query, $types){
        $query->leftJoin('type_'.$types->name, 'ads.id', '=', 'type_'.$types->name.'.ad_id');
        foreach($types->parents as $type){
            $query->leftJoin('type_'.$type->name, 'ads.id', '=', 'type_'.$type->name.'.ad_id');
        }
        return $query;
    }

    public function getAdditionalFieldsValues(){
        $query = DB::table('ads')->where('ads.id', $this->id);
        foreach($this->types as $type){
            $query->leftJoin('type_'.$type->name, 'ads.id', '=', 'type_'.$type->name.'.ad_id');
        }
        $additionalFields = $query->first();
        foreach($additionalFields as $name => $value){
            if($name == 'id'){
                continue;
            }
            $this[$name] = $value;
        }
        return $this;
    }


    /*
     * Сохранение объявления
     *
     * @param array $data
     *
     * @return App\Ad
     */
    public function saveAd($typeId,$data){
        //находим все типы, к которым принадлежит объявление
        $adType = AdType::where('id',$typeId)->first()->getAllParents();

        //выбираем все родительские типы объявления и текущий тип
        $adTypes = AdType::whereIn('id',$adType->parents)->orWhere('id',$adType->id)->get();

        $adPlaces = [];
        if(!empty($data['primary_place'])){
            $adPlaces[] = $data['primary_place'];
            $places = Place::where('id',$data['primary_place'])->first()->getAllParents();
            $adPlaces = array_merge($adPlaces, $places->parents);
        }

        //главное изображение
        if(isset($data['main_image_id']) and empty($data['main_image_id'])){
            if(!empty($data['images']) and is_array($data['images'])){
                $data['main_image_id'] = $data['images'][0];
            }else{
                $data['main_image_id'] = null;
            }
        }

        if(isset($data['agency_id']) and empty($data['agency_id'])){
            $data['agency_id'] = null;
        }

        //сохраняем основные поля объявления (таблица ads)
        $this->fill(array_merge($data,['primary_type'=>$adType->id]))->save();

        //привязываем типы к объявлению
        $this->types()->sync($adTypes);
        //привязываем местоположения к объявлению
        $this->places()->sync($adPlaces);

        //привязываем изображения к объявлению
        if(!empty($data['images']) and is_array($data['images'])){
            $this->images()->sync($data['images']);
        }

        //пишем данные объявления в дополнительные таблицы типов
        foreach($adTypes as $type){
            $adTypeFields = json_decode($type->fields,true);
            $adTypeValues = ['ad_id' => $this->id];

            foreach($data as $fieldName => $field){
                if(isset($adTypeFields[$fieldName])){
                    $adTypeValues[$fieldName] = (empty($data[$fieldName]))? NULL : $data[$fieldName];
                }
            }

            //обновляем или создаем новую запись
            if(DB::table('type_'.$type->name)->where('ad_id',$this->id)->first()){
                DB::table('type_'.$type->name)->where('ad_id',$this->id)->update($adTypeValues);
            }else{
                DB::table('type_'.$type->name)->insert($adTypeValues);
            }
        }
        return $this;
    }
}
