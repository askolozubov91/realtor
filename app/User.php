<?php

namespace App;

use App\Helpers\Facades\SMS;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, SoftDeletes;

    use EntrustUserTrait {
        EntrustUserTrait::restore insteadof SoftDeletes;
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'user_type',
        'company_name',
        'agency_id',
        'money',
        'confirmed_email',
        'confirmed_phone',
        'send_notifications_email',
        'send_notifications_phone',
        'send_news'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
     * Get the user that owns the phone.
     */
    public function userType()
    {
        return $this->belongsTo('App\UserType', 'user_type', 'id');
    }


    /**
     * Scope a query to default select users with all default relations.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithDefaultRelations($query)
    {
        return $query->with('agencies')->with('agencies.contacts')->with('agencies.contacts.type')->with('roles')->with('userType');
    }

    /**
     * The agencies that belong to the user.
     */
    public function agencies()
    {
        return $this->belongsToMany('App\Agency', 'users_agencies', 'user_id', 'agency_id');
    }


    public function ads()
    {
        return $this->hasMany('App\Ad', 'user_id');
    }

    public function bookmarks()
    {
        return $this->hasMany('App\Bookmarks', 'user_id');
    }

    public function savedSearch()
    {
        return $this->hasMany('App\Bookmarks', 'user_id');
    }


    /*
     * Create user
     *
     * @param array $data
     * @param string $role
     *
     * @return App\User
     */
    public static function createUser($data, $role='user'){
        //make hash password
        if(!empty($data['password'])){
            $data['password'] = Hash::make($data['password']);
        }
        //create user
        $user = self::create($data);
        //and connect role
        $user->attachRole(Role::where('name', $role)->first()->id);
        return $user;
    }


    /*
     * Update user data
     *
     * @param int $id
     * @param array $data
     *
     * @return App\User
     */
    public static function updateUser($id, $data){
        $user = User::where('id',$id)->first();

        //make hash password or delete password field if empty
        if(!empty($data['password'])){
            $data['password'] = Hash::make($data['password']);
        }else{
            unset($data['password']);
        }

        $user->fill($data)->save();

        //if it's not me and exist role id in data array
        if(Auth::user()->id != $id and !empty($data['role'])){
            $user->roles()->sync([$data['role']]);
        }

        return $user;
    }

    /*
     * Check that the user level no higher than my own
     *
     * @param int $userId
     *
     * @return boolean
     */
    public static function myLevelUp($userId){
        if(Auth::user()->roles()->first()->level <= User::withTrashed()->find($userId)->roles()->first()->level){
            return true;
        }
        return false;
    }

    /**
     * Отправка сообщения пользователю
     *
     * @param $view
     * @param $data
     * @param $theme
     */
    public function sendEmail($view, $data, $theme){
        $user = $this;
        Mail::queue($view, $data, function ($m) use ($user, $theme) {
            $m->from(Settings::getParameter('site_email'), 'domberem.ru');
            $m->to($user->email, $user->name)->subject($theme);
        });
    }

    /**
     * Отправка СМС пользователю
     *
     * @param $text
     */
    public function sendSMS($text){
        SMS::send('79241035832',$text);
    }

    /**
     * Подтверждение электронной почты пользователя
     *
     * @param $token
     * @return bool
     */
    public function confirmEmail($token){
        if($this->getSecretEmailToken() === $token){
            $this->confirmed_email = true;
            $this->send_news = true;
            $this->save();
            return true;
        }
        return false;
    }


    public function confirmPhone($code){
        if($code === $this->getSecretPhoneCode()){
            $this->confirmed_phone = true;
            $this->save();
            return true;
        }
        return false;
    }

    /**
     * Токен для подтверждения электронной почты пользователя
     *
     * @return string
     */
    public function getSecretEmailToken(){
        return md5(md5($this->id.$this->email).'H>ePi39*H08sCnuSlw+df3');
    }

    /**
     * Код для подтверждения email
     *
     * @return string
     */
    public function  getSecretEmailcode(){
        $code = $this->getSecretEmailToken();
        return mb_strtoupper(substr($code,4,6));
    }


    /**
     * Код для подтверждения телефона
     *
     * @return string
     */
    public function getSecretPhoneCode(){
        $code = md5(md5($this->id.$this->phone).'Ok39f(#jed9_k3dfjPDE-KED30');
        return mb_strtoupper(substr($code,4,6));
    }
}
