<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * Таблица в БД
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Модель без дат
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Поля доступные для пакетной записи
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'display_name',
        'value'
    ];

    /**
     * Получить значение параметра по его имени
     *
     * @param $name
     * @return mixed
     */
    public static function getParameter($name){
        $param = self::where('name',$name)->first();
        return $param->value;
    }

    /**
     * Сохранить настройки
     *
     * @param $data
     * @return $this
     */
    public function saveSettings($data){
        $this->fill($data)->save();
        return $this;
    }
}
