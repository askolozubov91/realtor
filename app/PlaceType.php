<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaceType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'places_types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the places for the type.
     */
    public function places()
    {
        return $this->hasMany('App\Place', 'id', 'type_id');
    }
}
