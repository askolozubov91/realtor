function initializePlaceSetUpMap($element) {

    var map, marker;

    var geocoder = new google.maps.Geocoder();
    var mapWrapper = $($element).parents('.select-place-block');
    var addressMap = mapWrapper.find('.address-map');
    var searchPlaceData = mapWrapper.find('.searchPlaceData');
    var latitude = mapWrapper.find('input.latitude').val()*1.0 || 60.4;
    var longitude = mapWrapper.find('input.longitude').val()*1.0 || 93.03;

    map = new google.maps.Map($element.get(0), {
        center: {lat: latitude, lng: longitude},
        zoom: 18,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: false
    });

    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        icon: '/images/map_marker.png',
        position: {lat: latitude, lng: longitude}
    });

    marker.addListener('dragend', function(event){
        mapWrapper.find('input.latitude').val(event.latLng.lat());
        mapWrapper.find('input.longitude').val(event.latLng.lng());
    });

    addressMap.on('keyup', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code === 27) {
            drawingManager.setDrawingMode(null);
        }
        var input = addressMap.val();
        if(input.length > 5){
            geocodeAddress();
        }
    });

    addressMap.on('change', function() {
        var input = addressMap.val();
        if(input.length > 5) {
            geocodeAddress();
        }
    });

    function geocodeAddress() {
        var address = searchPlaceData.val()+' '+addressMap.val();
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                mapWrapper.find('input.latitude').val(results[0].geometry.location.lat());
                mapWrapper.find('input.longitude').val(results[0].geometry.location.lng());
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                map.setZoom(18);
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
    }



}


