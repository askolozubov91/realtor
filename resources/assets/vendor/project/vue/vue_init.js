Vue.directive('selectpicker', {
    twoWay: true,
    bind: function() {
        $(this.el).on("change", function(e) {
            var selectedText = (!$(this.el).val())?'':$(this.el).find('option:selected').text();
            this.set({value: $(this.el).val(), text: selectedText});
        }.bind(this));
    },
    update: function(nv, ov) {
        $(this.el).trigger("change");
    }
});

Vue.directive('range', {
    twoWay: true,
    bind: function() {
        $(this.el).on("change", function(e) {
            this.set($(this.el).val());
        }.bind(this));
    }
});

function vue() {
    return new Vue({
        el: 'body',
        data: {
            showLoader: true,               //показывать прелоадер
            registrationErrors: [],         //массив с ошибками регистрации
            authErrors: [],                 //массив с ошибками авторизации
            confirmErrors: [],              //массив с ошибками подтверждения
            confirmMessage: [],             //сообщения, при подтверждении данных
            registrationUserType: 'agent',  //тип пользователя при регистрации (владелец или агент)
            authSuccess: false,             //авторизация успешна
            authUser: {},                   //данные авторизованнного пользователя
            userLoaded: false,              //прошла ли первичная загрузка пользователя в методе ready
            registrationUrl: '',
            authUrl: '',
            confirmContactsUrl: '',
            sendConfirmEmailUrl: '',
            sendConfirmPhoneUrl: '',
            getAuthUserUrl: '',
            adUploadedPhotos: [],
            adUploadedPhotoMainId: null,
            adTypeFields: false,
            showAdTypeLoader: false,
            selectedTypeId: '',
            adParamsFields: false,
            showPlaceLoader: false,
            placeAddress: '',
            adPublicationColorMarker: 0,
            adPublicationPremium: 0,
            adPublicationUp: 0,
            adErrors: [],
            adErrorsStatus: null,
            agencyErrors: [],
            agencyErrorsStatus: null,
            selectedAdAuthorAgency: null,
            agencyContacts: [],
            angecyLogoTumbnailUrl: null,
            angecyLogoId: null,
            globalCities: [],
            globalRegionId: null,
            globalRegionName: null,
            globalRegionSlug: null,
            showGlobalCityLoader: true,
            adPayError: '',
            saveAgencySuccess: false,
            paymentSum: 500,
        },
        ready: function(){
            this.registrationUrl = $('#vue-urls').data('registrationUrl');
            this.authUrl = $('#vue-urls').data('authUrl');
            this.confirmContactsUrl = $('#vue-urls').data('confirmContactsUrl');
            this.sendConfirmEmailUrl = $('#vue-urls').data('sendConfirmEmailUrl');
            this.sendConfirmPhoneUrl = $('#vue-urls').data('sendConfirmPhoneUrl');
            this.getAuthUserUrl = $('#vue-urls').data('getAuthUserUrl');

            this.getAuthUser();

        },
        methods: {
            getAuthUser: function(){
                var self = this;
                $.ajax({
                    url: self.getAuthUserUrl,
                    type: 'GET',
                    success:  function(data){
                        self.userLoaded = true;
                        self.authUser = data;
                        self.showLoader = false;
                    }
                });
            },
            //авторизация
            auth: function(event){
                event.preventDefault();
                var self = this;
                var form = $(event.target).parents('form');
                //запрос на авторизацию
                $.ajax({
                    url: self.authUrl,
                    data: form.serialize(),
                    type: 'POST',
                    beforeSend: function(){
                        self.showLoader = true;
                    },
                    error: function(data){
                        self.authErrors = data.responseJSON;
                    },
                    success:  function(data){
                        self.authErrors = [];
                        if(data.success){
                            self.authSuccess = true;
                            self.authUser = data.user;
                        }else{
                            self.authErrors = ['Ошибка авторизации. Проверьте правильность ввода данных.'];
                        }
                    },
                    complete: function(){
                        self.showLoader = false;
                    }
                })
            },
            //регистрация
            registration: function(event){
                event.preventDefault();
                var self = this;
                var form = $(event.target).parents('form');
                //запрос на регистрацию
                $.ajax({
                    url: self.registrationUrl,
                    data: form.serialize(),
                    type: 'POST',
                    beforeSend: function(){
                        self.showLoader = true;
                    },
                    error: function(data){
                        self.registrationErrors = data.responseJSON;
                    },
                    success:  function(data){
                        self.registrationErrors = [];
                        if(data.success){
                            self.authSuccess = true;
                            self.authUser = data.user;
                        }
                    },
                    complete: function(){
                        self.showLoader = false;
                    }
                })
            },
            //подтверждение данных
            confirmContacts: function(event){
                event.preventDefault();
                var self = this;
                var form = $(event.target).parents('form');

                $.ajax({
                    url: self.confirmContactsUrl,
                    data: form.serialize(),
                    type: 'POST',
                    beforeSend: function(){
                        self.showLoader = true;
                        self.confirmMessage = [];
                        self.confirmErrors = [];
                    },
                    success:  function(data){
                        if(data.errors.length != 0){
                            self.confirmErrors = data.errors;
                        }else{
                            self.authUser = data.user;
                            self.confirmMessage = data.success;
                        }
                    },
                    complete: function(){
                        self.showLoader = false;
                    }
                })
            },
            //отправить данные подтверждения на email
            sendConfirmEmail: function(){
                var self = this;
                $.ajax({
                    url: self.sendConfirmEmailUrl,
                    type: 'GET',
                    beforeSend: function(){
                        self.showLoader = true;
                        self.confirmMessage = [];
                        self.confirmErrors = [];
                    },
                    complete: function(){
                        self.confirmMessage = ["Код подтверждения адреса электронной почты повторно выслан вам."];
                        self.showLoader = false;
                    }
                })
            },
            //отправить код с подтверждением телефона
            sendConfirmPhone: function(){
                var self = this;
                $.ajax({
                    url: self.sendConfirmPhoneUrl,
                    type: 'GET',
                    beforeSend: function(){
                        self.showLoader = true;
                        self.confirmMessage = [];
                        self.confirmErrors = [];
                    },
                    complete: function(){
                        self.confirmMessage = ["Код подтверждения телефона выслан вам."];
                        self.showLoader = false;
                    }
                })
            },
            //удалить фото из объявления
            removeAdPhoto: function(removeUrl){
                var self = this;
                $.ajax({
                    url: removeUrl,
                    type: 'GET',
                    complete: function(){
                        for(var i = 0; i < self.adUploadedPhotos.length; i+=1){
                            if(self.adUploadedPhotos[i].delete_url == removeUrl){
                                self.adUploadedPhotos.splice(i, 1);
                            }
                        }
                    }
                })
            },
            //выбор типа объявления
            selectAdType: function(typeFieldsUrl, typeId){
                var self = this;
                self.adTypeFields = false;
                self.showAdTypeLoader = true;
                self.selectedTypeId = typeId;
                $.ajax({
                    url: typeFieldsUrl,
                    method: 'GET',
                    success: function(data){
                        self.adTypeFields = jQuery.extend(true, {}, data.fields);
                        delete data.fields.price;
                        delete data.fields.latitude;
                        delete data.fields.longitude;
                        delete data.fields.address;
                        if(jQuery.isEmptyObject(data.fields)){
                            self.adParamsFields = false;
                        }else{
                            self.adParamsFields = data.fields;
                        }
                    },
                    complete:function(){
                        self.showAdTypeLoader = false;
                    }
                });
            },
            selectPlaceList: function(wrapper, url, parentId, targetId){
                var self = this;
                $.ajax({
                    url: url,
                    data: { 'parent_id': parentId },
                    method: 'GET',
                    success: function(data){
                        wrapper.find(targetId+' option:not([value=""])').remove();
                        if(data.length > 0){
                            for(var i = 0; i < data.length; i+=1){
                                wrapper.find(targetId).append('<option value="'+data[i].id+'">'+data[i].display_name+'</option>');
                            }
                            wrapper.find(targetId).parent().show();
                        }else{
                            if(targetId !== '[name=place_address]'){
                                wrapper.find(targetId).parent().hide();
                            }
                        }
                    },
                    complete:function(){
                        wrapper.find(targetId).attr('disabled',false);
                        if(targetId == '[name=place_district]'){
                            wrapper.find('[name=place_address]').attr('disabled',false);
                        }
                        $('.selectpicker').selectpicker('refresh');
                        self.showPlaceLoader = false;
                        var map =  wrapper.find('.map-canvas');
                        google.maps.event.trigger(map.get(0), 'resize');
                    }
                });
            },
            getCities: function(url, parentId, parentName, parentSlug){
                var self = this;
                self.globalRegionId = parentId;
                self.globalRegionName = parentName;
                self.globalRegionSlug = parentSlug;
                self.showGlobalCityLoader = true;
                $.ajax({
                    url: url,
                    data: { 'parent_id': parentId },
                    method: 'GET',
                    success: function(data){
                        self.globalCities = data;
                        self.showGlobalCityLoader = false;
                    }
                });
            },
            saveAd: function(url, action, event){
                var buttonEl = $(event.target);
                var buttonText = buttonEl.text();
                buttonEl.html('<i class="fa fa-refresh fa-spin"></i> загрузка...');
                var formData = $('#saveAdForm').serialize() + '&ad_action=' + action;
                self = this;
                self.adPayError = '';
                $.ajax({
                    url: url,
                    data: formData,
                    method: 'POST',
                    success: function(data){
                        self.adErrors = []
                        if(data.url){
                            window.location = data.url;
                        }
                        if(data.error){
                            self.adPayError = data.error;
                        }
                    },
                    error: function(data){
                        self.adErrorsStatus = data.status;
                        self.adErrors = data.responseJSON;
                    },
                    complete:function(){
                        buttonEl.text(buttonText);
                    }
                });
            },
            addAgencyContact: function(){
                if(this.agencyContactValue){
                    this.agencyContacts.push({'type': this.agencyContactType.value, 'type_name': this.agencyContactType.text, 'value': this.agencyContactValue});
                    this.agencyContactValue = '';
                }
            },
            deleteAgencyContact: function(index){
                this.agencyContacts.splice(index, 1);
            },
            createAgency: function(url, event){
                var buttonEl = $(event.target);
                var buttonText = buttonEl.text();
                buttonEl.html('<i class="fa fa-refresh fa-spin"></i> загрузка...');
                var formData = $('#saveAgencyForm').serialize();
                self = this;
                $.ajax({
                    url: url,
                    data: formData,
                    method: 'POST',
                    success: function(data){
                        if(data.agencySaved){
                            $('#addAgencyModal').modal('hide');
                            self.getAuthUser();
                            self.selectedAdAuthorAgency = data.id;
                            location.reload();
                        }
                    },
                    error: function(data){
                        self.agencyErrorsStatus = data.status;
                        self.agencyErrors = data.responseJSON;
                    },
                    complete:function(){
                        buttonEl.text(buttonText);
                    }
                });
            },
            updateAgency: function(url, event){
                self.saveAgencySuccess = false;
                var buttonEl = $(event.target);
                var buttonText = buttonEl.text();
                buttonEl.html('<i class="fa fa-refresh fa-spin"></i> загрузка...');
                var formData = $('#updateAgencyForm').serialize();
                self = this;
                $.ajax({
                    url: url,
                    data: formData,
                    method: 'POST',
                    success: function(data){
                        console.log(data);
                        self.agencyErrors = [];
                        self.saveAgencySuccess = true;
                    },
                    error: function(data){
                        self.agencyErrorsStatus = data.status;
                        self.agencyErrors = data.responseJSON;
                        self.saveAgencySuccess = false;
                    },
                    complete:function(){
                        buttonEl.text(buttonText);
                    }
                });
            },
        },
        watch: {
            'adTypeFields': function (val, oldVal) {
                $('.selectpicker').selectpicker('refresh');
            },
            'adTypeFields.address': function (val, oldVal) {
                var map = $('#ad-select-place-map');
                google.maps.event.trigger(map.get(0), 'resize');
            },
        }
    });
}