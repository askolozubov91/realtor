(function( $ ) {

    var defaults = {
        localTime: true
    };

    var options;

    var methods = {
        init : function() {
            return this;
        },
        humanDate : function(){
            return this.each(function() {
                $(this).text($.formatDateTime($(this).text()));
            });
        }
    };

    $.formatDateTime = function(dateTimeString){
        var localTime = dateTimeString;
        if(options.localTime){
            localTime = moment.utc(dateTimeString).toDate();
        }
        return moment(localTime).locale('ru').calendar();
    };

    $.fn.dateFormat = function( method , params) {
        options = $.extend({}, defaults, options, params);

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' not found.' );
        }
    };

})(jQuery);