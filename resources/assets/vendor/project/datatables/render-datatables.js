(function( $ ) {

    var defaults = {
        url: '',
        processing: true,
        serverSide: true,
        responsive: true,
        language: {
            decimal:        "",
            emptyTable:     "Нет доступных записей",
            info:           "Показаны записи с _START_ по _END_ из _TOTAL_",
            infoEmpty:      "Показаны записи с 0 по 0 из 0",
            infoFiltered:   "(отфильтровано из _MAX_ записей)",
            infoPostFix:    "",
            thousands:      ",",
            lengthMenu:     "Показывать по _MENU_ записей",
            loadingRecords: "Загрузка...",
            processing:     "Выполнение...",
            search:         "Поиск:",
            zeroRecords:    "Соответствующих записей не найдено",
            paginate: {
                first:      "Первая",
                last:       "Последняя",
                next:       "&rarr;",
                previous:   "&larr;"
            },
            aria: {
                sortAscending:  ": сортировать по возрастанию",
                sortDescending: ": сортировать по убыванию"
            }
        },
        order: [[0, "desc"]],
        filterForm: '.datatable-filter'
    };

    var options;

    var methods = {
        init : function() {
            return $(this);
        },
        users : function(){
            return $(this).DataTable({
                processing: options.processing,
                serverSide: options.serverSide,
                responsive: options.responsive,
                language:   options.language,
                order: options.order,
                ajax: ajaxDatatable(this),
                columns: [
                    { data: 'id', name: 'id', className: 'text-center' },
                    { data: 'select', name: 'select', searchable: false, orderable: false, className: 'text-center'},
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
                    { data: 'name', name: 'name' },
                    { data: 'ads', name: 'ads', searchable: false, className: 'text-center' },
                    { data: 'contacts', name: 'contacts', searchable: false, orderable: false},
                    { data: 'user_type', name: 'user_type', searchable: false},
                    { data: 'roles', name: 'roles', searchable: false, orderable: false},
                    { data: 'money', name: 'money', searchable: false},
                    { data: 'created_at', name: 'created_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                    { data: 'updated_at', name: 'updated_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                    { data: 'phone', name: 'phone', className: 'never'},
                    { data: 'email', name: 'email', className: 'never'},
                    { data: 'company_name', name: 'company_name', className: 'never'},
                ]
            });
        },
        ads : function(){
            return $(this).DataTable({
                processing: options.processing,
                serverSide: options.serverSide,
                responsive: options.responsive,
                language:   options.language,
                order: options.order,
                ajax: ajaxDatatable(this),
                columns: [
                    { data: 'id', name: 'id', className: 'text-center' },
                    { data: 'select', name: 'select', searchable: false, orderable: false, className: 'text-center'},
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
                    { data: 'image', name: 'image', orderable: false, searchable: false, className: 'text-center'},
                    { data: 'title', name: 'title' },
                    { data: 'type', name: 'type', orderable: false, searchable: false},
                    { data: 'user_id', name: 'user_id', orderable: false, searchable: false, className: 'text-center'},
                    { data: 'price', name: 'price', className: 'text-center'},
                    { data: 'publication_status_id', name: 'publication_status_id', className: 'text-center'},
                    { data: 'created_at', name: 'created_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                    { data: 'updated_at', name: 'updated_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                ]
            });
        },
        agencies : function(){
            return $(this).DataTable({
                processing: options.processing,
                serverSide: options.serverSide,
                responsive: options.responsive,
                language:   options.language,
                order: options.order,
                ajax: ajaxDatatable(this),
                columns: [
                    { data: 'id', name: 'id', className: 'text-center' },
                    { data: 'select', name: 'select', searchable: false, orderable: false, className: 'text-center'},
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
                    { data: 'image', name: 'image', orderable: false, searchable: false, className: 'text-center'},
                    { data: 'name', name: 'name' },
                    { data: 'contacts', name: 'contacts', orderable: false, searchable: false},
                    { data: 'users', name: 'users', orderable: false, searchable: false},
                    { data: 'ads', name: 'ads_count', className: 'text-center', orderable: false, searchable: false},
                    { data: 'created_at', name: 'created_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                    { data: 'updated_at', name: 'updated_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                ]
            });
        },
        pages : function(){
            return $(this).DataTable({
                processing: options.processing,
                serverSide: options.serverSide,
                responsive: options.responsive,
                language:   options.language,
                order: options.order,
                ajax: ajaxDatatable(this),
                columns: [
                    { data: 'id', name: 'id', orderable: true },
                    { data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false},
                    { data: 'title', name: 'title'},
                    { data: 'content', name: 'content', orderable: false},
                    { data: 'parent_id', name: 'parent_id', orderable: true, searchable: false},
                    { data: 'created_at', name: 'created_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                    { data: 'updated_at', name: 'updated_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                ]
            });
        },
        posts : function(){
            return $(this).DataTable({
                processing: options.processing,
                serverSide: options.serverSide,
                responsive: options.responsive,
                language:   options.language,
                order: options.order,
                ajax: ajaxDatatable(this),
                columns: [
                    { data: 'id', name: 'id', orderable: true },
                    { data: 'action', name: 'action', orderable: false, className: 'text-center', searchable: false},
                    { data: 'image', name: 'image', className: 'text-center', searchable: false, orderable: false},
                    { data: 'title', name: 'title', orderable: true },
                    { data: 'content', name: 'content', orderable: false, searchable: false},
                    { data: 'categories', name: 'categories', orderable: false, searchable: false},
                    { data: 'created_at', name: 'created_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                    { data: 'updated_at', name: 'updated_at', searchable: false, render: function(data){return $.formatDateTime(data);}},
                ]
            });
        },
        settings : function(){
            return $(this).DataTable({
                processing: options.processing,
                serverSide: options.serverSide,
                responsive: options.responsive,
                language:   options.language,
                order: [[0, "asc"]],
                paging: false,
                searching: false,
                ajax: ajaxDatatable(this),
                columns: [
                    { data: 'id', name: 'id'},
                    { data: 'action', name: 'action', orderable: false, className: 'text-center'},
                    { data: 'name', name: 'name'},
                    { data: 'display_name', name: 'display_name', orderable: false},
                    { data: 'value', name: 'value', orderable: false},
                ]
            });
        }
    };

    function ajaxDatatable(obj){
        return {
            url: options.url || $(obj).data('url'),
            data: $.filersDatatable
        }
    }

    $.filersDatatable = function ( filterData ) {
        $(options.filterForm).each(function(){
            if($(this).is('[type=checkbox]')){
                if(!$(this).is(':checked')){
                    return true;
                }
            }
            filterData[$(this).attr('name')] = $(this).val();
        });
    };

    $.fn.renderDatatable = function( method , params) {
        options = $.extend({}, defaults, options, params);

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' not found.' );
        }
    };

})(jQuery);