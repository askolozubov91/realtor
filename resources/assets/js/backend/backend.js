jQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
        validLabels = /^(data|css):/,
        attr = {
            method: matchParams[0].match(validLabels) ?
                matchParams[0].split(':')[0] : 'attr',
            property: matchParams.shift().replace(validLabels,'')
        },
        regexFlags = 'ig',
        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
};

AjaxBootstrapSelectList.prototype.setTitle = function (title) {
    if (!this.plugin.request) {
        this.title = this.plugin.$element.attr('title');
        if($.isEmptyObject(this.plugin.selectpicker.$lis)) {
            this.plugin.selectpicker.options.selectedTextFormat = 'static';
        }
        this.plugin.$element.attr('title', title);
    }
};

function makeEditor($element){
    var imgId = 0;

    $($element).froalaEditor({
        language: 'ru',
        height: 300,
        imageUploadURL: '/site-control/images/image-upload-to-editor',
        imageMaxSize: 50 * 1024 * 1024,
        imageAllowedTypes: ['jpeg', 'jpg', 'png'],
        imageManagerLoadURL: "/site-control/images/gallery",
        imageManagerDeleteMethod: "GET",
        imageManagerDeleteURL: "/site-control/images/delete",
        toolbarButtons: ['bold', 'italic', 'strikeThrough', '|', 'color', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertHR', '-', 'insertLink', 'insertImage', 'insertVideo',  'insertTable', 'undo', 'redo', 'clearFormatting', 'html']
    }).on('froalaEditor.imageManager.beforeDeleteImage', function (e, editor, $img) {
        imgId = $img.data('id');
        editor.opts.imageManagerDeleteParams = {id: imgId};
    });
}

function initializeMaps(){
    $('.map-canvas').each(function(){
        google.maps.event.addDomListener(window, 'load', initializePlaceSetUpMap($(this)));
    });
}

$(document).ready(function(){

    initializeMaps();

    makeEditor('.froala-editor');

    /*
     * Enable selectpickers
     */
    $('.selectpicker').selectpicker();

    /*
     * ajax selectpicker
     */
    $('.ajax-selectpicker').selectpicker({
        liveSearch: true
    }).ajaxSelectPicker({
        langCode: 'ru',
        clearOnEmpty: true,
        preserveSelected: false
    });

    /*
     * Date and time format
     */
    $('.format-date-time').dateFormat('humanDate');


    /*
     * Render datatables
     */
    $('#users-table').renderDatatable('users');
    $('#ads-table').renderDatatable('ads');
    $('#agencies-table').renderDatatable('agencies');
    $('#pages-table').renderDatatable('pages');
    $('#posts-table').renderDatatable('posts');
    $('#settings-table').renderDatatable('settings');

    /*
     * Action url in modal
     */
    $(document).on('click','a[data-toggle = modal]',function(){
        $($(this).data('target')+' .modal-url-btn').attr('href', $(this).data('url'));
    });

    $(document).on('change','.dataTable .select-row', function(){
        var $this = $(this);
        var selectId = $this.data('select-id');
        if($this.is(":checked")){
            $('.action-datatable-form').append('<input type="hidden" name="actionId[]" id="action-row-'+selectId+'" value="' + selectId +'">');
        }else{
            $('#action-row-'+selectId).remove();
        }
    });

    /*
     * Dropdown menus
     */
    $('.plus-dropdown-menu li').has('ul').prepend('<i class="fa fa-plus-square"></i>');

    $(document).on('click','.plus-dropdown-menu .fa-plus-square',function(){
        $(this).parent().find('ul:first').slideDown();
        $(this).removeClass('fa-plus-square').addClass('fa-minus-square')
    });
    $(document).on('click','.plus-dropdown-menu .fa-minus-square',function(){
        $(this).parent().find('ul').slideUp();
        $(this).parent().find('.fa-minus-square').removeClass('fa-minus-square').addClass('fa-plus-square')
    });

    /*
     * Select place
     */
    var selectedMap;

    $(document).on('click', '[data-target=#editPlaceModal]',function(){
        selectedMap = $(this).parents('.select-place-block');
    });

    $(document).on('click', '.select-place', function(){
        var places = [];
        var placesTypes = [];
        selectedMap.find('input.primaryPlace').val($(this).data('placeId'));
        $(this).parents('li').each(function(){
            var item = $(this).find('.select-place');
            places.push(item.data('placeName'));
            if(item.data('placeType') == 'country' || item.data('placeType') == 'city'){
                placesTypes.push(item.data('placeName'));
            }
        });
        selectedMap.find('.empty-place').addClass('hidden');
        selectedMap.find('.address-field').removeClass('hidden');
        selectedMap.find('.selected-place').val(places.join(' / '));
        selectedMap.find('.searchPlaceData').val(placesTypes.reverse().join(', '));
        $('#editPlaceModal').modal('hide');
        return false;
    });

    $(document).on('keyup blur change', 'input.address-map', function(){
       var mapWrapper = $(this).parents('.select-place-block').find('.map-wrapper');
       var map = mapWrapper.find('.map-canvas');
       if(!$(this).val()){
           mapWrapper.addClass('hidden');
           mapWrapper.find('input.latitude').val("");
           mapWrapper.find('input.longitude').val("");
       }else{
           mapWrapper.removeClass('hidden');
           google.maps.event.trigger(map.get(0), 'resize');
       }
    });

    /*
     * Image upload
     */
    $('#imageUpload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            $('.images-gallery').append('<div class="col-xs-4 gallery-item"><button type="button" class="set-image-main" data-image-id="'+data.result.id+'"><i class="fa fa-check"></i></button><button type="button" class="close delete-gallery-image" data-url="'+data.result.delete_url+'"><i class="fa fa-times"></i></button><a href="'+data.result.url+'" class="thumbnail"><img src="'+data.result.thumbnail_url+'" alt="..."></a><input type="hidden" name="images[]" value="'+data.result.id+'"></div>');
        }
    });

    $('#imageSingleUpload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            $('.images-gallery').html('<div class="col-xs-8 col-xs-offset-2 gallery-item"><button type="button" class="close delete-gallery-image" data-url="'+data.result.delete_url+'"><i class="fa fa-times"></i></button><a href="'+data.result.url+'" class="thumbnail"><img src="'+data.result.thumbnail_url+'" alt="..."></a><input type="hidden" name="'+($('.images-gallery').data('fieldName')||'logo_image_id')+'" value="'+data.result.id+'"></div>');
        }
    });

    $(document).on('click','.delete-gallery-image', function(){
        var wrapper = $(this).parent();
        if(wrapper.find('.set-image-main').hasClass('active')){
            $('input[name=main_image_id]').val(null);
        }
        wrapper.remove();
        $.get($(this).data('url'));
    });

    $(document).on('click','.set-image-main',function(){
        $('.set-image-main').each(function() {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        $('input[name=main_image_id]').val($(this).data('imageId'));
    });

    /**
     * Duplicate blocks
     */
    $(document).on('click', '.duplicate-block', function(e){
        var clone = $(this).parents('.duplicate').clone();
        clone.find(':regex(name,\[[0-9]+\])').each(function(){
            var name = $(this).attr('name');
            var i = name.match(/\[[0-9]+\]/g)[0].match(/[0-9]+/g)[0] * 1;
            $(this).attr('name', name.replace(/\[[0-9]+\]/g, '['+(i+1)+']'));
        });
        clone.find('.bootstrap-select').remove();
        clone.find('.selectpicker').selectpicker();
        clone.find('.ajax-selectpicker').selectpicker({
            liveSearch: true
        }).ajaxSelectPicker({
            langCode: 'ru',
            clearOnEmpty: true,
            preserveSelected: false
        });
        $(this).parents('.duplicate-wrapper').append(clone);
        $(this).replaceWith("<button type='button' class='btn btn-link duplicate-block-remove text-danger pull-right'><i class='fa fa-minus'></i> удалить</button>");
        if(clone.find('.map-canvas').is('div')){
            google.maps.event.addDomListener(window, 'load', initializePlaceSetUpMap(clone.find('.map-canvas')));
        }
    });

    $(document).on('click', '.duplicate-block-remove', function(e){
        $(this).parents('.duplicate').remove();
    });

});