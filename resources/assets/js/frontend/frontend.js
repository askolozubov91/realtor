var catalogMap;
var catalogMapMarkers = [];
var allMarkers = [];

function changeMarker(adId){
    for (i in allMarkers){
        if(allMarkers[i].adId == adId){
            allMarkers[i].setIcon('/images/map_marker_hover.png');
        }else{
            allMarkers[i].setIcon('/images/map_marker.png');
        }
    }
}

function initialize_maps(){
    $('.map-canvas').each(function(){
        google.maps.event.addDomListener(window, 'load', initializePlaceSetUpMap($(this)));
    });

    $('.catalog-row').each(function(){
        var latitude = $(this).data('latitude');
        var longitude = $(this).data('longitude');
        var adId = $(this).data('adId');
        var title = $(this).data('title');
        if(latitude && longitude){
            catalogMapMarkers.push([title, latitude, longitude, adId]);
        }
    });

    function setMarkers(map) {
        var bounds = new google.maps.LatLngBounds();
        var image = {
            url: '/images/map_marker.png',
            size: new google.maps.Size(24, 32)
        };
        for (var i = 0; i < catalogMapMarkers.length; i++) {
            var adMarker = catalogMapMarkers[i];
            var position = new google.maps.LatLng(parseFloat(adMarker[1]), parseFloat(adMarker[2]));
            var marker = new google.maps.Marker({
                position: position,
                map: map,
                icon: image,
                title: adMarker[0],
                adId: adMarker[3]
            });
            bounds.extend(position);
            allMarkers.push(marker);

            google.maps.event.addListener(marker, 'click', function() {
                console.log(this.title);
            });
        }
        map.fitBounds(bounds);

    }

    function initMap() {
        catalogMap = new google.maps.Map(document.getElementById('catalog-map'), {
            zoom: 13,
            center: {lat: 59.325, lng: 18.070}
        });
        setMarkers(catalogMap);
    }

    if($('.catalog-map').is('div')) {
        initMap();
    }
}

$(document).ready(function(){

    var vm = vue();

    $('.data-slider').slider();

    var phoneMask = "+7 (999) 999-9999";
    $(".phone-mask").mask(phoneMask);

    /*
     * Date and time format
     */
    $('.format-date-time').dateFormat('humanDate');

    /*
     * Popovers
     */
    $('[data-toggle="popover"]').popover();

    /*
     * Enable selectpickers
     */
    $('.selectpicker').selectpicker();

    /*
     *
     */
    $('#imagesUpload').fileupload({
        dataType: 'json',

        progress: function(){
            $('#imagesUploadButton .btn').html('<i class="fa fa-refresh fa-spin"></i> Загрузка...');
        },
        done: function (e, data) {
            vm.adUploadedPhotos.push(data.result);
            $('#imagesUploadButton .btn').html('<i class="fa fa-image"></i> Загрузить фото');
        }
    });

    $('#imageSingleUpload').fileupload({
        dataType: 'json',
        progress: function(){
            $('#imageSingleUploadButton .btn').html('<i class="fa fa-refresh fa-spin"></i> Загрузка...');
        },
        done: function (e, data) {
            $('#imageSingleUploadButton .btn').html('<i class="fa fa-image"></i> Загрузить лого');
            vm.angecyLogoTumbnailUrl = data.result.thumbnail_url;
            vm.angecyLogoId = data.result.id;
        }
    });

    $('.uploaded-ad-photo-data').each(function(){
        vm.adUploadedPhotos.push({
            'id'            :  $(this).data('id'),
            'url'           :  $(this).data('url'),
            'thumbnail_url' :  $(this).data('thumbnailUrl'),
            'name'          :  $(this).data('name'),
            'delete_url'    :  $(this).data('deleteUrl'),
            'delete_type'   :  $(this).data('deleteType')
        });
    });

    if($('.uploaded-ad-photo-main').is('div')){
        vm.adUploadedPhotoMainId = $('.uploaded-ad-photo-main').data('id');
    }


    $(document).on('change','.place-select-region', function(){
        var tagretId = $(this).data('targetList');
        if(tagretId !== '[name=place_address]'){
            $(this).parent().nextAll().find('select, input').val('').attr('disabled','disabled');
            vm.placeAddress = '';
            vm.placeAddressAgency = '';
        }
        $(this).parents('.select-place-block').find('.primary-place').val($(this).val());
        vm.selectPlaceList($(this).parents('.select-place-block'),$(this).data('url'),$(this).val(),tagretId);
    });

    $("#addAgencyModal").on("shown.bs.modal", function () {
        var agencyMap = $('#agency-select-place-map');
        google.maps.event.trigger(agencyMap.get(0), 'resize');
    });

    $('#agency-contact-type-select-value').mask(phoneMask);
    $(document).on('change',"#agency-contact-type-select", function(){
        var type = $(this).val();
        var mask = '';
        switch (type) {
            case '1':
                mask = phoneMask;
                break;
            default:
                mask = '';
        }
        $('#agency-contact-type-select-value').unmask().val('').mask(mask);
    });

    $(document).on('click','a[data-toggle = modal]',function(){
        $($(this).data('target')+' .modal-url-btn').attr('href', $(this).data('url'));
    });

    $(document).on('click', '.catalog-filter-toggle', function(){
        $('.additional-fields').toggleClass('active');
    });

    $(document).on('change', '#catalog_sort', function(){
       $(this).parents().find('form#catalog-filter-form').submit();
    });


    if($('.catalog-map').is('div')){
        var catalogListHeight = $('.catalog-list').height();
        var $catalogMap = $('.catalog-map');
        var $footer = $('.footer-wrapper');
        var mapTopOffset = $catalogMap.offset().top;
        var footerTopOffset = $footer.offset().top - 60;
        $('#catalog-map.sidebar-map').height(catalogListHeight);
        function catalogMapSetPosition(){
            var windowHeight = $(window).height();
            var scrollTop = $(window).scrollTop();
            if(catalogListHeight > windowHeight){
                if(mapTopOffset < scrollTop){
                    if(footerTopOffset < scrollTop+windowHeight){
                        $catalogMap.css({ top: footerTopOffset-windowHeight-mapTopOffset + 'px' });
                    }else{
                        $catalogMap.css({ top: scrollTop-mapTopOffset + 'px' });
                    }
                }else{
                    $catalogMap.css({ top: 0 });
                }
            }
        }

        $(window).on('scroll', function(){
            catalogMapSetPosition();
        });

        $(window).on('resize', function(){
            catalogMapSetPosition();
            google.maps.event.trigger(catalogMap, "resize");
        });

        $(document).on('mouseenter', '.catalog-row', function(){
            var id = parseInt($(this).data('adId'));
            changeMarker(id);
        });

        $(document).on('mouseleave', '.catalog-row', function(){
            changeMarker(0);
        });
    }




    $(document).on('click','.bookmark-link', function(e){
        e.preventDefault();
        var bookmarkLink = $(this);
        var url = bookmarkLink.data('urlAdd');
        bookmarkLink.html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
        if(bookmarkLink.hasClass('delete-bookmark')){
            url = bookmarkLink.data('urlDelete');
        }
        $.ajax({
            url: url,
            method: 'GET',
            success: function(data){
                if(bookmarkLink.hasClass('delete-bookmark')){
                    bookmarkLink.html('<i class="fa fa-bookmark-o"></i>');
                }else{
                    bookmarkLink.html('<i class="fa fa-bookmark"></i>');
                }
                bookmarkLink.toggleClass('delete-bookmark');
            }
        });
    });

    $(document).on('click','.bookmark-link-long', function(e){
        e.preventDefault();
        var bookmarkLink = $(this);
        var url = bookmarkLink.data('urlAdd');
        bookmarkLink.html('<i class="fa-li fa fa-circle-o-notch fa-spin fa-fw"></i> Загрузка...');
        if(bookmarkLink.hasClass('delete-bookmark')){
            url = bookmarkLink.data('urlDelete');
        }
        $.ajax({
            url: url,
            method: 'GET',
            success: function(data){
                if(bookmarkLink.hasClass('delete-bookmark')){
                    bookmarkLink.html('<i class="fa-li fa fa-bookmark-o"></i> <a href="#">Добавить в закладки</a>');
                }else{
                    bookmarkLink.html('<i class="fa-li fa fa-bookmark"></i> <a href="#">Удалить из закладок</a>');
                }
                bookmarkLink.toggleClass('delete-bookmark');
            }
        });
    });

    $(document).on('click','.show-ad-phone', function(e){
        e.preventDefault();
        var el = $(this);
        var url = el.data('url');
        el.html('загрузка...');
        $.ajax({
            url: url,
            method: 'GET',
            success: function(data) {
                el.html(data.phone);
            }
        });
    });


    $(document).on('click', '.gallery-ad-images img', function(){
        $('#gallery-ad-main-img').attr('src',$(this).data('bigSrc'));
    });

    var scrolled = 0;
    var galleryAdImages = $('.gallery-ad-images');

    $(document).on('click', '.gallery-ad-nav.down' ,function(){
        scrolled = scrolled+310;
        if(scrolled > galleryAdImages[0].scrollHeight - 310){
            scrolled = galleryAdImages[0].scrollHeight - 310;
        }
        galleryAdImages.animate({
            scrollTop:  scrolled
        });
    });

    $(document).on('click', '.gallery-ad-nav.up' ,function(){
        scrolled = scrolled-310;
        if(scrolled < 0){
            scrolled = 0;
        }
        galleryAdImages.animate({
            scrollTop:  scrolled
        });
    });


    var relatedScrolled = 0;
    var relatedAdImages = $('.related-ad-images');
    var scrollWidth = 680;

    $(document).on('click', '.single-related-nav.right' ,function(){
        relatedScrolled = relatedScrolled+scrollWidth;
        if(relatedScrolled > relatedAdImages[0].scrollWidth - scrollWidth){
            relatedScrolled = relatedAdImages[0].scrollWidth - scrollWidth;
        }
        relatedAdImages.animate({
            scrollLeft:  relatedScrolled
        });
    });

    $(document).on('click', '.single-related-nav.left' ,function(){
        relatedScrolled = relatedScrolled-scrollWidth;
        if(relatedScrolled < 0){
            relatedScrolled = 0;
        }
        relatedAdImages.animate({
            scrollLeft:  relatedScrolled
        });
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = this.href.split('#');
        $('.nav a').filter('[href="#'+target[1]+'"]').tab('show');
    })
});
