@extends('emails.layouts.main')
@section('content')
    <p>
        Код для подтверждения адреса электронной почты, введите его при создании объявления:
    </p>
    <h2>{{$confirmEmailCode}}</h2>
    <p>
        Или перейдите по ссылке для автоматического подтверждения электронной почты: <a href="{{$confirmEmailLink}}">{{$confirmEmailLink}}</a>
    </p>
@endsection