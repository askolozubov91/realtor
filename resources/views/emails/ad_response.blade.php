@extends('emails.layouts.main')
@section('content')
    @if(!empty($ad->id))
        <p>
            Объявление: <a href="{{route('getSingle',$ad->id)}}">{{$ad->title}}</a>
        </p>
    @endif
    @if(!empty($name))
        <p>
            Имя пользователя: {{$name}}
        </p>
    @endif
    @if(!empty($phone))
        <p>
            Телефон пользователя: {{$phone}}
        </p>
    @endif
    @if(!empty($email))
        <p>
            E-mail пользователя: {{$email}}
        </p>
    @endif
    @if(!empty($ad_message))
        <p>
            Сообщение:<br>{{$ad_message}}
        </p>
    @endif
@endsection