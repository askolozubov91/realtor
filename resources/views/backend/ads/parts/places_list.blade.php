<p>Выберете метоположение, к которому вы хотите привязать объявление:</p>
<?php $depth = -1; ?>
@foreach($places as $place)
    @if($place['depth'] == $depth)
        </li><li>
            @elseif($place['depth'] > $depth)
                <ul @if($depth == -1) class="plus-dropdown-menu" @endif ><li>
                        @elseif($place['depth'] < $depth)
                            @for($i = 0; $i < ($depth - $place['depth']); $i++)
                    </li></ul>
        @endfor
        <li>
            @endif
            <span class="plus-dropdown-menu-item">{{$place['display_name']}} <a href="#" class="plus-dropdown-menu-add select-place" data-place-id="{{$place['id']}}" data-place-name="{{$place['display_name']}}" data-place-type="{{$place['type_name']}}">выбрать</a></span>
            <?php $depth = $place['depth']; ?>
            @endforeach
            @for($i = 0; $i <= $depth; $i++)
        </li></ul>
@endfor