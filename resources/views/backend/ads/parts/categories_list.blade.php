<p>Выберете рубрику, в которую вы хотите добавить объявление:</p>
<?php $depth = -1; ?>
@foreach($types as $type)
    @if($type['depth'] == $depth)
        </li><li>
    @elseif($type['depth'] > $depth)
        <ul @if($depth == -1) class="plus-dropdown-menu" @endif ><li>
    @elseif($type['depth'] < $depth)
        @for($i = 0; $i < ($depth - $type['depth']); $i++)
            </li></ul>
        @endfor
        <li>
    @endif
    <span class="plus-dropdown-menu-item">{{$type['display_name']}} ({{$type['count']}}) <a href="{{route('getAdminCreateAd',$type['id'])}}" class="plus-dropdown-menu-add"><i class="fa fa-plus"></i> добавить объявление</a></span>
    <?php $depth = $type['depth']; ?>
@endforeach
@for($i = 0; $i <= $depth; $i++)
    </li></ul>
@endfor