@extends('backend.layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <button class="btn btn-primary pull-right"  data-toggle="modal"  data-target="#AddAdModal"><i class="fa fa-plus"></i> добавить объявление</button>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Фильтр <a class="pull-right btn btn-default btn-xs" role="button" data-toggle="collapse" href="#collapseFiltes" aria-expanded="false" aria-controls="collapseFiltes">показать/скрыть</a>
                </div>
                <?php $input = Input::all(); ?>
                <div id="collapseFiltes" class="panel-collapse collapse @if(!empty($input)) in @endif"  aria-labelledby="collapseFiltes">
                    <div class="panel-body">
                        {!! Form::open(['url' => route((Route::getCurrentRoute()->getName() == 'getAdminUsersDeleted')?'getAdminUsersDeleted':'getAdminAds'), 'method' => 'get']); !!}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('filterPlace', 'Местоположение'); !!}
                                    {!! Form::select('place', [''=>'Все']+$places, Request::get('place'), ['class' => 'selectpicker filter-select show-menu-arrow form-control datatable-filter', 'id' => 'filterPlace', 'data-live-search'=>true]); !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('filterType', 'Тип'); !!}
                                    {!! Form::select('type', [''=>'Все']+$types, Request::get('type'), ['class' => 'selectpicker filter-select show-menu-arrow form-control datatable-filter', 'id' => 'filterType']); !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('filterStatus', 'Статус'); !!}
                                    {!! Form::select('status', [''=>'Все']+$statuses, Request::get('status'), ['class' => 'selectpicker filter-select show-menu-arrow form-control datatable-filter', 'id' => 'filterStatus']); !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('filterUser', 'Автор'); !!}
                                    {!! Form::select('user',(!empty($userSelectedInFilter)?[$userSelectedInFilter->id => 'ID'.$userSelectedInFilter->id.' '.$userSelectedInFilter->name]:[]),(!empty($userSelectedInFilter)?$userSelectedInFilter->id:null),['class'=>'form-control ajax-selectpicker datatable-filter filter-select', 'data-abs-ajax-url'=>route('postAdminSearchUsers'), 'id'=>'filterUser']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('filterAgency', 'Агентство'); !!}
                                    {!! Form::select('agency',(!empty($agencySelectedInFilter)?[$agencySelectedInFilter->id => 'ID'.$agencySelectedInFilter->id.' '.$agencySelectedInFilter->name]:[]),(!empty($agencySelectedInFilter)?$agencySelectedInFilter->id:null),['class'=>'form-control ajax-selectpicker datatable-filter filter-select', 'data-abs-ajax-url'=>route('postAdminSearchAgencies'), 'id'=>'filterAgency']) !!}
                                </div>
                            </div>
                            <div class="col-md-4 filter-button">
                                {!! Form::submit('показать', ['class'=>'btn btn-primary btn-block']); !!}
                            </div>
                        </div>
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table width="100%" class="table table-bordered table-striped" id="ads-table" data-url="{{(route('getAdminAdsDatatable',(Route::getCurrentRoute()->getName() == 'getAdminAdsDeleted')?'deleted':null))}}">
                <thead>
                <tr>
                    <th>№</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Заголовок</th>
                    <th>Тип</th>
                    <th>Автор</th>
                    <th>Цена</th>
                    <th>Статус</th>
                    <th>Создано</th>
                    <th>Обновлено</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => route('postAdminAdsSelectedAction'), 'class'=>'form-inline action-datatable-form']); !!}
                    <div class="row">
                            <div class="col-md-8">
                                @if(Route::getCurrentRoute()->getName() == 'getAdminAdsDeleted')
                                    {!! Form::select('action', [
                                    'restore' => 'Восстановить',
                                    'deleteForever'=>'Удалить навсегда',
                                    ], null, ['class' => 'selectpicker show-menu-arrow form-control', 'id' => 'selectedAction']); !!}
                                @else
                                    {!! Form::select('action', [
                                    'setPublicatedStatus' => 'Опубликовать',
                                    'setRejectedStatus' => 'Отклонить (модерация не пройдена)',
                                    'delete'=>'Удалить',
                                    'blockAuthor' => 'Заблокировать автора',
                                    'setDraftStatus' => 'Переместить в черновики',
                                    'setModerationStatus' => 'Отправить на модерацию',
                                    'setArchiveStatus' => 'Переместить в архив',
                                    ], null, ['class' => 'selectpicker show-menu-arrow form-control', 'id' => 'selectedAction']); !!}
                                @endif
                            </div>
                            <div class="col-md-4">
                                {!! Form::submit('применить к выбранным', ['class'=>'btn btn-primary btn-block']); !!}
                            </div>
                    </div>
                    {!! Form::close(); !!}
                </div>
            </div>
        </div>
    </div>
    @include('backend.parts.modal',[
       'id' => 'AddAdModal',
       'title' => 'Добавить объявление',
       'content' => view()->make('backend.ads.parts.categories_list'),
       'haveCancel' => true,
       'buttons' => []
    ])
    @include('backend.parts.modal',[
       'id' => 'deleteAdModal',
       'title' => 'Удалить объявление',
       'content' => 'Вы действительно хотите удалить это объявление?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection