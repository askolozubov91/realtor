@extends('backend.layouts.main')

@section('content')
    <div class="row">
        @if(empty($ad->id))
            {!! Form::open(['url'=>route('postAdminSaveAd', [$type->id])]) !!}
        @else
            {!! Form::open(['url'=>route('postAdminSaveAd', [$ad->primary_type,$ad->id])]) !!}
        @endif
            <div class="col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Данные объявления</div>
                    <div class="panel-body">
                        @if(empty($typeFields['title']))
                            <div class="form-group">
                                {!! Form::label('title', 'Заголовок объявления'); !!}
                                {!! Form::text('title',$ad->title,['class'=>'form-control']) !!}
                                <p class="help-block">Заголовок, который отображается в списке объявлений</p>
                            </div>
                        @endif
                        <div class="form-group">
                            {!! Form::label('content', 'Текст объявления'); !!}
                            {!! Form::textarea('content',$ad->content,['class'=>'form-control']) !!}
                            <p class="help-block">О вашем предложении</p>
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_id', 'Пользователь'); !!}
                            {!! Form::select('user_id',(!empty($ad->user->id)?[$ad->user->id=>'ID'.$ad->user->id.' '.$ad->user->name]:[]),(!empty($ad->user->id)?$ad->user->id:null),['class'=>'form-control ajax-selectpicker', 'data-abs-ajax-url'=>route('postAdminSearchUsers')]) !!}
                            <p class="help-block">Автор объявления</p>
                        </div>
                        <div class="form-group">
                            {!! Form::label('agency_id', 'Агентство'); !!}
                            {!! Form::select('agency_id',(!empty($ad->agency->id)?[$ad->agency->id=>'ID'.$ad->agency->id.' '.$ad->agency->name]:[]),(!empty($ad->agency->id)?$ad->agency->id:null),['class'=>'form-control ajax-selectpicker', 'data-abs-ajax-url'=>route('postAdminSearchAgencies')]) !!}
                            <p class="help-block">Агентство, с которым связано объявление</p>
                        </div>
                        @if(empty($typeFields['price']))
                            <div class="form-group">
                                {!! Form::label('price', 'Цена'); !!}
                                {!! Form::text('price',$ad->price,['class'=>'form-control']) !!}
                                <p class="help-block">Цена, которая будет указана в объявлении</p>
                            </div>
                        @endif
                        @foreach($typeFields as $name => $field)
                            @if(!empty($field['editor']) and !in_array($name, ['address']) and empty($field['group']))
                                <div class="form-group">
                                    @if($field['editor'] == 'text')
                                        {!! Form::label($name, $field['display_name']); !!}
                                        {!! Form::text($name,$ad[$name],['class'=>'form-control']) !!}
                                    @elseif($field['editor'] == 'select')
                                        {!! Form::label($name, $field['display_name']); !!}
                                        {!! Form::select($name,[null => 'Ничего не выбрано'] + $type->getTypeFieldAllValues($typeFields, $name),$ad[$name],['class'=>'form-control selectpicker']) !!}
                                    @elseif($field['editor'] == 'boolean')
                                        <div class="checkbox">
                                            <label>
                                                <input type="hidden" name="{{$name}}" value="0">
                                                {!! Form::checkbox($name,1,$ad[$name]) !!} {{$field['display_name']}}
                                            </label>
                                        </div>
                                    @endif
                                    @if(!empty($field['display_description']))
                                        <p class="help-block">{{$field['display_description']}}</p>
                                    @endif
                                </div>
                            @endif
                        @endforeach
                        @foreach($typeFieldsGroups as $group)
                             <hr>
                             <p><strong>{{$group}}</strong></p>
                             @foreach($typeFields as $name => $field)
                                  @if($field['editor'] == 'boolean' and array_get($field,'group') == $group)
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" name="{{$name}}" value="0">
                                            {!! Form::checkbox($name,1,$ad[$name]) !!} {{$field['display_name']}}
                                        </label>
                                    </div>
                                  @endif
                             @endforeach
                        @endforeach
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Местоположение</div>
                    <div class="panel-body select-place-block">
                        <div class="form-group">
                            <div class="input-group">
                                {!! Form::text('selected_place',$places,['class'=>'form-control selected-place', 'readonly' => 'readonly', 'placeholder'=>'Нажмите на кнопку "выбрать"...', 'id' => 'place-map']) !!}
                                {!! Form::hidden('primary_place', $ad->primary_place, ['class' => 'primaryPlace']) !!}
                                {!! Form::hidden('searchPlaceData', $searchPlaceData, ['class' => 'searchPlaceData']) !!}
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button" data-toggle="modal"  data-target="#editPlaceModal">выбрать</button>
                                </span>
                            </div>
                        </div>
                        @if(isset($typeFields['address']) and isset($typeFields['latitude']) and isset($typeFields['longitude']))
                            <div class="form-group">
                                {!! Form::label('address', 'Адрес'); !!}
                                <div class="alert alert-info text-center empty-place @if(!empty($ad->primary_place) or Input::old('primary_place')) hidden @endif">Сначала выберете местоположение.</div>
                                <div class="address-field @if(empty($ad->primary_place) and !Input::old('primary_place')) hidden @endif">
                                    {!! Form::text('address',$ad->address,['class'=>'form-control address-map', 'placeholder' => 'Введите адрес...']) !!}
                                </div>
                            </div>
                            <div class="map-wrapper @if(empty($ad->address) and !Input::old('address')) hidden @endif">
                                {!! Form::hidden('latitude', $ad->latitude, ['class' => 'latitude']) !!}
                                {!! Form::hidden('longitude', $ad->longitude, ['class' => 'longitude']) !!}
                                <div class="map-canvas" style="height: 300px;"></div>
                                <p class="help-block">Перетащите маркер в нужное место, если он расположен неправильно.</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                @if(!empty($ad->id))
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Информация
                        </div>
                        <div class="panel-body small-text">
                            <p><i class="fa fa-clock-o"></i> Создано: <span class="format-date-time">{{$ad->created_at}}</span></p>
                            <p><i class="fa fa-clock-o"></i> Обновлено: <span class="format-date-time">{{$ad->created_at}}</span></p>
                        </div>
                    </div>
                @endif
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Изображения
                        </div>
                        <div class="panel-body small-text">
                            <div class="fileinput-button upload-gallery-image">
                                <span class="btn btn-primary btn-block"><i class="fa fa-upload"></i> загрузить</span>
                                <input id="imageUpload" type="file" name="file" data-url="{{route('postAdminUploadImage')}}" multiple>
                            </div>
                            <div class="row images-gallery">
                                {!! Form::hidden('main_image_id', $ad->main_image_id) !!}
                                @foreach($ad->images as $image)
                                    <div class="col-xs-4 gallery-item">
                                        <button type="button" class="set-image-main @if($ad->main_image_id == $image->id) active @endif " data-image-id="{{$image->id}}"><i class="fa fa-check"></i></button>
                                        <button type="button" class="close delete-gallery-image" data-url="{{route('getAdminImageDelete', $image->id)}}"><i class="fa fa-times"></i></button>
                                        <a href="{{ImageResize::make($image->path, ['w'=>1920, 'h'=>1080])}}" class="thumbnail"><img src="{{ImageResize::make($image->path, ['w'=>150, 'h'=>150, 'c'=>true])}}" alt="..."></a>
                                        {!! Form::hidden('images[]', $image->id) !!}
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">Управление</div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('publication_status_id', 'Статус публикации'); !!}
                            {!! Form::select('publication_status_id',$statuses,$ad->publication_status_id,['class'=>'form-control selectpicker']) !!}
                            <p class="help-block">Статус публикации объявления</p>
                        </div>
                        <p><button type="submit" class="btn btn-success btn-block">Сохранить</button></p>
                        @if(!empty($ad->id))
                            <p class="text-center"><a href="#" class="text-danger" data-toggle="modal" data-url="{{route('getAdminDeleteAd',$ad->id)}}" data-target="#deleteAdModal"><i class="fa  fa-close"></i> удалить</a></p>
                        @endif
                    </div>
                </div>
            </div>
        {!! Form::close(); !!}
    </div>
    @include('backend.parts.modal',[
       'id' => 'editPlaceModal',
       'title' => 'Выбрать местоположение',
       'content' => view()->make('backend.ads.parts.places_list'),
       'haveCancel' => true,
       'buttons' => []
    ])
    @include('backend.parts.modal',[
       'id' => 'deleteAdModal',
       'title' => 'Удалить объявление',
       'content' => 'Вы действительно хотите удалить это объявление?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection