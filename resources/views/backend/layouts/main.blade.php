<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $pageTitle }}</title>
    <meta name="viewport" content="width=1200, initial-scale=1">
    <meta name="description" content="{{ $pageDescription }}">
    {!! Html::style(elixir("css/backend.css")); !!}
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYg6N_Li2uIHEEGh80I7eXZ1Blmnou4_4&libraries=places">
    </script>
    {!! Html::script(elixir("js/backend.js")); !!}
</head>
<body>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Домберём</a>
        </div>
        <div id="navbar" class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="#">На сайт</a></li>
                <li><a href="#">Выход</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container page-container">
    <div class="row">
        <div class="col-md-2">
            <div class="panel panel-default main-menu">
                <div class="panel-heading">
                    Главное меню
                </div>
                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li role="presentation" class="{{ Menu::activeLink(['getAdminCp']) }}"><a href="{{ route('getAdminCp') }}">Консоль</a></li>
                        <li role="presentation" class="dropdown {{Menu::activeLink(['getAdminUsers','getAdminUsersDeleted', 'getAdminUser'])}}">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                Пользователи <i class="fa fa-caret-right"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation" class="{{Menu::activeLink(['getAdminUsers'])}}"><a href="{{ route('getAdminUsers') }}">Все пользователи</a></li>
                                <li role="presentation" class="{{Menu::activeLink(['getAdminUsersDeleted'])}}"><a href="{{ route('getAdminUsersDeleted') }}">Удаленные пользователи</a></li>
                            </ul>
                        </li>
                        <li role="presentation" class="dropdown {{Menu::activeLink(['getAdminAds','getAdminAdsDeleted', 'getAdminCreateAd', 'getAdminAd', 'getAdminAdsTypes'])}}">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                Объявления <i class="fa fa-caret-right"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation" class="{{Menu::activeLink(['getAdminAds'])}}"><a href="{{ route('getAdminAds') }}">Все объявления</a></li>
                                <li role="presentation" class="{{Menu::activeLink(['getAdminAdsDeleted'])}}"><a href="{{ route('getAdminAdsDeleted') }}">Удаленные объявления</a></li>
                                <li role="presentation" class="{{Menu::activeLink(['getAdminAdsTypes'])}}"><a href="{{ route('getAdminAdsTypes') }}">Типы объявлений</a></li>
                            </ul>
                        </li>
                        <li role="presentation" class="dropdown {{Menu::activeLink(['getAdminAgencies', 'getAdminCreateAgency', 'getAdminAgenciesDeleted', 'getAdminAgency'])}}">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                Агентства <i class="fa fa-caret-right"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation" class=""><a href="{{route('getAdminAgencies')}}">Все агентства</a></li>
                                <li role="presentation" class=""><a href="{{route('getAdminAgenciesDeleted')}}">Удаленные агентства</a></li>
                            </ul>
                        </li>
                        <li role="presentation" class="{{ Menu::activeLink(['getAdminPages']) }}"><a href="{{ route('getAdminPages') }}">Страницы</a></li>
                        <li role="presentation" class="dropdown {{Menu::activeLink(['getAdminPostsCategories', 'getAdminCreatePostCategory', 'getAdminPostCategory', 'getAdminPosts', 'getAdminCreatePost', 'getAdminCreatePost', 'getAdminPost'])}}">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                Статьи <i class="fa fa-caret-right"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a href="{{route('getAdminPosts')}}">Все статьи</a></li>
                                <li role="presentation"><a href="{{route('getAdminPostsCategories')}}">Рубрики статей</a></li>
                            </ul>
                        </li>
                        <li role="presentation" class="{{ Menu::activeLink(['getAdminSettings', 'getAdminParameter']) }}"><a href="{{route('getAdminSettings')}}">Настройки</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-xs-12 page-title">
                    <h1>{{ $pageTitle }}</h1>
                    <p>{{ $pageDescription }}</p>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 page-messages">
                    {!! Notification::showAll() !!}
                    @if($errors->all())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                {{$error}}<br>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            @yield('content')
        </div>
    </div>
</div>
@include('backend.parts.modal',[
   'id' => 'deleteUserModal',
   'title' => 'Удалить пользователя',
   'content' => 'Вы действительно хотите удалить этого пользователя?',
   'haveCancel' => true,
   'buttons' => [
        '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
   ]
])
</body>