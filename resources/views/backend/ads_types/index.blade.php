@extends('backend.layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <ul class="ad-types-list">
                @foreach($types as $id => $name)
                    <li>
                        <a href="{{route('getAdminAdsTypes', ['id' => $id])}}" @if($id == $currentTypeId) selected @endif>{{$name}}</a>
                    </li>
                @endforeach
            </ul>
            <a href="#" class="btn btn-block btn-default"><i class="fa fa-plus"></i> добавить новый тип</a>
        </div>
        <div class="col-xs-8">
            @if(!empty($currentType))
                <form>
                    <div class="form-group">
                        <label for="parent_type">Родительский тип</label>
                        <select id="parent_type" class="selectpicker filter-select show-menu-arrow form-control">
                            <option value="">Тип не выбран</option>
                            @foreach($types as $id => $name)
                                <option value="{{route('getAdminAdsTypes', ['id' => $id])}}" @if($id == $currentType->parent_id) selected @endif>{{$name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="type_data">Данные типа:</label>
                        <textarea id="type_data" class="form-control" rows="20">{{json_encode(json_decode($currentType->fields),JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE)}}</textarea>
                    </div>
                    <div class="text-right">
                        <button type="button" data-toggle="modal" data-url="#" data-target="#deleteTypeModal" class="btn btn-danger">Удалить</button>
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </form>
            @else
                сначала выберите тип объявлений
            @endif
        </div>
    </div>
    @include('backend.parts.modal',[
       'id' => 'deleteTypeModal',
       'title' => 'Удалить тип',
       'content' => 'Вы действительно хотите удалить этот тип вместе со всеми объявлениями и дочерними типами?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection