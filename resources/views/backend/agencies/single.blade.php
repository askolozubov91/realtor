@extends('backend.layouts.main')

@section('content')
    <div class="row">
        @if(empty($agency->id))
            {!! Form::open(['url'=>route('postAdminSaveAgency')]) !!}
        @else
            {!! Form::open(['url'=>route('postAdminSaveAgency', $agency->id)]) !!}
        @endif
            <div class="col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Данные Агентства</div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Название'); !!}
                            {!! Form::text('name',$agency->name,['class'=>'form-control']) !!}
                            <p class="help-block">Название агентства или компании</p>
                        </div>
                        <div class="form-group">
                            {!! Form::label('users', 'Пользователи'); !!}
                            <div class="duplicate-wrapper">
                                <?php if(Input::old('users')): $agency->users = Input::old('users'); endif; ?>
                                @forelse($agency->users as $num => $user)
                                        <div class="row duplicate">
                                            <div class="col-xs-9">
                                                @if(Input::old('users'))
                                                    <?php $user = \App\User::findOrNew($user); ?>
                                                @endif
                                                {!! Form::select('users['.$num.']',(!empty($user->id)?[$user->id=>'ID'.$user->id.' '.$user->name]:[]),(!empty($user->id)?$user->id:null),['class'=>'form-control ajax-selectpicker', 'data-abs-ajax-url'=>route('postAdminSearchUsers')]) !!}
                                            </div>
                                            <div class="col-xs-3">
                                                @if($num+1 == count($agency->users))
                                                    <button type="button" class="btn btn-link duplicate-block pull-right"><i class="fa fa-plus"></i> еще один</button>
                                                @else
                                                    <button type='button' class='btn btn-link duplicate-block-remove text-danger pull-right'><i class='fa fa-minus'></i> удалить</button>
                                                @endif
                                            </div>
                                        </div>
                                @empty
                                        <div class="row duplicate">
                                            <div class="col-xs-9">
                                                {!! Form::select('users[0]',[],[],['class'=>'form-control ajax-selectpicker', 'data-abs-ajax-url'=>route('postAdminSearchUsers')]) !!}
                                            </div>
                                            <div class="col-xs-3">
                                                <button type="button" class="btn btn-link duplicate-block pull-right"><i class="fa fa-plus"></i> еще один</button>
                                            </div>
                                        </div>
                                @endforelse
                            </div>
                            <p class="help-block">Пользователи, которые могут управлять агентством</p>
                        </div>
                        <div class="form-group">
                            {!! Form::label('about', 'Описание'); !!}
                            {!! Form::textarea('about',$agency->about,['class'=>'form-control']) !!}
                            <p class="help-block">Описание агентства или компании</p>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Контакты</div>
                    <div class="panel-body">
                        <div class="duplicate-wrapper">
                            <?php if(Input::old('contacts')): $agency->contacts = Input::old('contacts'); endif; ?>
                            @forelse($agency->contacts as $num => $contact)
                                <div class="row duplicate">
                                    <div class="col-xs-4">
                                        {!! Form::select('contacts['.$num.'][contact_type]', $contactsTypes, $contact['contact_type'],['class'=>'form-control selectpicker']) !!}
                                    </div>
                                    <div class="col-xs-5">
                                        {!! Form::text('contacts['.$num.'][value]', $contact['value'] ,['class'=>'form-control']) !!}
                                    </div>
                                    <div class="col-xs-3">
                                        @if($num+1 == count($agency->contacts))
                                            <button type="button" class="btn btn-link duplicate-block pull-right"><i class="fa fa-plus"></i> еще один</button>
                                        @else
                                            <button type='button' class='btn btn-link duplicate-block-remove text-danger pull-right'><i class='fa fa-minus'></i> удалить</button>
                                        @endif
                                    </div>
                                </div>
                            @empty
                                <div class="row duplicate">
                                    <div class="col-xs-4">
                                        {!! Form::select('contacts[0][contact_type]', $contactsTypes, [],['class'=>'form-control selectpicker']) !!}
                                    </div>
                                    <div class="col-xs-5">
                                        {!! Form::text('contacts[0][value]', null,['class'=>'form-control']) !!}
                                    </div>
                                    <div class="col-xs-3">
                                        <button type="button" class="btn btn-link duplicate-block pull-right"><i class="fa fa-plus"></i> еще один</button>
                                    </div>
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div class="duplicate-wrapper">
                <?php if(Input::old('places')): $agency->addresses = Input::old('places'); endif; ?>
                @forelse($agency->addresses as $i => $address)
                        <div class="panel panel-default duplicate">
                            <div class="panel-heading">
                                Местоположение
                                @if($i+1 == count($agency->addresses))
                                    <button type="button" class="btn btn-link duplicate-block pull-right"><i class="fa fa-plus"></i> еще один адрес</button>
                                @else
                                    <button type='button' class='btn btn-link duplicate-block-remove text-danger pull-right'><i class='fa fa-minus'></i> удалить</button>
                                @endif
                            </div>
                            <div class="panel-body select-place-block">
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('places['.$i.'][selected_place]',$address['selected_place'],['class'=>'form-control selected-place', 'readonly' => 'readonly', 'placeholder'=>'Нажмите на кнопку "выбрать"...', 'id' => 'place-map']) !!}
                                        {!! Form::hidden('places['.$i.'][primary_place]',$address['primary_place'], ['class' => 'primaryPlace']) !!}
                                        {!! Form::hidden('places['.$i.'][search_place_data]',$address['search_place_data'], ['class' => 'searchPlaceData']) !!}
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="button" data-toggle="modal"  data-target="#editPlaceModal">выбрать</button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                        {!! Form::label('address', 'Адрес'); !!}
                                        <div class="alert alert-info text-center empty-place @if(!empty($address['primary_place']) or Input::old('primary_place')) hidden @endif">Сначала выберите местоположение.</div>
                                        <div class="address-field @if(empty($address['primary_place']) and !Input::old('primary_place')) hidden @endif">
                                            {!! Form::text('places['.$i.'][address]',$address['address'],['class'=>'form-control address-map', 'placeholder' => 'Введите адрес...']) !!}
                                        </div>
                                </div>
                                <div class="map-wrapper @if(empty($address['address']) and !Input::old('address')) hidden @endif">
                                        {!! Form::hidden('places['.$i.'][latitude]', $address['latitude'], ['class' => 'latitude']) !!}
                                        {!! Form::hidden('places['.$i.'][longitude]', $address['longitude'], ['class' => 'longitude']) !!}
                                        <div class="map-canvas" style="height: 300px;"></div>
                                        <p class="help-block">Перетащите маркер в нужное место, если он расположен неправильно.</p>
                                </div>
                            </div>
                        </div>
                @empty
                        <div class="panel panel-default duplicate">
                            <div class="panel-heading">Местоположение <button type="button" class="btn btn-link duplicate-block pull-right"><i class="fa fa-plus"></i> еще один адрес</button></div>
                            <div class="panel-body select-place-block">
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('places[0][selected_place]',null,['class'=>'form-control selected-place', 'readonly' => 'readonly', 'placeholder'=>'Нажмите на кнопку "выбрать"...', 'id' => 'place-map']) !!}
                                        {!! Form::hidden('places[0][primary_place]',null, ['class' => 'primaryPlace']) !!}
                                        {!! Form::hidden('places[0][search_place_data]',null, ['class' => 'searchPlaceData']) !!}
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button" data-toggle="modal"  data-target="#editPlaceModal">выбрать</button>
                                    </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('address', 'Адрес'); !!}
                                    <div class="alert alert-info text-center empty-place @if(Input::old('primary_place')) hidden @endif">Сначала выберите местоположение.</div>
                                    <div class="address-field @if(!Input::old('primary_place')) hidden @endif">
                                        {!! Form::text('places[0][address]',null,['class'=>'form-control address-map', 'placeholder' => 'Введите адрес...']) !!}
                                    </div>
                                </div>
                                <div class="map-wrapper @if(!Input::old('address')) hidden @endif">
                                    {!! Form::hidden('places[0][latitude]', null, ['class' => 'latitude']) !!}
                                    {!! Form::hidden('places[0][longitude]', null, ['class' => 'longitude']) !!}
                                    <div class="map-canvas" style="height: 300px;"></div>
                                    <p class="help-block">Перетащите маркер в нужное место, если он расположен неправильно.</p>
                                </div>
                            </div>
                        </div>
                @endforelse
                </div>
            </div>
            <div class="col-xs-4">
                @if(!empty($agency->id))
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Информация
                        </div>
                        <div class="panel-body small-text">
                            <p><i class="fa fa-clock-o"></i> Создано: <span class="format-date-time">{{$agency->created_at}}</span></p>
                            <p><i class="fa fa-clock-o"></i> Обновлено: <span class="format-date-time">{{$agency->created_at}}</span></p>
                        </div>
                    </div>
                @endif
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Логотип
                        </div>
                        <div class="panel-body small-text">
                            <div class="fileinput-button btn-block">
                                <span class="btn btn-primary btn-block"><i class="fa fa-upload"></i> загрузить</span>
                                <input id="imageSingleUpload" type="file" name="file" data-url="{{route('postAdminUploadImage')}}">
                            </div>
                            <div class="row images-gallery">
                                @if(!empty($agency->image->id))
                                    <div class="col-xs-8 col-xs-offset-2 gallery-item">
                                        <button type="button" class="close delete-gallery-image" data-url="'+data.result.delete_url+'"><i class="fa fa-times"></i></button>
                                        <a href="{{ImageResize::make($agency->image->path, ['w'=>1920, 'h'=>1920])}}" class="thumbnail"><img src="{{ImageResize::make($agency->image->path, ['w'=>150, 'h'=>150, 'c'=>true ])}}" alt="..."></a><input type="hidden" name="logo_image_id" value="{{$agency->image->id}}">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">Управление</div>
                    <div class="panel-body">
                        <p><button type="submit" class="btn btn-success btn-block">Сохранить</button></p>
                        @if(!empty($agency->id))
                            <p class="text-center"><a href="#" class="text-danger" data-toggle="modal" data-url="{{route('getAdminDeleteAgency',$agency->id)}}" data-target="#deleteAgencyModal"><i class="fa fa-close"></i> удалить</a></p>
                        @endif
                    </div>
                </div>
            </div>
        {!! Form::close(); !!}
    </div>
    @include('backend.parts.modal',[
       'id' => 'editPlaceModal',
       'title' => 'Выбрать местоположение',
       'content' => view()->make('backend.ads.parts.places_list'),
       'haveCancel' => true,
       'buttons' => []
    ])
    @include('backend.parts.modal',[
       'id' => 'deleteAgencyModal',
       'title' => 'Удалить агентство',
       'content' => 'Вы действительно хотите удалить это агентство?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection