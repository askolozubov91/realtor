@extends('backend.layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('getAdminCreateAgency')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> добавить агентство</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Фильтр <a class="pull-right btn btn-default btn-xs" role="button" data-toggle="collapse" href="#collapseFiltes" aria-expanded="false" aria-controls="collapseFiltes">показать/скрыть</a>
                </div>
                <?php $input = Input::all(); ?>
                <div id="collapseFiltes" class="panel-collapse collapse @if(!empty($input)) in @endif"  aria-labelledby="collapseFiltes">
                    <div class="panel-body">
                        {!! Form::open(['url' => route((Route::getCurrentRoute()->getName() == 'getAdminAgenciesDeleted')?'getAdminAgenciesDeleted':'getAdminAgencies'), 'method' => 'get']); !!}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('filterPlace', 'Местоположение'); !!}
                                    {!! Form::select('place', [''=>'Все']+$places, Request::get('place'), ['class' => 'selectpicker filter-select show-menu-arrow form-control datatable-filter', 'id' => 'filterPlace', 'data-live-search'=>true]); !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('filterUser', 'Пользователь'); !!}
                                    {!! Form::select('user',(!empty($userSelectedInFilter)?[$userSelectedInFilter->id => 'ID'.$userSelectedInFilter->id.' '.$userSelectedInFilter->name]:[]),(!empty($userSelectedInFilter)?$userSelectedInFilter->id:null),['class'=>'form-control ajax-selectpicker datatable-filter filter-select', 'data-abs-ajax-url'=>route('postAdminSearchUsers'), 'id'=>'filterUser']) !!}
                                </div>
                            </div>
                            <div class="col-md-4 filter-button">
                                {!! Form::submit('показать', ['class'=>'btn btn-primary btn-block']); !!}
                            </div>
                        </div>
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table width="100%" class="table table-bordered table-striped" id="agencies-table" data-url="{{(route('getAdminAgenciesDatatable',(Route::getCurrentRoute()->getName() == 'getAdminAgenciesDeleted')?'deleted':null))}}">
                <thead>
                <tr>
                    <th>№</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Название</th>
                    <th>Контакты</th>
                    <th>Пользователи</th>
                    <th>Объявления</th>
                    <th>Создано</th>
                    <th>Обновлено</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => route('postAdminAgenciesSelectedAction'), 'class'=>'form-inline action-datatable-form']); !!}
                    <div class="row">
                        <div class="col-md-8">
                            @if(Route::getCurrentRoute()->getName() == 'getAdminAgenciesDeleted')
                                {!! Form::select('action', [
                                'restore' => 'Восстановить',
                                'deleteForever'=>'Удалить навсегда',
                                ], null, ['class' => 'selectpicker show-menu-arrow form-control', 'id' => 'selectedAction']); !!}
                            @else
                                {!! Form::select('action', [
                                'delete'=>'Удалить',
                                ], null, ['class' => 'selectpicker show-menu-arrow form-control', 'id' => 'selectedAction']); !!}
                            @endif
                        </div>
                        <div class="col-md-4">
                            {!! Form::submit('применить к выбранным', ['class'=>'btn btn-primary btn-block']); !!}
                        </div>
                    </div>
                    {!! Form::close(); !!}
                </div>
            </div>
        </div>
    </div>
    @include('backend.parts.modal',[
       'id' => 'deleteAgencyModal',
       'title' => 'Удалить агентство',
       'content' => 'Вы действительно хотите удалить это агентство?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection