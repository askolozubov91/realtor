@extends('backend.layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('getAdminCreatePage')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> добавить страницу</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table width="100%" class="table table-bordered table-striped" id="pages-table" data-url="{{route('getAdminPagesDatatable')}}">
                <thead>
                <tr>
                    <th>№</th>
                    <th></th>
                    <th>Заголовок</th>
                    <th>Текст</th>
                    <th>Родительская страница</th>
                    <th>Создана</th>
                    <th>Обновлена</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    @include('backend.parts.modal',[
       'id' => 'deletePageModal',
       'title' => 'Удалить страницу',
       'content' => 'Вы действительно хотите удалить эту страницу?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection