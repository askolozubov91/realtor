@extends('backend.layouts.main')

@section('content')
    <div class="row">
        @if(empty($page->id))
            {!! Form::open(['url'=>route('postAdminSavePage')]) !!}
        @else
            {!! Form::open(['url'=>route('postAdminSavePage', $page->id)]) !!}
        @endif
            <div class="col-xs-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Данные страницы</div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('title', 'Заголовок страницы'); !!}
                            {!! Form::text('title',$page->title,['class'=>'form-control']) !!}
                            <p class="help-block">Заголовок страницы, который отображается пользователям</p>
                        </div>
                        <div class="form-group">
                            {!! Form::label('parentPage', 'Родительская страница') !!}
                            {!! Form::select('parent_id', [''=>'нет'] + $pages, $page->parent_id, ['class' => 'selectpicker show-menu-arrow form-control', 'id' => 'parentPage', 'data-style' => 'btn-default']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('content', 'Текст'); !!}
                            {!! Form::textarea('content',$page->content,['class'=>'form-control froala-editor']) !!}
                            <p class="help-block">Содержимое страницы</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                @if(!empty($page->id))
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Информация
                        </div>
                        <div class="panel-body small-text">
                            <p><i class="fa fa-clock-o"></i> Создана: <span class="format-date-time">{{$page->created_at}}</span></p>
                            <p><i class="fa fa-clock-o"></i> Обновлена: <span class="format-date-time">{{$page->created_at}}</span></p>
                        </div>
                    </div>
                @endif
                <div class="panel panel-primary">
                    <div class="panel-heading">Управление</div>
                    <div class="panel-body">
                        <p><button type="submit" class="btn btn-success btn-block">Сохранить</button></p>
                        @if(!empty($page->id))
                            <p class="text-center"><a href="#" class="text-danger" data-toggle="modal" data-url="{{route('getAdminDeletePage',$page->id)}}" data-target="#deletePageModal"><i class="fa  fa-close"></i> удалить</a></p>
                        @endif
                    </div>
                </div>
            </div>
        {!! Form::close(); !!}
    </div>
    @include('backend.parts.modal',[
       'id' => 'deletePageModal',
       'title' => 'Удалить страницу',
       'content' => 'Вы действительно хотите удалить эту страницу?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection