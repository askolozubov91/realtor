@extends('backend.layouts.main')

@section('content')
    <div class="row">
        @if(empty($category->id))
            {!! Form::open(['url'=>route('postAdminSavePostCategory')]) !!}
        @else
            {!! Form::open(['url'=>route('postAdminSavePostCategory', $category->id)]) !!}
        @endif
            <div class="col-xs-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Данные категории</div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('title', 'Название категории'); !!}
                            {!! Form::text('title',$category->title,['class'=>'form-control']) !!}
                            <p class="help-block">Название категории, которое отображается пользователям</p>
                        </div>
                        <div class="form-group">
                            {!! Form::label('parentCategory', 'Родительская категория') !!}
                            {!! Form::select('parent_id', [''=>'нет'] + $categories, $category->parent_id, ['class' => 'selectpicker show-menu-arrow form-control', 'id' => 'parentCategory', 'data-style' => 'btn-default']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">Управление</div>
                    <div class="panel-body">
                        <p><button type="submit" class="btn btn-success btn-block">Сохранить</button></p>
                        @if(!empty($category->id))
                            <p class="text-center"><a href="#" class="text-danger" data-toggle="modal" data-url="{{route('getAdminPostCategoryDelete',$category->id)}}" data-target="#deleteCategoryModal"><i class="fa  fa-close"></i> удалить</a></p>
                        @endif
                    </div>
                </div>
            </div>
        {!! Form::close(); !!}
    </div>
    @include('backend.parts.modal',[
       'id' => 'deleteCategoryModal',
       'title' => 'Удалить категорию',
       'content' => 'Вы действительно хотите удалить эту категорию?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection