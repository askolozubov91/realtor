@extends('backend.layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-9">
            <div class="categories-list">
                <?php $depth = -1; ?>
                @foreach($categories as $category)
                    @if($category['depth'] == $depth)
                        </li><li>
                            @elseif($category['depth'] > $depth)
                                <ul><li>
                                        @elseif($category['depth'] < $depth)
                                    </li></ul><li>
                            @endif
                            <div class="categories-list-item">
                                {{$category['title']}}
                                <a href="#" class="btn btn-danger pull-right btn-xs" data-toggle="modal" data-url="{{route('getAdminPostCategoryDelete', $category['id'])}}" data-target="#deleteCategoryModal"><i class="fa fa-times"></i></a>
                                <a href="{{route('getAdminPostCategory',$category['id'])}}" class="btn btn-primary pull-right btn-xs"><i class="fa fa-pencil"></i></a>
                            </div>
                            <?php $depth = $category['depth']; ?>
                            @endforeach
                            @for($i = 0; $i <= $depth; $i++)
                        </li></ul>
                        @endfor
            </div>
        </div>
        <div class="col-xs-3">
            <a href="{{route('getAdminCreatePostCategory')}}" class="btn btn-success btn-block"><i class="fa fa-plus"></i> добавить</a>
        </div>
    </div>
    @include('backend.parts.modal',[
       'id' => 'deleteCategoryModal',
       'title' => 'Удалить категорию',
       'content' => 'Вы действительно хотите удалить эту категорию?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection
