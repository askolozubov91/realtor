@extends('backend.layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <a href="{{route('getAdminCreatePost')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> добавить статью</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Фильтр <a class="pull-right btn btn-default btn-xs" role="button" data-toggle="collapse" href="#collapseFiltes" aria-expanded="false" aria-controls="collapseFiltes">показать/скрыть</a>
                </div>
                <?php $input = Input::all(); ?>
                <div id="collapseFiltes" class="panel-collapse collapse @if(!empty($input)) in @endif"  aria-labelledby="collapseFiltes">
                    <div class="panel-body">
                        {!! Form::open(['url' => route('getAdminPosts'), 'method' => 'get']); !!}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    {!! Form::label('filterCategory', 'Рубрика') !!}
                                    {!! Form::select('category', [''=>'Все'] + $categories, Request::get('category'), ['class' => 'selectpicker filer-select show-menu-arrow form-control datatable-filter', 'id' => 'filterCategory', 'data-style' => 'btn-primary']) !!}
                                </div>
                            </div>
                            <div class="col-md-4 filter-button">
                                {!! Form::submit('показать', ['class'=>'btn btn-primary btn-block']); !!}
                            </div>
                        </div>
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered table-striped" width="100%" id="posts-table" data-url="{{route('getAdminPostsDatatable')}}">
                <thead>
                <tr>
                    <th>№</th>
                    <th></th>
                    <th></th>
                    <th>Заголовок</th>
                    <th>Текст</th>
                    <th>Рубрики</th>
                    <th>Создана</th>
                    <th>Обновлена</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    @include('backend.parts.modal',[
       'id' => 'deletePostModal',
       'title' => 'Удалить статью',
       'content' => 'Вы действительно хотите удалить эту статью?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection