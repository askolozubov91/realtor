@extends('backend.layouts.main')

@section('content')
    <div class="row">
        @if(empty($post->id))
            {!! Form::open(['url'=>route('postAdminSavePost')]) !!}
        @else
            {!! Form::open(['url'=>route('postAdminSavePost', $post->id)]) !!}
        @endif
            <div class="col-xs-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Данные страницы</div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('title', 'Заголовок статьи'); !!}
                            {!! Form::text('title',$post->title,['class'=>'form-control']) !!}
                            <p class="help-block">Заголовок статьи, который отображается пользователям</p>
                        </div>
                        <div class="form-group">
                            {!! Form::label('categories', 'Рубрики') !!}
                            {!! Form::select('categories[]', $categories, $postCategoriesIds, ['class' => 'selectpicker show-menu-arrow form-control', 'id' => 'categories', 'data-style' => 'btn-default', 'multiple'=>'multiple']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('content', 'Текст'); !!}
                            {!! Form::textarea('content',$post->content,['class'=>'form-control froala-editor']) !!}
                            <p class="help-block">Содержимое статьи</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                @if(!empty($post->id))
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Информация
                        </div>
                        <div class="panel-body small-text">
                            <p><i class="fa fa-clock-o"></i> Создана: <span class="format-date-time">{{$post->created_at}}</span></p>
                            <p><i class="fa fa-clock-o"></i> Обновлена: <span class="format-date-time">{{$post->created_at}}</span></p>
                        </div>
                    </div>
                @endif
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Изображение
                        </div>
                        <div class="panel-body small-text">
                            <div class="fileinput-button btn-block">
                                <span class="btn btn-primary btn-block"><i class="fa fa-upload"></i> загрузить</span>
                                <input id="imageSingleUpload" type="file" name="file" data-url="{{route('postAdminUploadImage')}}">
                            </div>
                            <div class="row images-gallery" data-field-name="image_id">
                                @if(!empty($post->image->id))
                                    <div class="col-xs-8 col-xs-offset-2 gallery-item">
                                        <button type="button" class="close delete-gallery-image" data-url="'+data.result.delete_url+'"><i class="fa fa-times"></i></button>
                                        <a href="{{ImageResize::make($post->image->path, ['w'=>1920, 'h'=>1920])}}" class="thumbnail"><img src="{{ImageResize::make($post->image->path, ['w'=>150, 'h'=>150, 'c'=>true ])}}" alt="..."></a><input type="hidden" name="image_id" value="{{$post->image->id}}">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">Управление</div>
                    <div class="panel-body">
                        <p><button type="submit" class="btn btn-success btn-block">Сохранить</button></p>
                        @if(!empty($post->id))
                            <p class="text-center"><a href="#" class="text-danger" data-toggle="modal" data-url="{{route('getAdminPostDelete',$post->id)}}" data-target="#deletePostModal"><i class="fa  fa-close"></i> удалить</a></p>
                        @endif
                    </div>
                </div>
            </div>
        {!! Form::close(); !!}
    </div>
    @include('backend.parts.modal',[
       'id' => 'deletePostModal',
       'title' => 'Удалить статью',
       'content' => 'Вы действительно хотите удалить эту статью?',
       'haveCancel' => true,
       'buttons' => [
            '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
       ]
    ])
@endsection