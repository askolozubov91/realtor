@extends('backend.layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Фильтр <a class="pull-right btn btn-default btn-xs" role="button" data-toggle="collapse" href="#collapseFiltes" aria-expanded="false" aria-controls="collapseFiltes">показать/скрыть</a>
                </div>
                <?php $input = Input::all(); ?>
                <div id="collapseFiltes" class="panel-collapse collapse @if(!empty($input)) in @endif"  aria-labelledby="collapseFiltes">
                    <div class="panel-body">
                    {!! Form::open(['url' => route((Route::getCurrentRoute()->getName() == 'getAdminUsersDeleted')?'getAdminUsersDeleted':'getAdminUsers'), 'method' => 'get']); !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('filterRole', 'Роль пользователя'); !!}
                                    {!! Form::select('role', [''=>'Все'] + $roles, Request::get('role'), ['class' => 'selectpicker filter-select show-menu-arrow form-control datatable-filter', 'id' => 'filterRole']); !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('filterType', 'Тип пользователя'); !!}
                                    {!! Form::select('type', [''=>'Все'] + $users_types, Request::get('type'), ['class' => 'selectpicker filter-select show-menu-arrow form-control datatable-filter', 'id' => 'filterType']); !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('confirmed_email', '1', Request::get('confirmed_email'), ['class'=>'datatable-filter']); !!} E-mail подтвержден
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('confirmed_phone', '1', Request::get('confirmed_phone'), ['class'=>'datatable-filter']); !!} телефон подтвержден
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('have_agency', '1', Request::get('have_agency'), ['class'=>'datatable-filter']); !!} есть агентство
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                {!! Form::submit('показать', ['class'=>'btn btn-primary btn-block']); !!}
                            </div>
                        </div>
                    {!! Form::close(); !!}
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table width="100%" class="table table-bordered table-striped" id="users-table" data-url="{{(route('getAdminUsersDatatable',(Route::getCurrentRoute()->getName() == 'getAdminUsersDeleted')?'deleted':null))}}">
                <thead>
                <tr>
                    <th>№</th>
                    <th></th>
                    <th>Действия</th>
                    <th>Имя</th>
                    <th>Объявления</th>
                    <th>Контакты</th>
                    <th>Тип</th>
                    <th>Роль</th>
                    <th>Баланс</th>
                    <th>Создан</th>
                    <th>Обновлен</th>
                    <th>Телефон</th>
                    <th>E-mail</th>
                    <th>Компания</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['url' => route('postAdminUsersSelectedAction'), 'class'=>'form-inline action-datatable-form']); !!}
                    <div class="row">
                        @if(Route::getCurrentRoute()->getName() == 'getAdminUsersDeleted')
                            <div class="col-md-8">
                                {!! Form::select('action', ['deleteForever'=>'Удалить навсегда', 'restore' => 'Восстановить'], null, ['class' => 'selectpicker show-menu-arrow form-control', 'id' => 'selectedAction']); !!}
                            </div>
                        @else
                            <div class="col-md-8">
                                {!! Form::select('action', ['delete'=>'Удалить', 'block' => 'Заблокировать', 'unblock' => 'Разблокировать'], null, ['class' => 'selectpicker show-menu-arrow form-control', 'id' => 'selectedAction']); !!}
                            </div>
                        @endif
                        <div class="col-md-4">
                            {!! Form::submit('применить к выбранным', ['class'=>'btn btn-primary btn-block']); !!}
                        </div>
                    </div>
                    {!! Form::close(); !!}
                </div>
            </div>
        </div>
    </div>
@endsection