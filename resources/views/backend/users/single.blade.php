@extends('backend.layouts.main')

@section('content')
    <div class="row">
        {!! Form::open(['url' => route('postAdminUpdateUser', $user->id)]); !!}
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Данные пользователя
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('userName', 'Имя пользователя'); !!}
                            {!! Form::text('name', $user->name, ['class'=>'form-control', 'id'=>'userName', $disabledForm]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('userEmail', 'E-mail пользователя'); !!}
                            {!! Form::email('email', $user->email, ['class'=>'form-control', 'id'=>'userEmail', $disabledForm]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('userPhone', 'Телефон пользователя'); !!}
                            {!! Form::text('phone', $user->phone, ['class'=>'form-control', 'id'=>'userPhone', $disabledForm]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('userCompanyName', 'Компания пользователя'); !!}
                            {!! Form::text('company_name', $user->company_name, ['class'=>'form-control', 'id'=>'userCompanyName', $disabledForm]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('userType', 'Тип пользователя'); !!}
                            {!! Form::select('user_type', $users_types, $user->user_type, ['class'=>'form-control', 'id'=>'userType', $disabledForm]); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('userMoney', 'Баланс пользователя'); !!}
                            {!! Form::text('money', $user->money, ['class'=>'form-control', 'id'=>'userMoney', $disabledForm]); !!}
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::hidden('confirmed_email', 0) !!}
                                        {!! Form::checkbox('confirmed_email', '1', $user->confirmed_email, [$disabledForm]); !!} email подтвержден
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {!! Form::hidden('confirmed_phone', 0) !!}
                                        {!! Form::checkbox('confirmed_phone', '1', $user->confirmed_phone, [$disabledForm]); !!} телефон подтвержден
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::hidden('send_notifications_email', 0) !!}
                                        {!! Form::checkbox('send_notifications_email', '1', $user->send_notifications_email, [$disabledForm]); !!} отправка уведомлений на email
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {!! Form::hidden('send_notifications_phone', 0) !!}
                                        {!! Form::checkbox('send_notifications_phone', '1', $user->send_notifications_phone, [$disabledForm]); !!} отправка уведомлений на телефон
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {!! Form::hidden('send_news', 0) !!}
                                        {!! Form::checkbox('send_news', '1', $user->send_news, [$disabledForm]); !!} новостная рассылка на email
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @if($myUser->id != $user->id)
                        <div class="form-group">
                            {!! Form::label('userRole', 'Роль пользователя'); !!}
                            {!! Form::select('role', $roles, $user->roles[0]->id, ['class'=>'form-control', 'id'=>'userRole', $disabledForm]); !!}
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('userPassword', 'Пароль') !!}
                                    {!! Form::password('password', ['id' => 'userPassword', 'class' => 'form-control', 'aria-describedby' =>'helpBlockUserPassword', $disabledForm]) !!}
                                    <span id="helpBlockUserPassword" class="help-block">Оставьте поле пустым, если не хотите менять пароль.</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('userPasswordConfirm', 'Подтверждение пароля') !!}
                                    {!! Form::password('password_confirm', ['id' => 'userPasswordConfirm', 'class' => 'form-control', 'aria-describedby' =>'helpBlockUserPasswordConfirm', $disabledForm]) !!}
                                    <span id="helpBlockUserPasswordConfirm" class="help-block">Оставьте поле пустым, если не хотите менять пароль.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Информация
                    </div>
                    <div class="panel-body small-text">
                        <p><i class="fa fa-clock-o"></i> Создан: <span class="format-date-time">{{$user->created_at}}</span></p>
                        <p><i class="fa fa-clock-o"></i> Обновлен: <span class="format-date-time">{{$user->updated_at}}</span></p>
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Управление
                    </div>
                    <div class="panel-body text-center">
                        <p>{!! Form::submit('Сохранить', ['class' => 'btn btn-success btn-block', $disabledForm]) !!}</p>
                        @if($disabledForm === 'disabled')
                            <p>Вы не можете редактировать или удалить этого пользователя поскольку его роль: <strong>"{{$user->roles[0]->display_name}}"</strong>, а ваша <strong>"{{$myUser->roles[0]->display_name}}"</strong>.</p>
                        @else
                            <p><a href="#" class="text-danger" data-toggle="modal" data-url="{{route('getAdminDeleteUser',$user->id)}}" data-target="#deleteUserModal"><i class="fa  fa-close"></i> удалить</a></p>
                        @endif
                    </div>
                </div>
            </div>
        {!! Form::close(); !!}
    </div>
@endsection