@extends('backend.layouts.main')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered table-striped" width="100%" id="settings-table" data-url="{{route('getAdminSettingsDatatable')}}">
                <thead>
                    <tr>
                        <th>№</th>
                        <th></th>
                        <th>Название</th>
                        <th>Параметр</th>
                        <th>Значение</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <hr>
        </div>
    </div>
@endsection