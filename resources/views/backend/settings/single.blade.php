@extends('backend.layouts.main')

@section('content')
    <div class="row">
        {!! Form::open(['url'=>route('getAdminSaveParameter', $param->id)]) !!}
            <div class="col-xs-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Данные параметра</div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('value', $param->display_name); !!}
                            {!! Form::text('value',$param->value,['class'=>'form-control']) !!}
                            <p class="help-block">Значение параметра</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">Управление</div>
                    <div class="panel-body">
                        <p><button type="submit" class="btn btn-success btn-block">Сохранить</button></p>
                    </div>
                </div>
            </div>
        {!! Form::close(); !!}
    </div>
@endsection