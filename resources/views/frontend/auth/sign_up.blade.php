@extends('frontend.layouts.main')

@section('content')
    {!! print_r($errors) !!}}
    <div class="content">
        {!! Form::open(['url' => route('postSignUp')]) !!}
            <div class="form-group">
                {!! Form::label('signUpName', 'Ваше имя'); !!}
                {!! Form::text('name',null,['class' => 'form-control', 'id' => 'signUpName', 'placeholder' => 'Как к вам обращаться...']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('signUpCompany', 'Название вашей компании'); !!}
                {!! Form::text('company_name',null,['class' => 'form-control', 'id' => 'signUpCompany', 'placeholder' => 'Название компании или агентства, которое вы предсталяете...']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('signUpEmail', 'Ваш адрес электронной почты'); !!}
                {!! Form::email('email',null,['class' => 'form-control', 'id' => 'signUpEmail', 'placeholder' => 'Куда направлять электронную почту...']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('signUpPhone', 'Ваш номер мобильного телефона'); !!}
                {!! Form::text('phone',null,['class' => 'form-control', 'id' => 'signUpPhone', 'placeholder' => 'Куда отправлять смс-уведомления...']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('signUpPassword', 'Пароль'); !!}
                {!! Form::password('password',['class' => 'form-control', 'id' => 'signUpPassword']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('signUpPasswordConfirm', 'Повторите пароль'); !!}
                {!! Form::password('passwordConfirm',['class' => 'form-control', 'id' => 'signUpPasswordConfirm']) !!}
            </div>
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('rulesAgree', 1, true) !!} Я согласен с <a href="#">правилами сайта</a>
                </label>
            </div>
            {!! Form::submit('Зарегистрироваться',['class'=>'btn btn-default']); !!}
        {!! Form::close() !!}
    </div>
@endsection
