@extends('frontend.layouts.main')

@section('content')
    {!! Notification::showAll() !!}
    <div class="content">
        {!! Form::open(['url' => route('postLogIn')]) !!}
        <div class="form-group">
            {!! Form::label('emailOrPhone', 'Номер телефона или email'); !!}
            {!! Form::text('emailOrPhone',null,['class' => 'form-control', 'id' => 'emailOrPhone', 'placeholder' => 'Ваш email или номер телефона...']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('signUpPassword', 'Пароль'); !!}
            {!! Form::password('password',['class' => 'form-control', 'id' => 'signUpPassword']) !!}
        </div>
        <div class="checkbox">
            <label>
                {!! Form::checkbox('rememberMe', 1, true) !!} запомнить меня
            </label>
        </div>
        {!! Form::submit('Войти',['class'=>'btn btn-default']); !!}
        {!! Form::close() !!}
    </div>
@endsection
