<div class="row">
    <div class="col-lg-12">
        @if(Auth::check())
            {!! Form::open(['url' => route('postSaveSearch')]) !!}
                <div class="form-group">
                    <label>Название</label>
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Введите название, например: трехкомнатные картиры в Москве...']) !!}
                    {!! Form::hidden('search_string', Request::fullUrl()) !!}
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-8 col-sm-4">
                        <button type="submit" class="btn btn-success btn-block">Сохранить</button>
                    </div>
                </div>
            {!! Form::close() !!}
        @else
            <div class="alert alert-info text-center margin-top-20">
                Необходимо зарегистрироваться и войти,<br> что бы иметь возможность сохранять поиск.
            </div>
        @endif
    </div>
</div>
