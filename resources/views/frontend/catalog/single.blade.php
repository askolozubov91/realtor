@extends('frontend.layouts.main')

@section('breadcrumbs')
    {!! Breadcrumbs::render('getHome') !!}
@endsection

@section('title')
    {{$ad->title}}
@endsection

@section('content')
    <div class="row catalog-alerts">
        <div class="col-xs-12 page-messages">
            {!! Notification::showAll() !!}
            @if($errors->all())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </div>
            @endif
            @if($ad->status->name !== 'publicated')
                <div class="alert alert-danger">
                    @if($ad->status->name === 'archive')
                        Объявление перенесено в архив, вы можете <a href="{{route('getEditAd', $ad->id)}}">опубликовать это объявление снова</a>.
                    @endif
                    @if($ad->status->name === 'moderation')
                        Объявление проходит модерацию, после окончания процедуры проверки оно будет опубликовано или отклонено модератором.
                    @endif
                    @if($ad->status->name === 'rejected')
                        Объявление отклонено модератором поскольку нарушает правила сайта, <a href="{{route('getEditAd', $ad->id)}}">отредактируйте</a> его и и отправьте на модерацию повторно.
                    @endif
                    @if($ad->status->name === 'draft')
                        Это черновик объявления, вы можете <a href="{{route('getEditAd', $ad->id)}}">отредактировать его или опубликовать</a>.
                    @endif
                </div>
            @endif
        </div>
    </div>
    <div class="row margin-top-30">
        <div class="col-xs-8">
            <div class="ad-gallery-block">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#ad-photo" aria-controls="ad-photo" role="tab" data-toggle="tab"><i class="fa fa-image"></i> Фото</a></li>
                    <li role="presentation"><a href="#ad-map" aria-controls="ad-map" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i> На карте</a></li>
                </ul>
                <div class="tab-content ad-gallery">
                    <div role="tabpanel" class="tab-pane active" id="ad-photo">
                        <div class="row">
                            @if(!empty($ad->images[0]))
                                <div class="@if(count($ad->images) > 1) col-xs-9 @else col-xs-12 @endif">
                                    <div class="gallery-ad-main-img">
                                        <img id="gallery-ad-main-img" src="{{ImageResize::make((!empty($ad->image->path))?$ad->image->path:null, ['w'=>1920, 'h'=>1080, 'c'=>false ])}}" width="100%">
                                        @if(count($ad->images) > 1)
                                            <button class="btn btn-success">
                                                <i class="fa fa-image"></i> Показать все фото ({{count($ad->images)}})
                                            </button>
                                        @endif
                                    </div>
                                </div>
                                @if(count($ad->images) > 1)
                                    <div class="col-xs-3">
                                        <div class="gallery-ad-images-wrapper">
                                            @if(count($ad->images) > 4)
                                                <span class="gallery-ad-nav up"><i class="fa fa-angle-up"></i></span>
                                            @endif
                                            <div class="gallery-ad-images">
                                                @foreach($ad->images as $image)
                                                    <img src="{{ImageResize::make((!empty($image->path))?$image->path:null, ['w'=>140, 'h'=>70, 'c'=>true ])}}" width="140" height="70" data-big-src="{{ImageResize::make((!empty($image->path))?$image->path:null, ['w'=>1920, 'h'=>1080, 'c'=>false ])}}" width="170" height="140">
                                                @endforeach
                                            </div>
                                            @if(count($ad->images) > 4)
                                                <span class="gallery-ad-nav down"><i class="fa fa-angle-down"></i></span>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            @else
                                <div class="alert text-center margin-top-20">
                                    Для этого объявления автор не загрузил ни одной фотографии.
                                </div>
                            @endif
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="ad-map">2345</div>
                </div>
            </div>
            <div class="margin-top-20">
                <p>{{$ad->content}}</p>
            </div>
            <div class="single-ad-params margin-top-30">
                <table class="table table-striped">
                    @foreach($typeFields as $name => $field)
                        @if(!in_array($name, ['address', 'price', 'latitude', 'longitude']))
                            <tr>
                                <td>
                                    {{$field['display_name']}}
                                </td>
                                <td class="text-right">
                                    @if($field['editor'] == 'text')
                                        @if(!empty($ad[$name]))
                                            {{$ad[$name]}}
                                        @else
                                            не указано
                                        @endif
                                    @endif
                                    @if($field['editor'] == 'select')
                                        @if(!empty($ad[$name]))
                                            {{$field['values'][$ad[$name]]}}
                                        @else
                                            не указано
                                        @endif
                                    @endif
                                    @if($field['editor'] == 'group')
                                         <?php $groupValues = []; ?>
                                         @foreach($field['values'] as $value => $valueData)
                                                @if(!empty($ad[$value]))
                                                    <?php $groupValues[] = $valueData['display_name']; ?>
                                                @endif
                                         @endforeach
                                        {{(!empty($groupValues))?implode(', ',$groupValues):'не указано'}}
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
            <div id="single-ad-contact-form" class="single-ad-contact-form">
                <h2>Откликнуться на объявление</h2>
                {!! Form::open(['url'=>route('postSendSingleAdForm')]) !!}
                    {!! Form::hidden('user_id', $ad->user->id) !!}
                    {!! Form::hidden('ad_id', $ad->id) !!}
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Ваше имя:</label>
                                {!! Form::text('name', (Auth::check())?Auth::user()->name:null, ['class' => 'form-control', 'placeholder'=>'Имя']) !!}
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Ваш E-mail:</label>
                                        {!! Form::text('email', (Auth::check())?Auth::user()->email:null, ['class' => 'form-control', 'placeholder'=>'E-mail']) !!}
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Ваш телефон:</label>
                                        {!! Form::text('phone', (Auth::check())?'+'.Auth::user()->phone:null, ['class' => 'form-control', 'placeholder'=>'Телефон']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Сообщение:</label>
                                {!! Form::textarea('ad_message', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-offset-9 col-xs-3">
                            <button type="submit" class="btn btn-success btn-block">Отправить</button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            @if(!empty($relatedAds))
                <div class="single-related-ads margin-top-30">
                <h2>Похожие объявления</h2>
                <div class="single-related-ads-inner">
                    @if(count($relatedAds) > 4)
                        <span class="left single-related-nav"><i class="fa fa-angle-left"></i></span>
                        <span class="right single-related-nav"><i class="fa fa-angle-right"></i></span>
                    @endif
                    <ul class="related-ad-images">
                        @foreach($relatedAds as $relatedAd)
                            <li>
                                <div class="catalog-image">
                                    <a class="catalog-image-link" href="{{route('getSingle', $relatedAd->id)}}" title="{{$relatedAd->title}}">
                                        <img src="{{ImageResize::make((!empty($relatedAd->image->path))?$ad->image->path:null, ['w'=>160, 'h'=>160, 'c'=>true ])}}" width="100%">
                                                    <span class="catalog-images-count">
                                                        <i class="fa fa-image"></i> {{count($relatedAd->images)}}
                                                    </span>
                                        <div class="catalog-content-top-meta">
                                            <ul>
                                                @if(Auth::check())
                                                    <li class="bookmark-link @if(in_array($relatedAd->id, $userBookmarks)) delete-bookmark @endif"
                                                        data-url-add="{{route('getAddBookmark', $relatedAd->id)}}"
                                                        data-url-delete="{{route('getDeleteBookmark', $relatedAd->id)}}">
                                                        @if(in_array($relatedAd->id, $userBookmarks))
                                                            <i class='fa fa-bookmark'></i>
                                                        @else
                                                            <i class='fa fa-bookmark-o'></i>
                                                        @endif
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </a>
                                </div>
                                <div class="catalog-content-ads">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="catalog-content-price">
                                                @if(!empty($relatedAd->price))
                                                    {{number_format($relatedAd->price, 0, '.', ' ')}} <i class="fa fa-rub"></i>
                                                @else
                                                    <span class="no-price">цена не указана</span>
                                                @endif
                                            </div>
                                            <h3><a href="{{route('getSingle', $relatedAd->id)}}" title="{{$relatedAd->title}}">{{str_limit($relatedAd->title, 30)}}</a></h3>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
        </div>
        <div class="col-xs-4">
            <div class="ad-sidebar-block">
                <p class="ad-id">объявление № {{$ad->id}}</p>
                <p class="ad-price">
                    @if(!empty($ad->price))
                        {{number_format($ad->price, 0, '.', ' ')}} <i class="fa fa-rub"></i>
                    @else
                        цена не указана
                    @endif
                </p>
                <p class="ad-address">
                    <?php
                    $address = [];
                    if(!empty($ad->address)){
                        $address[] = $ad->address;
                    }
                    ?>
                    @foreach($ad->places as $place)
                        @if($place->type->name == 'city_metro')
                            <?php $address[] = $place->display_name; ?>
                        @endif
                    @endforeach
                    @foreach($ad->places as $place)
                        @if($place->type->name == 'city_district')
                            <?php $address[] = $place->display_name; ?>
                        @endif
                    @endforeach
                    @foreach($ad->places as $place)
                        @if($place->type->name == 'city')
                            <?php $address[] = $place->display_name; ?>
                        @endif
                    @endforeach
                    {{implode(', ', $address)}}
                </p>
                <p class="ad-show-map">
                    <i class="fa fa-map-marker"></i><a href="#ad-map" aria-controls="ad-map" role="tab" data-toggle="tab">Показать на карте</a>
                </p>
                <hr>
                <div class="ad-author">
                    @if(!empty($ad->agency))
                        <h3>{{str_limit($ad->agency->name, 20)}}</h3>
                        <p>Агентство</p>
                    @else
                        <h3>{{str_limit($ad->user->name, 20)}}</h3>
                        <p>{{$ad->user->userType->display_name}}</p>
                    @endif
                </div>
                <div class="ad-contact">
                    <p><span class="show-ad-phone" data-url="{{route('getAdPhone', $ad->id)}}"><a href="#" class="btn btn-success"><i class="fa fa-mobile-phone"></i> Показать телефон</a></span></p>
                    @if(!empty($ad->user->email))
                        <p><a href="#single-ad-contact-form" class="btn btn-success"><i class="fa fa-envelope"></i> Отправить письмо</a></p>
                    @endif
                </div>
                <div class="ad-date">
                    Последнее изменение объявления: <br>
                    <span class="format-date-time">{{$ad->updated_at}}</span>
                </div>
            </div>
            <ul class="fa-ul single-ad-sidebar-buttons">
                @if(Auth::check())
                    <li><i class="fa-li fa fa-bullhorn"></i><a href="{{route('getEditAd', $ad->id)}}#publication-type">Продвинуть объявление</a></li>
                    <li><i class="fa-li fa fa-pencil"></i><a href="{{route('getEditAd', $ad->id)}}">Редактировать объявление</a></li>
                @endif
                    @if(Auth::check())
                        <li class="bookmark-link-long @if(in_array($ad->id, $userBookmarks)) delete-bookmark @endif"
                            data-url-add="{{route('getAddBookmark', $ad->id)}}"
                            data-url-delete="{{route('getDeleteBookmark', $ad->id)}}">
                            @if(in_array($ad->id, $userBookmarks))
                                <i class='fa-li fa fa-bookmark'></i> <a href="#">Удалить из закладок</a>
                            @else
                                <i class='fa-li fa fa-bookmark-o'></i> <a href="#">Добавить в закладки</a>
                            @endif
                        </li>
                    @endif
            </ul>
        </div>
    </div>
    @include('frontend.parts.modal',[
       'id' => 'saveSearchModal',
       'title' => 'Сохранить поиск',
       'content' => view()->make('frontend.catalog.parts.save-search'),
    ])
@endsection
