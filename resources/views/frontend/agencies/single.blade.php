@extends('frontend.layouts.main')

@section('breadcrumbs')
    {!! Breadcrumbs::render('getAgencySingle', $agency->name) !!}
@endsection

@section('title')
    {{$agency->name}}
@endsection

@section('content')
    <div class="row catalog-alerts">
        <div class="col-xs-12 page-messages">
            {!! Notification::showAll() !!}
            @if($errors->all())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    {!! Form::open(['url'=>route('getAgencySingle', $agency->id), 'method' => 'GET', 'id' => 'catalog-filter-form']) !!}
        <div class="catalog-filter">
            <div class="row">
                @if(!empty($districts[0]) or !empty($metro[0]))
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="place_ad">
                                @if(!empty($metro[0])) Станция метро @else Район @endif
                            </label>
                            <select class="selectpicker form-control" data-none-selected-text="Ничего не выбрано..." id="place_ad" class="form-control" type="text" name="place_ad">
                                <option value="">Ничего не выбрано...</option>
                                <option data-divider="true"></option>
                                <?php $adPlace = $districts; if(!empty($metro[0])) $adPlace = $metro; ?>
                                @foreach($adPlace as $place)
                                    <option value="{{$place->id}}" @if($place->id == Request::input('place_ad')) selected @endif >
                                        {{ $place->display_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="col-xs-3">
                    <div class="form-group range-filter">
                        <label>Цена (руб.)</label>
                        <input id="ad_params_price" value="" class="form-control" type="text" name="ad_params[price][]" placeholder="0">
                        <span class="dash">&mdash;</span>
                        <input id="ad_params_price" value="" class="form-control" type="text" name="ad_params[price][]" placeholder="100 000 000">
                    </div>
                </div>
            </div>
            <div class="row additional-fields">
            </div>
        </div>
        <div class="catalog-filter-control">
            <div class="row">
                <div class="col-xs-offset-10 col-xs-2">
                    <div class="catalog-filter-buttons">
                        <button type="submit" class="btn btn-success btn-block">Показать</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalog-filter-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <span class="ads-count">Объявления: {{Input::get('page')*30}} - {{Input::get('page')*30 + count($ads)}} из {{$adsCount}}</span>
                    </div>
                    <div class="col-xs-4">
                        <div class="row catalog-sort">
                            <div class="col-xs-3">
                                <label for="catalog_sort">
                                    Сортировка:
                                </label>
                            </div>
                            <div class="col-xs-9">
                                <select class="selectpicker form-control col-xs-7" data-none-selected-text="Ничего не выбрано..." id="catalog_sort" class="form-control" type="text" name="catalog_sort">
                                    <option value="new" @if(Input::get('catalog_sort') == 'new') selected="selected" @endif >Новые предложения</option>
                                    <option value="low_price"  @if(Input::get('catalog_sort') == 'low_price') selected="selected" @endif >Дешевле</option>
                                    <option value="high_price"  @if(Input::get('catalog_sort') == 'high_price') selected="selected" @endif >Дороже</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <ul class="catalog-view-mode">
                            <li>Вид:</li>
                            <li><a href="?{{http_build_query(array_merge(Input::all(),['catalog_view' => 'list']))}}" class="@if(session('catalogView') != 'grid' and session('catalogView') != 'map') active @endif "><i class="fa fa-list-ul"></i></a></li>
                            <li><a href="?{{http_build_query(array_merge(Input::all(),['catalog_view' => 'grid']))}}" class="@if(session('catalogView') == 'grid') active @endif "><i class="fa fa-th"></i></a></li>
                            <li><a href="?{{http_build_query(array_merge(Input::all(),['catalog_view' => 'map']))}}" class="@if(session('catalogView') == 'map') active @endif "><i class="fa fa-map"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    @if(count($ads) > 0)
        @if(session('catalogView') == 'map')
            </div>
            <div class="continer-fluid">
                <div class="catalog-map" style="margin-top: 60px; margin-bottom: -30px;">
                    <div id="catalog-map" style="height: 50vh; position: relative;"></div>
                </div>
                <div class="hidden">
                    @foreach($ads as $ad)
                        <div class="catalog-row"
                             data-ad-id="{{$ad->id}}"
                             data-title="{{$ad->title}}"
                             data-latitude="{{$ad->latitude}}"
                             data-longitude="{{$ad->longitude}}">
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="container">
        @else
            <div class="catalog-content">
                <div class="row">
                    <div class="@if(session('showCatalogMap')) col-xs-5 @else col-xs-2 @endif">
                        <div class="catalog-map">
                            @if(session('showCatalogMap'))
                                <a href="?{{http_build_query(array_merge(Input::all(),['show_catalog_map' => 'off']))}}" class="show-catalog-map"><i class="fa fa-angle-left"></i> скрыть карту</a>
                            @else
                                <a href="?{{http_build_query(array_merge(Input::all(),['show_catalog_map' => 'on']))}}" class="show-catalog-map"><i class="fa fa-angle-right"></i></a>
                            @endif
                            <div id="catalog-map" class="sidebar-map"></div>
                        </div>
                    </div>
                    <div class="@if(session('showCatalogMap')) col-xs-7 @else col-xs-10 @endif">
                        @if(session('catalogView') == 'grid')
                            <div class="catalog-list">
                                <div class="row">
                                    <?php $adNum = 0; ?>
                                    @foreach($ads as $ad)
                                        <div  class="@if(session('showCatalogMap')) col-xs-6 @else col-xs-4 @endif">
                                            <div class="catalog-row grid"
                                                 data-ad-id="{{$ad->id}}"
                                                 data-title="{{$ad->title}}"
                                                 data-latitude="{{$ad->latitude}}"
                                                 data-longitude="{{$ad->longitude}}">
                                                <div class="catalog-image">
                                                    <a class="catalog-image-link" href="{{route('getSingle', $ad->id)}}" title="{{$ad->title}}">
                                                        <img src="{{ImageResize::make((!empty($ad->image->path))?$ad->image->path:null, ['w'=>300, 'h'=>200, 'c'=>true ])}}" width="300" height="200">
                                                        <span class="catalog-images-count">
                                                            <i class="fa fa-image"></i> {{count($ad->images)}}
                                                        </span>
                                                        <div class="catalog-content-top-meta">
                                                            <ul>
                                                                @if(!empty($ad->premium->where('name', 'publication_premium')->first()->pivot->off_datetime) and $ad->premium->where('name', 'publication_premium')->first()->pivot->off_datetime >= \Carbon\Carbon::now())
                                                                    <li><i class='fa fa-star'></i></li>
                                                                @endif
                                                                @if($ad->premium->where('name', 'color_marker')->first() and $ad->premium->where('name', 'color_marker')->first()->pivot->off_datetime >= \Carbon\Carbon::now() )
                                                                    <li><i class='fa fa-paint-brush'></i></li>
                                                                @endif
                                                                @if($ad->premium->where('name', 'publication_up')->first() and $ad->premium->where('name', 'publication_up')->first()->pivot->off_datetime >= \Carbon\Carbon::now())
                                                                    <li><i class='fa fa-arrow-up'></i></li>
                                                                @endif
                                                                    @if(Auth::check())
                                                                     <li class="bookmark-link @if(in_array($ad->id, $userBookmarks)) delete-bookmark @endif"
                                                                         data-url-add="{{route('getAddBookmark', $ad->id)}}"
                                                                         data-url-delete="{{route('getDeleteBookmark', $ad->id)}}">
                                                                         @if(in_array($ad->id, $userBookmarks))
                                                                             <i class='fa fa-bookmark'></i>
                                                                         @else
                                                                             <i class='fa fa-bookmark-o'></i>
                                                                         @endif
                                                                     </li>
                                                                    @endif
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="catalog-content-ads">
                                                    <div class="row">
                                                        <div class="col-xs-12 catalog-content-left">
                                                            <?php
                                                            $address = [];
                                                            if(!empty($ad->address)){
                                                                $address[] = $ad->address;
                                                            }
                                                            ?>
                                                            @foreach($ad->places as $place)
                                                                @if($place->type->name == 'city_metro')
                                                                    <?php $address[] = $place->display_name; ?>
                                                                @endif
                                                            @endforeach
                                                            @foreach($ad->places as $place)
                                                                @if($place->type->name == 'city_district')
                                                                    <?php $address[] = $place->display_name; ?>
                                                                @endif
                                                            @endforeach
                                                            @foreach($ad->places as $place)
                                                                @if($place->type->name == 'city')
                                                                    <?php $address[] = $place->display_name; ?>
                                                                @endif
                                                            @endforeach
                                                            <p class="catalog-content-address">
                                                                <span title="{{implode(', ', $address)}}">
                                                                   {{str_limit(implode(', ', $address), 40)}}
                                                                </span>
                                                            </p>
                                                                <div class="catalog-content-price">
                                                                    @if(!empty($ad->price))
                                                                        {{number_format($ad->price, 0, '.', ' ')}} <i class="fa fa-rub"></i>
                                                                    @else
                                                                        цена не указана
                                                                    @endif
                                                                </div>
                                                                <h2><a href="{{route('getSingle', $ad->id)}}" title="{{$ad->title}}">{{str_limit($ad->title, 30)}}</a></h2>
                                                                <p class="catalog-content-text">
                                                                    @if(session('showCatalogMap'))
                                                                        {{str_limit($ad->content, 70)}}
                                                                    @else
                                                                        {{str_limit($ad->content, 120)}}
                                                                    @endif
                                                                </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if(session('showCatalogMap'))
                                            @if(++$adNum % 2 == 0)
                                </div>
                                <div class="row">
                                    @endif
                                    @else
                                        @if(++$adNum % 3 == 0)
                                </div><div class="row">
                                    @endif
                                    @endif
                                    @endforeach
                                </div>
                                <div>
                                    {!! $ads->render() !!}
                                </div>
                            </div>
                        @else
                            <div class="catalog-list">
                                @foreach($ads as $ad)
                                    <div class="catalog-row"
                                         data-ad-id="{{$ad->id}}"
                                         data-latitude="{{$ad->latitude}}"
                                         data-longitude="{{$ad->longitude}}">
                                        <div class="catalog-image">
                                            <a class="catalog-image-link" href="{{route('getSingle', $ad->id)}}" title="{{$ad->title}}">
                                                <img src="{{ImageResize::make((!empty($ad->image->path))?$ad->image->path:null, ['w'=>170, 'h'=>140, 'c'=>true ])}}" width="170" height="140">
                                            <span class="catalog-images-count">
                                                <i class="fa fa-image"></i> {{count($ad->images)}}
                                            </span>
                                            </a>
                                        </div>
                                        <div class="catalog-content-ads">
                                            <div class="row">
                                                <div class="@if(session('showCatalogMap')) col-xs-8 @else col-xs-7 @endif catalog-content-left">
                                                    <h2><a href="{{route('getSingle', $ad->id)}}" title="{{$ad->title}}">{{str_limit($ad->title, 40)}}</a></h2>
                                                    <?php
                                                    $address = [];
                                                    if(!empty($ad->address)){
                                                        $address[] = $ad->address;
                                                    }
                                                    ?>
                                                    @foreach($ad->places as $place)
                                                        @if($place->type->name == 'city_metro')
                                                            <?php $address[] = $place->display_name; ?>
                                                        @endif
                                                    @endforeach
                                                    @foreach($ad->places as $place)
                                                        @if($place->type->name == 'city_district')
                                                            <?php $address[] = $place->display_name; ?>
                                                        @endif
                                                    @endforeach
                                                    @foreach($ad->places as $place)
                                                        @if($place->type->name == 'city')
                                                            <?php $address[] = $place->display_name; ?>
                                                        @endif
                                                    @endforeach
                                                    <p class="catalog-content-address">{{implode(', ', $address)}}</p>
                                                    <p class="catalog-content-text">
                                                        @if(session('showCatalogMap'))
                                                            {{str_limit($ad->content, 70)}}
                                                        @else
                                                            {{str_limit($ad->content, 120)}}
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="@if(session('showCatalogMap')) col-xs-4 @else col-xs-5 @endif catalog-content-right">
                                                    @if(!session('showCatalogMap'))
                                                        <div class="catalog-content-author">
                                                            @if(!empty($ad->agency))
                                                                <h3>{{str_limit($ad->agency->name, 20)}}</h3>
                                                                <p>Агентство</p>
                                                            @else
                                                                <h3>{{str_limit($ad->user->name, 20)}}</h3>
                                                                <p>{{$ad->user->userType->display_name}}</p>
                                                            @endif
                                                        </div>
                                                        <div class="catalog-content-contacts">
                                                            <i class="fa fa-mobile-phone"></i> <span class="show-ad-phone" data-url="{{route('getAdPhone', $ad->id)}}"><a href="#">Показать телефон</a></span>
                                                        </div>
                                                    @endif
                                                    <div class="catalog-content-price">
                                                        @if(!empty($ad->price))
                                                            {{number_format($ad->price, 0, '.', ' ')}} <i class="fa fa-rub"></i>
                                                        @else
                                                            цена не указана
                                                        @endif
                                                    </div>
                                                    <div class="catalog-content-top-meta">
                                                        <ul>
                                                            @if(!empty($ad->premium->where('name', 'publication_premium')->first()->pivot->off_datetime) and $ad->premium->where('name', 'publication_premium')->first()->pivot->off_datetime >= \Carbon\Carbon::now())
                                                                <li><i class='fa fa-star'></i></li>
                                                            @endif
                                                            @if($ad->premium->where('name', 'color_marker')->first() and $ad->premium->where('name', 'color_marker')->first()->pivot->off_datetime >= \Carbon\Carbon::now() )
                                                                <li><i class='fa fa-paint-brush'></i></li>
                                                            @endif
                                                            @if($ad->premium->where('name', 'publication_up')->first() and $ad->premium->where('name', 'publication_up')->first()->pivot->off_datetime >= \Carbon\Carbon::now())
                                                                <li><i class='fa fa-arrow-up'></i></li>
                                                            @endif
                                                                @if(Auth::check())
                                                                    <li class="bookmark-link @if(in_array($ad->id, $userBookmarks)) delete-bookmark @endif"
                                                                        data-url-add="{{route('getAddBookmark', $ad->id)}}"
                                                                        data-url-delete="{{route('getDeleteBookmark', $ad->id)}}">
                                                                        @if(in_array($ad->id, $userBookmarks))
                                                                            <i class='fa fa-bookmark'></i>
                                                                        @else
                                                                            <i class='fa fa-bookmark-o'></i>
                                                                        @endif
                                                                    </li>
                                                                @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div>
                                    {!! $ads->render() !!}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    @else
        <div class="catalog-content text-center margin-bottom-20">
            <div class="alert alert-danger margin-top-30">
                Объявлений по вашему запросу не найдено, попробуйте изменить параметры поиска.
            </div>
        </div>
    @endif
    @include('frontend.parts.modal',[
       'id' => 'saveSearchModal',
       'title' => 'Сохранить поиск',
       'content' => view()->make('frontend.catalog.parts.save-search'),
    ])
@endsection
