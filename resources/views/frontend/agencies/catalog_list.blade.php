@extends('frontend.layouts.main')

@section('breadcrumbs')
    {!! Breadcrumbs::render('getAgenciesCatalog') !!}
@endsection

@section('title')
    Агентства недвижимости в городе {{$globalCity->display_name}}
@endsection

@section('content')
    <div class="row catalog-alerts">
        <div class="col-xs-12 page-messages">
            {!! Notification::showAll() !!}
            @if($errors->all())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        {{$error}}<br>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    {!! Form::open(['url'=>route('getAgenciesCatalog'), 'method' => 'GET', 'id' => 'catalog-filter-form']) !!}
        <div class="catalog-filter">
            <div class="row">
                @if(!empty($districts[0]) or !empty($metro[0]))
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="place_agency">
                                @if(!empty($metro[0])) Станция метро @else Район @endif
                            </label>
                            <select class="selectpicker form-control" data-none-selected-text="Ничего не выбрано..." id="place_agency" class="form-control" type="text" name="place_agency">
                                <option value="">Ничего не выбрано...</option>
                                <option data-divider="true"></option>
                                <?php $agencyPlace = $districts; if(!empty($metro[0])) $agencyPlace = $metro; ?>
                                @foreach($agencyPlace as $place)
                                    <option value="{{$place->id}}" @if($place->id == Request::input('place_agency')) selected @endif >
                                        {{ $place->display_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
                <div class="col-xs-9">
                    <div class="form-group">
                        <label>Название</label>
                        <input id="agency_params_name" value="{{Request::input('agency_params.name')}}" class="form-control" type="text" name="agency_params[name]" placeholder="Введите название агентства или его часть...">
                    </div>
                </div>
            </div>
        </div>
        <div class="catalog-filter-control">
            <div class="row">
                <div class="col-xs-offset-10 col-xs-2">
                    <div class="catalog-filter-buttons">
                        <button type="submit" class="btn btn-success btn-block">Показать</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="catalog-filter-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <span class="ads-count">Агентства: {{Input::get('page')*30}} - {{Input::get('page')*30 + count($agencies)}} из {{$agenciesCount}}</span>
                    </div>
                    <div class="col-xs-6">
                        <div class="row catalog-sort">
                            <div class="col-xs-3">
                                <label for="catalog_sort">
                                    Сортировка:
                                </label>
                            </div>
                            <div class="col-xs-9">
                                <select class="selectpicker form-control col-xs-7" data-none-selected-text="Ничего не выбрано..." id="catalog_sort" class="form-control" type="text" name="catalog_sort">
                                    <option value="new" @if(Input::get('catalog_sort') == 'new') selected="selected" @endif >Новые агентства</option>
                                    <option value="ads_count"  @if(Input::get('catalog_sort') == 'ads_count') selected="selected" @endif >По количеству предложений</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    @if(count($agencies) > 0)
        <div class="catalog-content catalog-agencies">
            <div class="row">
                <div class="col-xs-12">
                    <div class="catalog-list">
                        <div class="catalog-items">
                            <?php $agencyNum = 0; ?>
                            @foreach($agencies as $agency)
                                <div class="catalog-item">
                                    <?php
                                    $address = [];
                                    $contact_phone = null;
                                    if(!empty($agency->addresses) && count( $agency->addresses )){
                                        $address[] = $agency->addresses[0]->address;
                                    }
                                    if(!empty($agency->contacts)){
                                        foreach( $agency->contacts as $agency_contact ) {
                                            if( 1 == $agency_contact->contact_type ) { // contact_type is phone
                                                $contact_phone = $agency_contact->value;

                                                break;
                                            }
                                        }
                                    }
                                    ?>
                                    @foreach($agency->places as $place)
                                        @if($place->type->name == 'city_metro')
                                            <?php $address[] = $place->display_name; ?>
                                        @endif
                                    @endforeach
                                    @foreach($agency->places as $place)
                                        @if($place->type->name == 'city_district')
                                            <?php $address[] = $place->display_name; ?>
                                        @endif
                                    @endforeach
                                    @foreach($agency->places as $place)
                                        @if($place->type->name == 'city')
                                            <?php $address[] = $place->display_name; ?>
                                        @endif
                                    @endforeach
                                    <div class="catalog-row">
                                        <div class="catalog-image">
                                            <a class="catalog-image-link" href="{{route('getAgencySingle', $agency->id)}}" title="{{$agency->name}}">
                                                <img src="{{ImageResize::make((!empty($agency->image->path))?$agency->image->path:null, ['w'=>172, 'h'=>172, 'c'=>false ])}}" width="172">
                                            </a>
                                        </div>
                                        <div class="catalog-content-agency">
                                            <div class="catalog-content-name">{{$agency->name}}</div>
                                            <div class="catalog-content-address">{{implode(', ', $address)}}</div>
                                            @if($contact_phone)
                                                <div class="catalog-content-phone"><i class="fa fa-mobile"></i>{{$contact_phone}}</div>
                                            @endif
                                            <div class="catalog-content-text">{{str_limit($agency->about, 100)}}</div>
                                            <div class="catalog-content-readmore">
                                                <a href="{{route('getAgencySingle', $agency->id)}}">посмотреть предложения агентства ({{$agency->ads()->count()}})</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div>
                            {!! $agencies->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="catalog-content text-center margin-bottom-20">
            <div class="alert alert-danger margin-top-30">
                Агентств по вашему запросу не найдено, попробуйте изменить параметры поиска.
            </div>
        </div>
    @endif
@endsection
