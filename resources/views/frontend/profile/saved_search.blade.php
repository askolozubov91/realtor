@extends('frontend.layouts.profile')

@section('profile_content')
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="{{ route('getProfileBookmarks') }}">Закладки</a></li>
            <li role="presentation" class="active"><a href="{{route('getProfileSavedSearch')}}">Сохраненный поиск</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="row">
                    <div class="col-xs-12 margin-bottom-30">
                        <div>
                        @forelse($savedSearch as $bookmark)
                                <div class="profile-list ads-list saved-search-list">
                                    <div class="profile-list-inner">
                                        <div class="ads-list-content">
                                            <p class="profile-list-title"><a href="{{$bookmark->search_string}}">{{$bookmark->title}}</a></p>
                                            <p class="profile-list-meta">закладка создана: <span class="format-date-time">{{$bookmark->created_at}}</span></p>
                                        </div>
                                        <div class="ads-list-markers">
                                            <i class="fa fa-times"></i> <a href="#"  data-toggle="modal" data-url="{{ route('getDeleteSavedSearch', $bookmark->id) }}" data-target="#deleteBookmarkModal">удалить</a>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <div class="alert alert-info text-center margin-top-20">
                                    У вас пока нет ни одного сохраненного поиска.
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            {!! $savedSearch->render() !!}
        </div>
        @include('frontend.parts.modal',[
                  'id' => 'deleteBookmarkModal',
                  'title' => 'Удалить закладку',
                  'content' => 'Вы действительно хотите удалить эту закладку?',
                  'haveCancel' => true,
                  'buttons' => [
                    '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
                  ]
        ])
@endsection