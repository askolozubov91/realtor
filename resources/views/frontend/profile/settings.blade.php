@extends('frontend.layouts.profile')

@section('profile_content')
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="{{route('getProfileSettings')}}">Пользователь</a></li>
            @foreach($user->agencies as $agency)
                <li role="presentation"><a href="{{route('getProfileAgencySettings',$agency->id)}}">{{$agency->name}}</a></li>
            @endforeach
        </ul>
        <div class="tab-content">
            <div class="tab-pane active">
                {!! Form::open(['url' => route('postUpdateUser')]) !!}
                <div class="row">
                    <div class="col-xs-12 margin-bottom-30">
                        <h2>Данные аккаунта</h2>
                        <div class="form-group">
                            {!! Form::label('user_type','Тип пользователя') !!}
                            {!! Form::select('user_type', $user_types, $user->user_type, ['class' => 'form-control selectpicker', 'id' => 'user_type']) !!}
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    {!! Form::label('user_name','Ваше имя') !!}
                                    {!! Form::text('name', $user->name, ['class' => 'form-control', 'id' => 'user_name', 'placeholder' => 'Имя']) !!}
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    {!! Form::label('company_name','Название компании или агентства') !!}
                                    {!! Form::text('company_name', $user->company_name, ['class' => 'form-control', 'id' => 'company_name', 'placeholder' => 'Компания в которой вы работаете']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <a href="#" v-show="authUser.confirmed_phone == 0" class="pull-right" data-toggle="modal" data-target="#confirmPhoneModal" v-on:click="sendConfirmPhone">подтвердить</a>
                                    {!! Form::label('phone','Номер телефона') !!}
                                    {!! Form::text('phone', $user->phone, ['class' => 'form-control phone-mask', 'id' => 'phone']) !!}
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <a href="#" v-show="authUser.confirmed_email == 0" class="pull-right" data-toggle="modal" data-target="#confirmEmailModal" v-on:click="sendConfirmEmail">подтвердить</a>
                                    {!! Form::label('email','E-mail') !!}
                                    {!! Form::text('email', $user->email, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Электронная почта']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 margin-bottom-30">
                        <h2>Смена пароля</h2>
                        <p>Оставьте эти поля пустыми, если не хотите менять пароль</p>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    {!! Form::label('password','Пароль') !!}
                                    {!! Form::password('password', ['class' => 'form-control', 'id' => 'password']) !!}
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    {!! Form::label('password_confirm','Подтверждение пароля') !!}
                                    {!! Form::password('password_confirm', ['class' => 'form-control', 'id' => 'password_confirm']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 margin-bottom-30">
                        <h2>Уведомления</h2>
                        <p>Уведомления на E-mail</p>
                        <div class="checkbox">
                            {!! Form::hidden('send_news', 0) !!}
                            {!! Form::checkbox('send_news', 1, $user->send_news, ['id' => 'send_news']) !!}
                            <label for="send_news">получать новости от администрации сайта</label>
                        </div>
                        <div class="checkbox">
                            {!! Form::hidden('send_notifications_email', 0) !!}
                            {!! Form::checkbox('send_notifications_email', 1, $user->send_notifications_email, ['id' => 'send_notifications_email']) !!}
                            <label for="send_notifications_email">получать уведомления</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 margin-bottom-30">
                        <div class="bg-info-light save-settings">
                            <a href="#" class="remove-account"><i class="fa fa-times"></i> удалить аккаунт</a>
                            <button type="submit" class="btn btn-success pull-right">Сохранить изменения</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @include('frontend.parts.modal',[
                  'id' => 'deleteUserModal',
                  'title' => 'Удалить аккаунт',
                  'content' => 'Вы действительно хотите удалить свой аккаунт?',
                  'haveCancel' => true,
                  'buttons' => [
                    '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
                  ]
        ])
        @include('frontend.parts.modal',[
                    'id' => 'confirmPhoneModal',
                    'title' => 'Подтвердить телефон',
                    'content' => view()->make('frontend.profile.parts.confirm_phone'),
        ])
        @include('frontend.parts.modal',[
                    'id' => 'confirmEmailModal',
                    'title' => 'Подтвердить E-mail',
                    'content' => view()->make('frontend.profile.parts.confirm_email'),
        ])
@endsection