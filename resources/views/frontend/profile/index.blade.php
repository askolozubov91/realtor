@extends('frontend.layouts.profile')

@section('profile_content')
        <div class="row">
            <div class="col-xs-12 margin-bottom-30">
                <div class="gray-panel border-gray radius drop-shadow profile-card">
                    <h2>{{ Auth::user()->name }}</h2>
                    <div class="profile-contacts">
                        <div class="profile-contacts-item">
                            <div class="profile-contacts-item-icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="profile-contacts-item-content">
                                <p>+{{Auth::user()->phone}}</p>
                            </div>
                        </div>
                        <div class="profile-contacts-item">
                            <div class="profile-contacts-item-icon">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <div class="profile-contacts-item-content">
                                <p>{{Auth::user()->email}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="profile-contacts-edit">
                        <a href="{{route('getProfileSettings')}}"><i class="fa fa-cog"></i>Изменить контактные данные и другие параметры</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8 col-lg-9 profile-block-title">
                <h3>Объявления</h3>
                <ul>
                    @foreach($statuses as $status)
                        <li>{{$status->display_name}}: <a href="{{ route('getProfileAds', $status->name) }}"><strong>{{(!empty($status->myAdsCount[0]))?$status->myAdsCount[0]->count:0}}</strong></a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-xs-4 col-lg-3">
                <a href="{{route('getCreateAd')}}" class="btn btn-success btn-block">Добавить объявление</a>
            </div>
        </div>
        <div class="row margin-bottom-30">
            <div class="col-xs-12">
                @forelse($ads as $ad)
                    <div class="profile-list white-gradient">
                        <div class="profile-list-inner">
                            <p class="profile-list-title"><a href="{{route('getEditAd', $ad->id)}}">{{$ad->title}}</a></p>
                            <p class="profile-list-meta">изменено: <span class="format-date-time">{{$ad->updated_at}}</span></p>
                        </div>
                        <div class="profile-list-actions">
                            <ul class="fa-ul">
                                <li><a href="{{route('getEditAd', $ad->id)}}"><i class="fa-li fa fa-pencil"></i>Редактировать</a></li>
                                <li><a href="{{route('getEditAd', $ad->id)}}#publication-type"><i class="fa-li fa fa-bullhorn"></i>Продвинуть</a></li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <div class="alert alert-info text-center margin-top-20">
                        У вас пока нет ни одного объявления, но его можно <a href="#">создать</a>.
                    </div>
                @endforelse
            </div>
            <div class="col-xs-12 full-width-large-btn">
                <a href="{{route('getProfileAds')}}" class="btn btn-info btn-block btn-lg"><i class="fa fa-cog"></i>Управление объявлениями</a>
            </div>
        </div>
        <div class="row margin-bottom-30">
            <div class="col-xs-12">
                <div class="bg-info-light profile-gray-block">
                    <h3>Сохраненный поиск</h3>
                    @forelse($savedSearch as $bookmark)
                        <div class="profile-list gray-gradient">
                            <div class="profile-list-inner">
                                <p class="profile-list-title"><a href="{{$bookmark->search_string}}">{{$bookmark->title}}</a></p>
                                <p class="profile-list-meta">поиск сохранен: <span class="format-date-time">{{$bookmark->created_at}}</span></p>
                            </div>
                            <div class="profile-list-actions">
                                <ul class="fa-ul">
                                    <li><a href="#" data-toggle="modal" data-url="{{ route('getDeleteSavedSearch', $bookmark->id) }}" data-target="#deleteBookmarkModal"><i class="fa-li fa fa-times"></i>Удалить</a></li>
                                </ul>
                            </div>
                        </div>
                    @empty
                        <div class="alert alert-info text-center margin-top-20">
                            У вас пока нет ни одного сохраненного поиска.
                        </div>
                    @endforelse
                </div>
            </div>
            <div class="col-xs-12">
                <a href="{{ route('getProfileSavedSearch') }}" class="btn btn-info btn-block btn-lg">Показать все <strong>({{$savedSearchCount}})</strong></a>
            </div>
        </div>
        <div class="row margin-bottom-30">
            <div class="col-xs-12">
                <div class="bg-info-light profile-gray-block">
                    <h3>Закладки</h3>
                    @forelse($bookmarks as $bookmark)
                        <div class="profile-list gray-gradient">
                            <div class="profile-list-inner">
                                <p class="profile-list-title"><a href="#">{{$bookmark->ad->title}}</a></p>
                                <p class="profile-list-meta">закладка создана: <span class="format-date-time">{{$bookmark->created_at}}</span></p>
                            </div>
                            <div class="profile-list-actions">
                                <ul class="fa-ul">
                                    <li><a href="#" data-toggle="modal" data-url="{{ route('getDeleteBookmark', $bookmark->ad->id) }}" data-target="#deleteBookmarkModal"><i class="fa-li fa fa-times"></i>Удалить</a></li>
                                </ul>
                            </div>
                        </div>
                    @empty
                        <div class="alert alert-info text-center margin-top-20">
                            У вас пока нет ни одной закладки.
                        </div>
                    @endforelse
                </div>
            </div>
            <div class="col-xs-12">
                <a href="{{ route('getProfileBookmarks') }}" class="btn btn-info btn-block btn-lg">Показать все <strong>({{$bookmarksCount}})</strong></a>
            </div>
        </div>
@endsection