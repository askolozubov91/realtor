<div class="row">
    <div class="col-xs-12">
        {!! Form::open(['method'=>'GET', 'target'=>'_blank', 'url'=>route('startPayment'), 'id'=>'payment-form', 'class' => 'horizontal-form']) !!}
            <div class="row">
                <div class="col-xs-12 payment-about">
                    <h3>Сумма</h3>
                    <p>Укажите количество денег, которые вы хотите внести:</p>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-9">
                    <input type="text" name="payment_sum" class="form-control" id="paymentSum" v-model="paymentSum">
                </div>
                <label for="paymentSum" class="col-xs-3 control-label">рублей</label>
            </div>
            <div class="row">
                <div class="col-xs-12 payment-about">
                    <h3>Способ оплаты</h3>
                    <p>Выберите подходящий способ оплаты</p>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <select id="paymentType"  name="payment_type" class="form-control selectpicker">
                        <option value="BC">Банковская карта</option>
                        <option value="YM">Яндекс деньги</option>
                        <option value="WM">Webmoney</option>
                        <option value="QW">QiWi-кошелек</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-success pull-right">Пополнить</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>