<div class="row margin-bottom-20" v-if="confirmMessage.length != 0">
    <div class="col-xs-12">
        <div class="alert alert-success">
            <ul>
                <li v-for="message in confirmMessage">
                    @{{message}}
                </li>
            </ul>
        </div>
    </div>
</div>
<div v-show="authUser.phone && authUser.confirmed_phone == 0">
    {!! Form::open() !!}
    <p>На указанный вами телефоный номер <span class="text-primary">+@{{ authUser.phone }}</span> был отправлен код подтверждения, введите его, что бы подтвердить номер.</p>
    <div class="row">
        <div class="col-xs-12">
            <strong>{!! Form::label('phoneConfirmCode', 'Код подтверждения телефона:') !!}</strong>
        </div>
    </div>
    <div class="row">

        <div class="col-xs-7" v-bind:class="[adErrosStatus == 403 ? 'has-error' : '']">
            {!! Form::text('phone_confirm_code', null, ['class' => 'form-control', 'id' => 'phoneConfirmCode']) !!}
        </div>
        <div class="col-xs-5">
            <button class="btn btn-link" type="button" v-on:click="sendConfirmPhone">повторно выслать код</button>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 margin-top-20">
            {!! Form::button('Подтвердить данные', ['type' => 'button', 'class' => 'btn btn-success btn-block btn-lg', 'v-on:click' => 'confirmContacts($event)']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>