<div class="row margin-bottom-20" v-if="confirmMessage.length != 0">
    <div class="col-xs-12">
        <div class="alert alert-success">
            <ul>
                <li v-for="message in confirmMessage">
                    @{{message}}
                </li>
            </ul>
        </div>
    </div>
</div>
<div v-show="authUser.email && authUser.confirmed_email == 0">
    {!! Form::open() !!}
        <p class="margin-top-30">На указанный вами E-mail <span class="text-primary">@{{ authUser.email }}</span> был отправлен код подтверждения, введите его, что бы подтвердить адрес.</p>
        <div class="row">
            <div class="col-xs-12">
                <strong>{!! Form::label('emailConfirmCode', 'Код подтверждения электронной почты:') !!}</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-7" v-bind:class="[adErrosStatus == 403 ? 'has-error' : '']">
                {!! Form::text('email_confirm_code', null, ['class' => 'form-control', 'id' => 'emailConfirmCode']) !!}
            </div>
            <div class="col-xs-5">
                <button class="btn btn-link" type="button" v-on:click="sendConfirmEmail">повторно выслать код</button>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 margin-top-20">
                {!! Form::button('Подтвердить данные', ['type' => 'button', 'class' => 'btn btn-success btn-block btn-lg', 'v-on:click' => 'confirmContacts($event)']) !!}
            </div>
        </div>
    {!! Form::close() !!}
</div>
