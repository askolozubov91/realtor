@extends('frontend.layouts.profile')

@section('profile_content')

        <div class="row">
            <div class="col-xs-12 margin-bottom-30">
                <div class="profile-submenu">
                    <ul>
                        @foreach($statuses as $status)
                            <li><a href="{{ route('getProfileAds', $status->name) }}" class="{{(Route::current()->statusName == $status->name or (!Route::current()->statusName and $status->name == 'publicated'))?'active':''}}">{{$status->display_name}}: <strong>{{(!empty($status->myAdsCount[0]))?$status->myAdsCount[0]->count:0}}</strong></a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="profile-ads">
                    @forelse($ads as $ad)
                        <div class="profile-list ads-list">
                            <div class="profile-list-inner">
                                <div class="ads-list-image">
                                    <a href="#"><img src="{{ImageResize::make((!empty($ad->image->path))?$ad->image->path:null, ['w'=>70, 'h'=>70, 'c'=>true ])}}" width="70" height="70"></a>
                                </div>
                                <div class="ads-list-content">
                                    <p class="profile-list-title"><a href="{{route('getEditAd', $ad->id)}}">{{$ad->title}}</a></p>
                                    <p class="profile-list-meta">изменено: <span class="format-date-time">{{$ad->updated_at}}</span></p>
                                    <ul class="fa-ul ads-list-meta">
                                        <li><a href="{{route('getEditAd', $ad->id)}}"><i class="fa-li fa fa-pencil"></i>Редактировать</a></li>
                                        @if($adStatus->name != 'archive')
                                            <li><a href="#" data-toggle="modal" data-url="{{ route('getAdStatus',['statusName' => 'archive', 'id' => $ad->id]) }}" data-target="#sendToArchiveModal"><i class="fa-li fa fa-times"></i>Удалить</a></li>
                                        @endif
                                        @if($adStatus->name != 'draft' and $adStatus->name != 'archive')
                                            <li><a href="#" data-toggle="modal" data-url="{{ route('getAdStatus',['statusName' => 'draft', 'id' => $ad->id]) }}" data-target="#sendToDraftModal"><i class="fa-li fa fa-edit"></i>В черновики</a></li>
                                        @endif
                                        @if($adStatus->name == 'publicated')
                                        <li><a href="{{route('getEditAd', $ad->id)}}#publication-type"><i class="fa-li fa fa-bullhorn"></i>Продвинуть</a></li>
                                        @endif
                                    </ul>
                                </div>

                                <div class="ads-list-markers">
                                    <ul>
                                        <li @if(
                                                !empty($ad->premium->where('name', 'publication_premium')->first()->pivot->off_datetime)
                                                and $ad->premium->where('name', 'publication_premium')->first()->pivot->off_datetime >= \Carbon\Carbon::now()
                                            ) class="active" @endif><i class='fa fa-star'></i></li>
                                        <li @if(
                                                $ad->premium->where('name', 'color_marker')->first()
                                                and $ad->premium->where('name', 'color_marker')->first()->pivot->off_datetime >= \Carbon\Carbon::now()
                                            ) class="active" @endif><i class='fa fa-paint-brush'></i></li>
                                        <li @if(
                                                $ad->premium->where('name', 'publication_up')->first()
                                                and $ad->premium->where('name', 'publication_up')->first()->pivot->off_datetime >= \Carbon\Carbon::now()
                                            ) class="active" @endif><i class='fa fa-arrow-up'></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="alert alert-info text-center margin-top-20">
                            У вас пока нет ни одного объявления со статусом "{{$adStatus->display_name}}".
                        </div>
                    @endforelse
                </div>
                <div>
                    {!! $ads->render() !!}
                </div>
            </div>
        </div>
        @include('frontend.parts.modal',[
                  'id' => 'sendToDraftModal',
                  'title' => 'Перенести в черновики',
                  'content' => 'Вы действительно хотите перенести это объявление в черновики?',
                  'haveCancel' => true,
                  'buttons' => [
                    '<a href="#" class="btn btn-danger modal-url-btn">Перенести в черновики</a>'
                  ]
        ])
        @include('frontend.parts.modal',[
                  'id' => 'sendToArchiveModal',
                  'title' => 'Перенести в архив',
                  'content' => 'Вы действительно хотите перенести это объявление в архив?',
                  'haveCancel' => true,
                  'buttons' => [
                     '<a href="#" class="btn btn-danger modal-url-btn">Перенести в архив</a>'
                  ]
        ])
@endsection