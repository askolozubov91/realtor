@extends('frontend.layouts.profile')

@section('profile_content')
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="{{route('getProfileSettings')}}">Пользователь</a></li>
                @foreach($user->agencies as $ag)
                    <li role="presentation" @if($ag->id === $agency->id) class="active" @endif><a href="{{route('getProfileAgencySettings',$ag->id)}}">{{$ag->name}}</a></li>
                @endforeach
            </ul>
            <div class="tab-content">
                <div class="tab-pane active">
                <div class="row margin-top-30">
                    <div class="col-xs-12">
                        <h3>Данные агентства</h3>
                        <p>Информация об агентстве.</p>
                    </div>
                </div>
                {!! Form::open(['id' => 'updateAgencyForm']) !!}
                <div class="row margin-top-10">
                    <div class="col-xs-3">
                        <div class="margin-bottom-10">
                            {!! Form::hidden('oldImgUrl',!empty($agency->image->path)?ImageResize::make($agency->image->path, ['w'=>150, 'h'=>150, 'c'=>true ]):null,['v-model' => 'angecyLogoTumbnailUrl']) !!}
                            {!! Form::hidden('angecyLogoId',!empty($agency->image->id)?$agency->image->id:null,['v-model' => 'angecyLogoId']) !!}
                            <img v-bind:src="(angecyLogoId)?angecyLogoTumbnailUrl:'/images/no_image.jpg'" width="100%" class="img-rounded">
                        </div>
                        <div>
                            <div class="fileinput-button" id="imagesUploadButtonButton">
                                <span class="btn btn-success btn-block">Загрузить лого</span>
                                <input id="imageSingleUpload" type="file" name="file" data-url="{{route('postUploadImage')}}" multiple>
                            </div>
                        </div>
                        <div v-if="angecyLogoId">
                            {!! Form::button('<i class="fa fa-times"></i> Удалить лого', ['type' => 'button', 'class' => 'btn btn-block btn-link', 'v-on:click' => 'angecyLogoId = null']) !!}
                            {!! Form::hidden('logo_image_id','@{{ angecyLogoId }}') !!}
                        </div>
                    </div>
                    <div class="col-xs-9">
                        <div class="form-group" v-bind:class="[agencyErrors.name ? 'has-error' : '']">
                            {!! Form::label('agencyName', 'Название') !!}
                            {!! Form::text('name', $agency->name, ['class' => 'form-control', 'id' => 'agencyName']) !!}
                            <span class="help-block" v-for="err in agencyErrors.name">@{{ err }}</span>
                        </div>
                        <div class="form-group" v-bind:class="[agencyErrors.about ? 'has-error' : '']">
                            {!! Form::label('agencyAbout', 'Описание') !!}
                            {!! Form::textarea('about', $agency->about, ['class' => 'form-control', 'id' => 'agencyAbout', 'rows' => 6]) !!}
                            <span class="help-block" v-for="err in agencyErrors.about">@{{ err }}</span>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-20 select-place-block">
                    <div class="col-xs-12 relative">
                        <h3>Местоположение</h3>
                        <p class="margin-bottom-20" v-bind:class="[agencyErrors['places.0.primary_place'] ? 'text-danger' : '']">Укажите местоположение агентства.</p>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="gray-panel padding-15" style="height: 240px;"  v-bind:style="[agencyErrors['places.0.primary_place']  ? {backgroundColor : '#a94442 !important'} : '']">
                                    <div class="form-group">
                                        <select name="place_region" v-selectpicker="placeRegionAgency" class="form-control selectpicker place-select-region" data-url="{{route('getPlacesByParentId')}}" data-target-list="[name=place_city]">
                                            <option value="" data-hidden="true">Выберите регион...</option>
                                            @foreach($places as $region)
                                                @if($region->type->name == 'region')
                                                    <option value="{{$region->id}}" @if(in_array($region->id, $agency->places->keyBy('id')->keys()->toArray())) selected="selected" @endif >{{$region->display_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select id="place-city" v-selectpicker="placeCityAgency" name="place_city" class="form-control selectpicker place-select-region" @if(!in_array('city',$places->keyBy('type.name')->keys()->toArray())) disabled="disabled" @endif data-url="{{route('getPlacesByParentId')}}" data-target-list="[name=place_district]">
                                            <option value="" data-hidden="true">Выберите город...</option>
                                            @foreach($places as $city)
                                                @if($city->type->name == 'city')
                                                    <option value="{{$city->id}}" @if(in_array($city->id, $agency->places->keyBy('id')->keys()->toArray())) selected="selected" @endif >{{$city->display_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select id="place-district" v-selectpicker="placeDistrictAgency" name="place_district" class="form-control selectpicker place-select-region" @if(!in_array('city_district',$places->keyBy('type.name')->keys()->toArray()) and !in_array('city_metro',$places->keyBy('type.name')->keys()->toArray()) and !in_array('city_global_district',$places->keyBy('type.name')->keys()->toArray())) disabled="disabled" @endif data-url="{{route('getPlacesByParentId')}}" data-target-list="[name=place_address]">
                                            <option value="" data-hidden="true">Выберите район или станцию метро...</option>
                                            @foreach($places as $district)
                                                @if($district->type->name == 'city_district' or $district->type->name == 'city_metro' or $district->type->name == 'city_global_district')
                                                    <option value="{{$district->id}}" @if(in_array($district->id, $agency->places->keyBy('id')->keys()->toArray())) selected="selected" @endif >{{$district->display_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group margin-top-20 place-select-address">
                                        {!! Form::text('place_address', empty($agency_place->address)?'':$agency_place->address, ['class' => 'form-control address-map', 'id' => 'place-address', 'disabled' => empty($agency_place->address)?'':'disabled', 'v-model' => 'placeAddressAgency', 'placeholder' => 'Укажите адрес...']) !!}
                                    </div>
                                </div>
                                <?php $selected_place = ''; ?>
                                @foreach($places as $city)
                                    @if($city->type->name == 'city')
                                        @if(in_array($city->id, $agency->places->keyBy('id')->keys()->toArray())) <?php $selected_place = $city->display_name; ?> @endif
                                    @endif
                                @endforeach
                                {!! Form::hidden('places[0][selected_place]',$selected_place,['class' => 'searchPlaceData', 'v-model' => 'placeCityAgency.text'])!!}
                                {!! Form::hidden('places[0][primary_place]',!empty($agency_place->primary_place)?$agency_place->primary_place:'',['class' => 'primary-place']) !!}
                                {!! Form::hidden('places[0][latitude]',!empty($agency_place->latitude)?$agency_place->latitude:'',['class' => 'latitude']) !!}
                                {!! Form::hidden('places[0][longitude]',!empty($agency_place->longitude)?$agency_place->longitude:'',['class' => 'longitude']) !!}
                                {!! Form::hidden('places[0][address]',!empty($agency_place->address)?$agency_place->address:'',['v-model' => 'placeAddressAgency']) !!}
                            </div>
                            <div class="col-xs-6">
                                <div class="relative">
                                    <div class="text-center map-message" v-show="placeAddressAgency">
                                        <i class="fa fa-forward"></i> Передвиньте маркер на карте в нужное место, если он расположен неверно.
                                    </div>
                                    <div class="map-hover" v-show="!placeAddressAgency">
                                        <div class="map-hover-inner text-center">
                                            <i class="fa fa-home"></i><br>
                                            сначала укажите адрес
                                        </div>
                                    </div>
                                    <div id="agency-select-place-map" class="map-canvas" style="height: 240px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-20">
                    <div class="col-xs-12 relative">
                        <h3>Контакты</h3>
                        <p>Контактная информация.</p>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row duplicate">
                                    <div class="col-xs-4">
                                        {!! Form::select('agency_contact_type', $contactsTypes, [],['id'=>'agency-contact-type-select', 'class'=>'form-control selectpicker', 'v-selectpicker' => 'agencyContactType']) !!}
                                    </div>
                                    <div class="col-xs-5">
                                        {!! Form::text('agency_contact_value', null,['id'=>'agency-contact-type-select-value','class'=>'form-control', 'v-model' => 'agencyContactValue']) !!}
                                    </div>
                                    <div class="col-xs-3">
                                        <button type="button" class="btn btn-primary btn-block" v-on:click="addAgencyContact"><i class="fa fa-plus"></i> добавить контакт</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row margin-top-20" v-if="agencyContacts.length == 0">
                            <div class="col-xs-12">
                                <div class="alert alert-info text-center">
                                    <p>Вы не добавили еще ни одного контакта агентства.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row margin-top-20" v-if="agencyErrors['phone.need']">
                            <div class="col-xs-12">
                                <div class="alert alert-danger text-center">
                                    <p>Добавьте хотя бы один контактный телефон.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($agency->contacts as $key => $contact)
                                {!! Form::hidden('old_contact['.$key.'][type_name]', $contact->type->display_name, ['v-model' => "agencyContacts[".$key."]['type_name']"]) !!}
                                {!! Form::hidden('old_contact['.$key.'][type]', $contact->type->id, ['v-model' => "agencyContacts[".$key."]['type']"]) !!}
                                {!! Form::hidden('old_contact['.$key.'][value]', $contact->value, ['v-model' => "agencyContacts[".$key."]['value']"]) !!}
                            @endforeach
                        </div>
                        <div class="row" v-for="contact in agencyContacts">
                            <div class="col-xs-12">
                                <div class="agency-contact-row" v-bind:style="[agencyErrors['contacts.'+$index+'.value']  ? {color : '#a94442 !important'} : '']">
                                    <span class="pull-left">@{{ contact.type_name }}</span>
                                    <span class="pull-right">@{{ contact.value }} <button type="button" class="btn btn-link" v-on:click="deleteAgencyContact($index)"><i class="fa fa-times"></i></button></span>
                                    <div class="clearfix"></div>
                                    <p v-if="agencyErrors['contacts.'+$index+'.value']" class="text-danger">@{{agencyErrors['contacts.'+$index+'.value']}}</p>
                                </div>
                                {!! Form::hidden('contacts[@{{ $index }}][contact_type]', '@{{ contact.type }}') !!}
                                {!! Form::hidden('contacts[@{{ $index }}][value]', '@{{ contact.value }}') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-20">
                    <div class="col-xs-12 relative text-center">
                        <hr>
                        <div class="row" v-if="agencyErrors.length != 0">
                            <div class="col-xs-12">
                                <div class="alert alert-danger text-center">
                                    <p>При заполнении формы допущены ошибки, пожалуйста, исправьте их и попробуйте еще раз.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row" v-if="saveAgencySuccess">
                            <div class="col-xs-12">
                                <div class="alert alert-success text-center">
                                    <p>Изменения успешно сохранены.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-xs-offset-4">
                                <button type="button" class="btn btn-success btn-block" v-on:click="updateAgency('{{route('getUpdateAgency', $agency->id)}}',$event)">сохранить</button>
                            </div>
                            <div class="col-xs-4 col-xs-offset-4">
                                <a type="button" class="btn btn-link btn-block" href="#" data-toggle="modal" data-url="{{ route('getProfileAgencyDelete',['id' => $agency->id]) }}" data-target="#deleteAgencyModal"><i class="fa fa-times"></i> удалить агентство</a>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            </div>
            @include('frontend.parts.modal',[
                          'id' => 'deleteAgencyModal',
                          'title' => 'Удалить агентсво',
                          'content' => 'После удаления все объявления этого агентства, будут перенесены в архив. Вы действительно хотите удалить это агентство?',
                          'haveCancel' => true,
                          'buttons' => [
                             '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
                          ]
                ])
@endsection