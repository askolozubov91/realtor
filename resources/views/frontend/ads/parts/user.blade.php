<div v-show="!authUser.id && userLoaded">
    <h2>Пользователь</h2>
    <p class="margin-bottom-20">Укажите ваши данные.</p>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#newUser" aria-controls="newUser" role="tab" data-toggle="tab">Я новый пользователь</a></li>
        <li role="presentation"><a href="#oldUser" aria-controls="oldUser" role="tab" data-toggle="tab">У меня уже есть аккаунт</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="newUser">
            <div class="row margin-bottom-20" v-if="registrationErrors.length != 0">
                <div class="col-xs-12">
                    <div class="alert alert-danger">
                        <ul>
                            <li v-for="error in registrationErrors">
                                @{{error}}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            {!! Form::open() !!}
            {!! Form::hidden('user_type', null, ['v-model' => 'registrationUserType']) !!}
            <div class="row">
                <div class="col-xs-6">
                    <div class="row select-user-type">
                        <div class="col-xs-6">
                            <div class="border-gray radius select-block padding-15"
                                 v-bind:class="{'active-block': (registrationUserType == 'owner'?true:false) }"
                                 v-on:click="registrationUserType = 'owner'">
                                <h3><i class="fa fa-user"></i>Собственник недвижимости</h3>
                                <p>Вы владелец недвижимости и не берете дополнительную коммисию от совершенных сделок.</p>
                                <p>Объявления будут помечены, как объявления от собственника.</p>
                                <p>Вы можете разместить до 5 объявлений.</p>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="border-gray radius select-block padding-15"
                                 v-bind:class="{'active-block': (registrationUserType == 'agent'?true:false) }"
                                 v-on:click="registrationUserType = 'agent'">
                                <h3><i class="fa fa-building"></i>Агентство недвижимости или посредник</h3>
                                <p>Вы совершаете сделки, как посредник.</p>
                                <p>Нет ограничения на количество объявлений.</p>
                                <p>Вы можете разместить свою компанию в каталоге агентств.</p>
                                <p>Ваши объявления помечены, как объявления от посредника или агентства.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        {!! Form::label('userName', 'Ваше имя:') !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'userName', 'placeholder' => 'Имя']) !!}
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('userPhone', 'Номер телефона:') !!}
                                {!! Form::text('phone', null, ['class' => 'form-control phone-mask', 'id' => 'userPhone', 'placeholder' => 'Телефон']) !!}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('userEmail', 'Адрес электронной почты:') !!}
                                {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'userEmail', 'placeholder' => 'E-mail']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('userPassword', 'Пароль:') !!}
                                {!! Form::password('password', ['class' => 'form-control', 'id' => 'userPassword']) !!}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('userPasswordConfirm', 'Повторите пароль:') !!}
                                {!! Form::password('passwordConfirm', ['class' => 'form-control', 'id' => 'userPasswordConfirm']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <div class="checkbox">
                                    {!! Form::checkbox('rulesAgree', 1, true, ['id' => 'confirmRules']) !!}<label for="confirmRules">Подтверждаю свое согласие с <a href="#">правилами сайта</a></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::captcha(['id' => 'captcha1']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-xs-offset-6">
                            {!! Form::button('Продолжить', ['type' => 'button', 'class' => 'btn btn-success btn-block', 'v-on:click' => 'registration($event)']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div role="tabpanel" class="tab-pane" id="oldUser">
            <div class="row margin-bottom-20" v-if="authErrors.length != 0">
                <div class="col-xs-12">
                    <div class="alert alert-danger">
                        <ul>
                            <li v-for="error in authErrors">
                                @{{error}}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            {!! Form::open(['class' => 'auth-form']) !!}
            <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <div class="form-group">
                        {!! Form::label('userName', 'Ваше имя:') !!}
                        {!! Form::text('emailOrPhone', null, ['class' => 'form-control', 'id' => 'userName', 'placeholder' => 'Имя']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('userPassword', 'Пароль:') !!}
                        {!! Form::password('password', ['class' => 'form-control', 'id' => 'userPassword']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::captcha(['id' => 'captcha2']) !!}
                    </div>
                    <div class="row margin-bottom-20">
                        <div class="col-xs-6">
                            <div class="checkbox">
                                {!! Form::checkbox('rememberMe', 1, true, ['id' => 'rememberMe']) !!}<label for="rememberMe">запомнить меня</label>
                            </div>
                        </div>
                        <div class="col-xs-6 text-right">
                            <a href="#">напомнить пароль</a>
                        </div>
                    </div>
                    {!! Form::button('Войти', ['type' => 'button', 'class' => 'btn btn-success btn-block btn-lg', 'v-on:click' => 'auth($event)']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div v-show="authUser.id">
    <div v-show="(authUser.email && authUser.confirmed_email == 0) || (authUser.phone && authUser.confirmed_phone == 0)">
        <h2>Пользователь</h2>
        {!! Form::open() !!}
        <p class="margin-bottom-20">Подтвердите ваши данные.</p>
        <div class="border-gray radius padding-15">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2 margin-bottom-20">
                    <h4 class="margin-bottom-30">Подтвердите ваши данные</h4>
                    <div class="row margin-bottom-20" v-if="confirmErrors.length != 0">
                        <div class="col-xs-12">
                            <div class="alert alert-danger">
                                <ul>
                                    <li v-for="error in confirmErrors">
                                        @{{error}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row margin-bottom-20" v-if="confirmMessage.length != 0">
                        <div class="col-xs-12">
                            <div class="alert alert-success">
                                <ul>
                                    <li v-for="message in confirmMessage">
                                        @{{message}}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div v-show="authUser.phone && authUser.confirmed_phone == 0">
                        <p>На указанный вами телефоный номер <span class="text-primary">+@{{ authUser.phone }}</span> был отправлен код подтверждения, введите его, что бы подтвердить номер.</p>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('phoneConfirmCode', 'Код подтверждения телефона:') !!}</strong>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-xs-7" v-bind:class="[adErrosStatus == 403 ? 'has-error' : '']">
                                {!! Form::text('phone_confirm_code', null, ['class' => 'form-control', 'id' => 'phoneConfirmCode']) !!}
                            </div>
                            <div class="col-xs-5">
                                <button class="btn btn-link" type="button" v-on:click="sendConfirmPhone">повторно выслать код</button>
                            </div>
                        </div>
                    </div>
                    <div v-show="authUser.email && authUser.confirmed_email == 0">
                        <p class="margin-top-30">На указанный вами E-mail <span class="text-primary">@{{ authUser.email }}</span> был отправлен код подтверждения, введите его, что бы подтвердить адрес.</p>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('emailConfirmCode', 'Код подтверждения электронной почты:') !!}</strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7" v-bind:class="[adErrosStatus == 403 ? 'has-error' : '']">
                                {!! Form::text('email_confirm_code', null, ['class' => 'form-control', 'id' => 'emailConfirmCode']) !!}
                            </div>
                            <div class="col-xs-5">
                                <button class="btn btn-link" type="button" v-on:click="sendConfirmEmail">повторно выслать код</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 margin-top-20">
                            {!! Form::button('Подтвердить данные', ['type' => 'button', 'class' => 'btn btn-success btn-block btn-lg', 'v-on:click' => 'confirmContacts($event)']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-offset-2 col-xs-8 margin-top-20 text-center">
                            <p>Если вы указали неправильный номер телефона или адрес электронной почты, то вы можете их изменить в <a href="#">настройках</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <div v-show="(authUser.email && authUser.confirmed_email != 0) && (authUser.phone && authUser.confirmed_phone != 0)">
        <h2>Пользователь</h2>
        <p class="margin-bottom-20">Данные, которые будут указаны в объявлении.</p>
        <div class="border-gray radius padding-15 ad-user-data">
            <div class="row margin-top-20 margin-bottom-20">
                <div v-bind:class="[(authUser.user_type.name == 'agent' && authUser.agencies.length == 0)?'col-xs-7':'col-xs-12']">
                    <h3>Что будет указано в объявлении</h3>
                    <p v-if="authUser.user_type.name == 'agent' && authUser.agencies.length > 0" class="margin-bottom-20">Выберите кто будет автором объявления:</p>
                    <div>
                        {!! Form::hidden('selected_ad_author',(empty($ad->agency_id)?'':$ad->agency_id),['v-model' => 'selectedAdAuthorAgency']) !!}
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" v-bind:class="[(!selectedAdAuthorAgency)?'active':'']"><a v-on:click="selectedAdAuthorAgency = null" href="#author-tab-user" aria-controls="author-tab-user" role="tab" data-toggle="tab"><input type="radio" v-bind:checked="(!selectedAdAuthorAgency)?'checked':''" ><label>@{{ authUser.name }}</label></a></li>
                            <li role="presentation" v-bind:class="[(selectedAdAuthorAgency == agency.id)?'active':'']" v-for="agency in authUser.agencies"><a v-on:click="selectedAdAuthorAgency = agency.id" href="#author-tab-agency-@{{ agency.id }}" aria-controls="profile" role="tab" data-toggle="tab"><input type="radio" v-bind:checked="(selectedAdAuthorAgency == agency.id)?'checked':''" ><label>@{{ agency.name }}</label></a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane" v-bind:class="[(!selectedAdAuthorAgency)?'active':'']" id="author-tab-user">
                                <ul class="table-list">
                                    <li>
                                                        <span class="table-list-title">
                                                            <span>Ваше имя:</span>
                                                        </span>
                                                        <span class="table-list-value">
                                                            @{{ authUser.name }}
                                                        </span>
                                    </li>
                                    <li>
                                                        <span class="table-list-title">
                                                            <span>Номер телефона:</span>
                                                        </span>
                                                        <span class="table-list-value">
                                                            @{{ (authUser.phone)?'+'+authUser.phone:'не указан' }}
                                                        </span>
                                    </li>
                                    <li>
                                                        <span class="table-list-title">
                                                            <span>Адрес электронной почты:</span>
                                                        </span>
                                                        <span class="table-list-value">
                                                            @{{ (authUser.email)?authUser.email:'не указан' }}
                                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div role="tabpanel" class="tab-pane" v-bind:class="[(selectedAdAuthorAgency == agency.id)?'active':'']" v-for="agency in authUser.agencies" id="author-tab-agency-@{{ agency.id }}">
                                <ul class="table-list">
                                    <li>
                                                        <span class="table-list-title">
                                                            <span>Название:</span>
                                                        </span>
                                                        <span class="table-list-value">
                                                            @{{ agency.name }}
                                                        </span>
                                    </li>
                                    <li>
                                                        <span class="table-list-title">
                                                            <span>Телефоны:</span>
                                                        </span>
                                                        <span class="table-list-value">
                                                            <span v-for="contact in agency.contacts" v-if="contact.type.name == 'phone'" v-text="contact.value+' '"></span>
                                                        </span>
                                    </li>
                                    <li>
                                                        <span class="table-list-title">
                                                            <span>Адреса электронной почты:</span>
                                                        </span>
                                                        <span class="table-list-value">
                                                            <span v-for="contact in agency.contacts" v-if="contact.type.name == 'email'" v-text="contact.value+' '"></span>
                                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5">
                    <div v-show="authUser.user_type.name == 'agent' && authUser.agencies.length == 0" class="border-gray radius">
                        <div class="profile-add-agency add-agency-with-padding">
                            @include('frontend.parts.promo_create_agency')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>