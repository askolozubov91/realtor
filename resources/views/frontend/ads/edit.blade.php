@extends('frontend.layouts.main')

@section('breadcrumbs')
    {!! Breadcrumbs::render('getCreateAd') !!}
@endsection

@section('title')
    Редактирование объявления №{{$ad->id}}
@endsection

@section('content')
    @if($ad->status->name == 'publicated')
        <div class="row margin-top-20">
            <div class="col-xs-12">
                <div class="alert alert-warning">
                    <p>Внимание! Ваше объявление опубликовано, после внесения изменений оно будет заново отправлено на модерацию.</p>
                </div>
            </div>
        </div>
    @endif
    <div class="row margin-top-20">
        <div class="col-xs-12">
            <div class="relative" style="min-height: 300px;">
                @include('frontend.ads.parts.user')
            </div>
        </div>
    </div>
    {!! Form::open(['id'=>'saveAdForm']) !!}
    {!! Form::hidden('type_id',$ad->primary_type) !!}
    {!! Form::hidden('agency_id',null,['v-model'=>'selectedAdAuthorAgency']) !!}
    <div class="relative">
        <div class="row margin-top-20">
            <div class="col-xs-12 relative">
                <h2>Фотографии</h2>
                <p class="margin-bottom-20">Загрузите несколько фотографий.</p>
                <div class="edit-ad-photos block-images-upload border-gray radius">
                    <div class="block-images-button">
                        <div class="fileinput-button" id="imagesUploadButton">
                            <span class="btn btn-primary btn-block"><i class="fa fa-image"></i> Загрузить фото</span>
                            <input id="imagesUpload" type="file" name="file" data-url="{{route('postUploadImage')}}" multiple>
                        </div>
                    </div>
                    <div class="images-list">
                        <div class="row" v-if="adUploadedPhotos.length == 0">
                            <div class="col-xs-12">
                                <div class="text-center no-photo-uploaded">
                                    <div><i class="fa fa-image"></i></div>
                                    <div><p>Нажмите на кнопку "Загрузить фото", что бы выбрать изображения для загрузки.</p></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="hidden uploaded-ad-photo-main" data-id="{{$ad->main_image_id}}"></div>
                            @foreach($ad->images as $photo)
                                <div class="hidden uploaded-ad-photo-data"
                                     data-id="{{$photo->id}}"
                                     data-url="{{ImageResize::make($photo->path, ['w'=>1920, 'h'=>1920])}}"
                                     data-thumbnail-url="{{ImageResize::make($photo->path, ['w'=>150, 'h'=>150, 'c'=>true ])}}"
                                     data-name="{{$photo->name}}"
                                     data-delete-url="{{route('getImageDelete', $photo->id)}}"
                                     data-delete-type="GET"
                                ></div>
                            @endforeach
                            <div class="col-xs-1" v-for="photo in adUploadedPhotos">
                                <span class="remove-image" v-on:click="removeAdPhoto(photo.delete_url)"></span>
                                        <span class="thumbnail" v-bind:class="adUploadedPhotoMainId == photo.id?'active':''" v-on:click="adUploadedPhotoMainId = photo.id">
                                            <img :src="photo.thumbnail_url" src="#">
                                        </span>
                                {!! Form::hidden('images[]','@{{photo.id}}') !!}
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::hidden('main_image_id','',['v-model' => 'adUploadedPhotoMainId'])!!}
            </div>
        </div>
        <div class="relative">
            <div class="row margin-top-20">
                <div class="col-xs-12">
                    <h2>Заголовок</h2>
                    <p class="margin-bottom-20">Напишите заголовок объявления.</p>
                    <div class="gray-panel border-gray radius padding-15" v-bind:class="adTypeFields?'drop-shadow':''">
                        <div v-bind:class="[adErros.title ? 'has-error' : '']">
                            {!! Form::text('title', $ad->title, ['class' => 'form-control']) !!}
                            <span class="help-block" v-for="err in adErros.title">@{{ err }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-top-20">
                <div class="col-xs-8">
                    <h2>Текст объявления</h2>
                    <p class="margin-bottom-20">Подробно опишите ваше предложение.</p>
                    <div v-bind:class="[adErros.content ? 'has-error' : '']">
                        {!! Form::textarea('content', $ad->content, ['class' => 'form-control', 'rows' => 6]) !!}
                        <span class="help-block" v-for="err in adErros.content">@{{ err }}</span>
                    </div>
                </div>
                <div class="col-xs-4">
                    <h2>Цена</h2>
                    <p class="margin-bottom-20">@{{ (typeof adTypeFields.price != 'undefined')?adTypeFields.price.display_description:'Укажите цену.' }}</p>
                    <div class="block-price gray-panel border-gray radius padding-15">
                        <div class="row label-after-field">
                            <div class="col-xs-4">
                                {!! Form::text('price', $ad->price, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-xs-8">
                                <label>рублей</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="checkbox">
                                    {!! Form::checkbox('nullPrice', 1, false, ['id' => 'nullPrice']) !!}<label for="nullPrice">цена по-договоренности (не указывать цену)</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-top-20">
                <div class="col-xs-12">
                    <h2>Параметры объявления</h2>
                    <p class="margin-bottom-20">Укажите дополнительные параметры объявления.</p>
                    <div class="row">
                        @foreach($typeFields as $name => $field)
                            @if(!in_array($name, ['address', 'price', 'latitude', 'longitude']))
                                <div class="col-xs-3">
                                    @if($field['editor'] == 'text')
                                        <div class="form-group" v-bind:class="[adErros[index] ? 'has-error' : '']">
                                            <label for="{{$name}}_param">{{$field['display_name']}}</label>
                                            <input id="{{$name}}_param" value="{{$ad[$name]}}" class="form-control" type="text" name="{{$name}}">
                                        </div>
                                    @endif
                                    @if($field['editor'] == 'select')
                                        <div class="form-group" v-bind:class="[adErros[index] ? 'has-error' : '']">
                                            <label for="{{$name}}_param">{{$field['display_name']}}</label>
                                            <select class="selectpicker form-control" data-none-selected-text="Ничего не выбрано..." id="{{$name}}_param" class="form-control" type="text" name="{{$name}}">
                                                <option value="">Ничего не выбрано...</option>
                                                <option data-divider="true"></option>
                                                @foreach($field['values'] as $value => $valueName)
                                                    <option value="{{$value}}" @if($value == $ad[$name]) selected="selected" @endif >
                                                        {{ $valueName }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    @if($field['editor'] == 'group')
                                        <div class="form-group">
                                            <label for="{{$name}}_param">{{$field['display_name']}}</label>
                                            <select class="selectpicker form-control" data-none-selected-text="Ничего не выбрано..." multiple="multiple" id="{{$name}}_param" class="form-control" type="text" name="group_field[{{$name}}][]">
                                                @foreach($field['values'] as $value => $valueData)
                                                    <option value="{{$value}}" @if(!empty($ad[$value])) selected="selected" @endif >
                                                        {{ $valueData['display_name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            @if(isset($typeFields['address']) and isset($typeFields['latitude']) and isset($typeFields['longitude']))
                <div class="row margin-top-20 select-place-block">
                <div class="col-xs-12 relative">
                    <div class="loader" v-if="showPlaceLoader"></div>
                    <h2>Местоположение</h2>
                    <p class="margin-bottom-20" v-bind:class="[adErros.primary_place ? 'text-danger' : '']">Укажите адрес объекта.</p>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="gray-panel padding-15" style="height: 240px;"  v-bind:style="[adErros.primary_place ? {backgroundColor : '#a94442 !important'} : '']">
                                <div class="form-group">
                                    <select name="place_region" v-selectpicker="placeRegion" class="form-control selectpicker place-select-region" data-url="{{route('getPlacesByParentId')}}" data-target-list="[name=place_city]">
                                        <option value="" data-hidden="true">Выберите регион...</option>
                                        @foreach($places as $region)
                                            @if($region->type->name == 'region')
                                                <option value="{{$region->id}}" @if(in_array($region->id, $ad->places->keyBy('id')->keys()->toArray())) selected="selected" @endif >{{$region->display_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select id="place-city" v-selectpicker="placeCity" name="place_city" class="form-control selectpicker place-select-region" @if(!in_array('city',$places->keyBy('type.name')->keys()->toArray())) disabled="disabled" @endif data-url="{{route('getPlacesByParentId')}}" data-target-list="[name=place_district]">
                                        <option value="" data-hidden="true">Выберите город...</option>
                                        @foreach($places as $city)
                                            @if($city->type->name == 'city')
                                                <option value="{{$city->id}}" @if(in_array($city->id, $ad->places->keyBy('id')->keys()->toArray())) selected="selected" @endif >{{$city->display_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select id="place-district" v-selectpicker="placeDistrict" name="place_district" class="form-control selectpicker place-select-region" @if(!in_array('city_district',$places->keyBy('type.name')->keys()->toArray()) and !in_array('city_metro',$places->keyBy('type.name')->keys()->toArray()) and !in_array('city_global_district',$places->keyBy('type.name')->keys()->toArray())) disabled="disabled" @endif data-url="{{route('getPlacesByParentId')}}" data-target-list="[name=place_address]">
                                        <option value="" data-hidden="true">Выберите район или станцию метро...</option>
                                        @foreach($places as $district)
                                            @if($district->type->name == 'city_district' or $district->type->name == 'city_metro' or $district->type->name == 'city_global_district')
                                                <option value="{{$district->id}}" @if(in_array($district->id, $ad->places->keyBy('id')->keys()->toArray())) selected="selected" @endif >{{$district->display_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group margin-top-20 place-select-address">
                                    <input id="place-address" class="form-control address-map" type="text" value="{{ $ad['address'] }}" name="place_address" placeholder="Укажите адрес..." @if(empty($ad['address']))disabled="disabled" @endif v-model="placeAddress">
                                </div>
                            </div>
                            {!! Form::hidden('searchPlaceData','',['class' => 'searchPlaceData', 'v-model' => 'placeCity.text'])!!}
                            {!! Form::hidden('primary_place',$ad['primary_place'],['class' => 'primary-place']) !!}
                            {!! Form::hidden('latitude',$ad['latitude'],['class' => 'latitude']) !!}
                            {!! Form::hidden('longitude',$ad['longitude'],['class' => 'longitude']) !!}
                        </div>
                        <div class="col-xs-6">
                            <div class="relative">
                                <div class="text-center map-message" v-show="placeAddress">
                                    <i class="fa fa-forward"></i> Передвиньте маркер на карте в нужное место, если он расположен неверно.
                                </div>
                                <div class="map-hover" v-show="!placeAddress">
                                    <div class="map-hover-inner text-center">
                                        <i class="fa fa-home"></i><br>
                                        сначала укажите адрес
                                    </div>
                                </div>
                                <div id="ad-select-place-map" class="map-canvas" style="height: 240px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div id="publication-type" class="row margin-top-20 select-place-block">
                <div class="col-xs-12 relative">
                    <h2>Публикация объявления</h2>
                    <p class="margin-bottom-20">Выберите дополнительные опции публикации и укажите количество дней.</p>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="border-gray radius ad-publication-type"  v-bind:class="adTypeFields?'drop-shadow':''">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="border-gray ad-type-item">
                                            <div class="ad-type-item-header">
                                                <span class="ad-type-item-header-icon"><img src="/images/publication_type_icon_1.png" width="45" height="45"></span><span class="ad-type-item-header-text">Выделить цветом</span>
                                            </div>
                                            <div class="ad-type-item-content gray-panel">
                                                <ul class="fa-ul">
                                                    <li><i class="fa-li fa fa-angle-right"></i>Объявление выделено цветом.</li>
                                                    <li><i class="fa-li fa fa-angle-right"></i>Повышено количество просмотров объявления.</li>
                                                </ul>
                                            </div>
                                            <div class="ad-type-item-slider">
                                                <?php
                                                    $days = 7;
                                                    $date_off = false;
                                                    if($ad->premium->where('name', 'color_marker')->first()){
                                                        $date_off = $ad->premium->where('name', 'color_marker')->first()->pivot->off_datetime;
                                                        $days = $days - \Carbon\Carbon::parse($date_off)->diffInDays(\Carbon\Carbon::now()) - 1;
                                                    }
                                                    $ticks = [];
                                                    for($i = 0; $i <= $days; $i++){
                                                        $ticks[] = $i;
                                                    }
                                                    $ticksLabels = [];
                                                    for($i = 0; $i <= $days; $i++){
                                                        switch($i){
                                                            case 0:
                                                                $ticksLabels[] = '"0 дней"';
                                                                break;
                                                            case $days:
                                                                $ticksLabels[] = '"'.$days.' дней"';
                                                                break;
                                                            default:
                                                                $ticksLabels[] = '""';
                                                        }
                                                    }
                                                ?>
                                                <input class="data-slider" name="ad_promo[color_marker]" v-range="adPublicationColorMarker" type="text" value="0" data-slider-value="0" data-slider-ticks="[{{implode(',',$ticks)}}]" data-slider-ticks-snap-bounds="1" data-slider-ticks-labels='[{{implode(',',$ticksLabels)}}]' data-slider-tooltip="always"/>
                                            </div>
                                            <div class="ad-type-item-footer">
                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <div class="ad-type-item-count">
                                                            За <input type="text" class="form-control" v-model="adPublicationColorMarker" disabled="disabled"> дней
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-5">
                                                        <div class="ad-type-item-price">
                                                            <span class="ad-type-item-total" v-text="adPublicationColorMarker * {{App\AdPremiumRate::getRateByName('color_marker')->price}}"></span><i class="fa fa-rub"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center margin-top-20">
                                            @if($days < 7)
                                                <p>Объявление уже выделено цветом. <strong><span class="format-date-time">{{$date_off}}</span></strong> истекает срок действия опции. Количество дней, на которые можно продлить: <strong>{{$days}}</strong></p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="border-gray ad-type-item">
                                            <div class="ad-type-item-header">
                                                <span class="ad-type-item-header-icon"><img src="/images/publication_type_icon_2.png" width="45" height="45"></span><span class="ad-type-item-header-text">Премиум размещение</span>
                                            </div>
                                            <div class="ad-type-item-content gray-panel">
                                                <ul class="fa-ul">
                                                    <li><i class="fa-li fa fa-angle-right"></i>Премиум объявления отображаются в начале списка.</li>
                                                    <li><i class="fa-li fa fa-angle-right"></i>Повышено количество просмотров объявления.</li>
                                                </ul>
                                            </div>
                                            <div class="ad-type-item-slider">
                                                <?php
                                                $days = 7;
                                                $date_off = false;
                                                if($ad->premium->where('name', 'publication_premium')->first()){
                                                    $date_off = $ad->premium->where('name', 'publication_premium')->first()->pivot->off_datetime;
                                                    $days = $days - \Carbon\Carbon::parse($date_off)->diffInDays(\Carbon\Carbon::now()) - 1;
                                                }
                                                $ticks = [];
                                                for($i = 0; $i <= $days; $i++){
                                                    $ticks[] = $i;
                                                }
                                                $ticksLabels = [];
                                                for($i = 0; $i <= $days; $i++){
                                                    switch($i){
                                                        case 0:
                                                            $ticksLabels[] = '"0 дней"';
                                                            break;
                                                        case $days:
                                                            $ticksLabels[] = '"'.$days.' дней"';
                                                            break;
                                                        default:
                                                            $ticksLabels[] = '""';
                                                    }
                                                }
                                                ?>
                                                <input class="data-slider" name="ad_promo[publication_premium]" v-range="adPublicationPremium" type="text" value="0" data-slider-value="0" data-slider-ticks="[{{implode(',',$ticks)}}]" data-slider-ticks-snap-bounds="1" data-slider-ticks-labels='[{{implode(',',$ticksLabels)}}]' data-slider-tooltip="always"/>
                                            </div>
                                            <div class="ad-type-item-footer">
                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <div class="ad-type-item-count">
                                                            За <input type="text" class="form-control" v-model="adPublicationPremium" disabled="disabled"> дней
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-5">
                                                        <div class="ad-type-item-price">
                                                            <span class="ad-type-item-total" v-text="adPublicationPremium * {{App\AdPremiumRate::getRateByName('publication_premium')->price}}"></span><i class="fa fa-rub"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center margin-top-20">
                                            @if($days < 7)
                                                <p>Объявление уже находится в премиум-размещении. <strong><span class="format-date-time">{{$date_off}}</span></strong> истекает срок действия опции. Количество дней, на которые можно продлить: <strong>{{$days}}</strong></p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="border-gray ad-type-item">
                                            <div class="ad-type-item-header">
                                                <span class="ad-type-item-header-icon"><img src="/images/publication_type_icon_3.png" width="45" height="45"></span><span class="ad-type-item-header-text">Поднять объявление</span>
                                            </div>
                                            <div class="ad-type-item-content gray-panel">
                                                <ul class="fa-ul">
                                                    <li><i class="fa-li fa fa-angle-right"></i>Объявление автоматически попадает в начало списка раз в сутки.</li>
                                                    <li><i class="fa-li fa fa-angle-right"></i>Повышено количество просмотров объявления.</li>
                                                </ul>
                                            </div>
                                            <div class="ad-type-item-slider">
                                                <?php
                                                $days = 7;
                                                $date_off = false;
                                                if($ad->premium->where('name', 'publication_up')->first()){
                                                    $date_off = $ad->premium->where('name', 'publication_up')->first()->pivot->off_datetime;
                                                    $days = $days - \Carbon\Carbon::parse($date_off)->diffInDays(\Carbon\Carbon::now()) - 1;
                                                }
                                                $ticks = [];
                                                for($i = 0; $i <= $days; $i++){
                                                    $ticks[] = $i;
                                                }
                                                $ticksLabels = [];
                                                for($i = 0; $i <= $days; $i++){
                                                    switch($i){
                                                        case 0:
                                                            $ticksLabels[] = '"0 дней"';
                                                            break;
                                                        case $days:
                                                            $ticksLabels[] = '"'.$days.' дней"';
                                                            break;
                                                        default:
                                                            $ticksLabels[] = '""';
                                                    }
                                                }
                                                ?>
                                                <input class="data-slider" name="ad_promo[publication_up]" v-range="adPublicationUp" type="text" value="0" data-slider-value="0" data-slider-ticks="[{{implode(',',$ticks)}}]" data-slider-ticks-snap-bounds="1" data-slider-ticks-labels='[{{implode(',',$ticksLabels)}}]' data-slider-tooltip="always"/>
                                            </div>
                                            <div class="ad-type-item-footer">
                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <div class="ad-type-item-count">
                                                            За <input type="text" class="form-control" v-model="adPublicationUp" disabled="disabled"> дней
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-5">
                                                        <div class="ad-type-item-price">
                                                            <span class="ad-type-item-total" v-text="adPublicationUp * {{App\AdPremiumRate::getRateByName('publication_up')->price}}"></span><i class="fa fa-rub"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center margin-top-20">
                                            @if($days < 7)
                                                <p>Автоматическое поднятие объявления уже установлено. <strong><span class="format-date-time">{{$date_off}}</span></strong> истекает срок действия опции. Количество дней, на которые можно продлить: <strong>{{$days}}</strong></p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-top-50">
                <div class="col-xs-12">
                    <div class="alert alert-danger text-center" v-if="adErrosStatus == 403">
                        <p>Подтвердите адрес электронной почты или номер телефона в верхней части страницы, после этого вы сможете опубликовать объявление.</p>
                    </div>
                    <div class="alert alert-danger text-center" v-if="adErros.length != 0">
                        <p>При заполнении формы объявления допущены ошибки, исправьте их, после этого вы сможете опубликовать объявление.</p>
                    </div>
                    <div class="alert alert-danger text-center" v-if="adPayError">
                        <p>@{{{adPayError}}}</p>
                    </div>
                </div>
                <div class="col-xs-3 col-xs-offset-3">
                    <button type="button" class="btn btn-primary btn-block" v-on:click="saveAd('{{route('postSaveAd', $ad->id)}}','just_save', $event)">сохранить черновик</button>
                </div>
                <div class="col-xs-3">
                    <button type="button" v-on:click="saveAd('{{route('postSaveAd', $ad->id)}}','publish', $event)" class="btn btn-success btn-block" v-text="(adPublicationColorMarker+adPublicationPremium+adPublicationUp>0)?'опубликовать за '+(adPublicationColorMarker*{{App\AdPremiumRate::getRateByName('color_marker')->price}}+adPublicationPremium*{{App\AdPremiumRate::getRateByName('publication_premium')->price}}+adPublicationUp*{{App\AdPremiumRate::getRateByName('publication_up')->price}})+' руб.':'опубликовать бесплатно'"></button>
                </div>
            </div>
            @if($ad->status->name != 'archive')
                <div class="row margin-top-20">
                    <div class="col-xs-4 col-xs-offset-4">
                        <a type="button" class="btn btn-link btn-block" href="#" data-toggle="modal" data-url="{{ route('getAdStatus',['statusName' => 'archive', 'id' => $ad->id]) }}" data-target="#sendToArchiveModal"><i class="fa fa-times"></i> удалить объявление</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {!! Form::close() !!}
    {!! Captcha::scriptWithCallback(['captcha1', 'captcha2']) !!}
    <div class="margin-top-20"></div>
    @include('frontend.parts.modal',[
                  'id' => 'sendToArchiveModal',
                  'title' => 'Перенести в архив',
                  'content' => 'Вы действительно хотите перенести это объявление в архив?',
                  'haveCancel' => true,
                  'buttons' => [
                     '<a href="#" class="btn btn-danger modal-url-btn">Перенести в архив</a>'
                  ]
        ])
@endsection