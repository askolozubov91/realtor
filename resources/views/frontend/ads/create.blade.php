@extends('frontend.layouts.main')

@section('breadcrumbs')
    {!! Breadcrumbs::render('getCreateAd') !!}
@endsection

@section('title')
    Создание объявления
@endsection

@section('content')
        <div class="row margin-top-20">
            <div class="col-xs-12">
                <div class="relative" style="min-height: 300px;">
                    <div class="loader" v-show="showLoader"></div>
                    @include('frontend.ads.parts.user')
                </div>
            </div>
        </div>
        {!! Form::open(['url' => route('postCreateAd'), 'id' => 'saveAdForm']) !!}
            {!! Form::hidden('agency_id',null,['v-model'=>'selectedAdAuthorAgency']) !!}
            <div class="relative">
            <div class="no-active" v-show="!authUser.id"></div>
            <div class="row margin-top-20">
                <div class="col-xs-8 relative">
                    <div class="loader" v-show="showAdTypeLoader"></div>
                    <h2>Раздел</h2>
                    <p class="margin-bottom-20">Выберите раздел для публикации объявления.</p>
                    <div class="edit-ad-type block-list border-gray radius">
                        <?php $depth = -1; ?>
                        @foreach($types as $type)
                            @if($type['depth'] == $depth)
                                </li><li>
                                    @elseif($type['depth'] > $depth)
                                        <ul><li>
                                                @elseif($type['depth'] < $depth)
                                                    @for($i = 0; $i < ($depth - $type['depth']); $i++)
                                            </li></ul>
                                @endfor
                                <li>
                                    @endif
                                    <span class="select-ad-type block-list-item" v-bind:class="selectedTypeId == '{{$type['id']}}'?'active':''" v-on:click="selectAdType('{{route('getAdTypeFields', $type['id'])}}', {{$type['id']}})">{{$type['display_name']}}</span>
                                    <?php $depth = $type['depth']; ?>
                                    @endforeach
                                    @for($i = 0; $i <= $depth; $i++)
                                </li></ul>
                                @endfor
                        {!! Form::hidden('type_id',null,['v-model'=>'selectedTypeId']) !!}
                    </div>
                </div>
                <div class="col-xs-4 relative">
                    <div class="no-active" v-show="!adTypeFields && authUser.id"></div>
                    <h2>Фотографии</h2>
                    <p class="margin-bottom-20">Загрузите несколько фотографий.</p>
                    <div class="edit-ad-photos block-images-upload border-gray radius">
                        <div class="block-images-button">
                            <div class="fileinput-button" id="imagesUploadButton">
                                <span class="btn btn-primary btn-block"><i class="fa fa-image"></i> Загрузить фото</span>
                                <input id="imagesUpload" type="file" name="file" data-url="{{route('postUploadImage')}}" multiple>
                            </div>
                        </div>
                        <div class="images-list">
                            <div class="row" v-if="adUploadedPhotos.length == 0">
                                <div class="col-xs-12">
                                    <div class="text-center no-photo-uploaded">
                                        <div><i class="fa fa-image"></i></div>
                                        <div><p>Нажмите на кнопку "Загрузить фото", что бы выбрать изображения для загрузки.</p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-xs-3" v-for="photo in adUploadedPhotos">
                                        <span class="remove-image" v-on:click="removeAdPhoto(photo.delete_url)"></span>
                                        <span class="thumbnail" v-bind:class="adUploadedPhotoMainId == photo.id?'active':''" v-on:click="adUploadedPhotoMainId = photo.id">
                                            <img :src="photo.thumbnail_url" src="#">
                                        </span>
                                        {!! Form::hidden('images[]','@{{photo.id}}') !!}
                                    </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::hidden('main_image_id','',['v-model' => 'adUploadedPhotoMainId'])!!}
                </div>
            </div>
            <div class="relative">
                <div class="no-active" v-show="!adTypeFields && authUser.id"></div>
                <div class="row margin-top-20">
                    <div class="col-xs-12">
                        <h2>Заголовок</h2>
                        <p class="margin-bottom-20">Напишите заголовок объявления.</p>
                        <div class="gray-panel border-gray radius padding-15" v-bind:class="adTypeFields?'drop-shadow':''">
                            <div v-bind:class="[adErrors.title ? 'has-error' : '']">
                                {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                <span class="help-block" v-for="err in adErrors.title">@{{ err }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-20">
                    <div class="col-xs-8">
                        <h2>Текст объявления</h2>
                        <p class="margin-bottom-20">Подробно опишите ваше предложение.</p>
                        <div v-bind:class="[adErrors.content ? 'has-error' : '']">
                            {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => 6]) !!}
                            <span class="help-block" v-for="err in adErrors.content">@{{ err }}</span>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <h2>Цена</h2>
                        <p class="margin-bottom-20">@{{ (typeof adTypeFields.price != 'undefined')?adTypeFields.price.display_description:'Укажите цену.' }}</p>
                        <div class="block-price gray-panel border-gray radius padding-15">
                            <div class="row label-after-field">
                                <div class="col-xs-4">
                                    {!! Form::text('price', 0, ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-xs-8">
                                    <label>рублей</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="checkbox">
                                        {!! Form::checkbox('nullPrice', 1, false, ['id' => 'nullPrice']) !!}<label for="nullPrice">цена по-договоренности (не указывать цену)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-20" v-if="adParamsFields">
                    <div class="col-xs-12">
                        <h2>Параметры объявления</h2>
                        <p class="margin-bottom-20">Укажите дополнительные параметры объявления.</p>
                        <div class="row">
                            <template v-for="(index, field) in adParamsFields">
                                <div class="col-xs-3">
                                    <div class="form-group" v-if="field.editor == 'text'" v-bind:class="[adErrors[index] ? 'has-error' : '']">
                                        <label for="@{{ index }}_param">@{{ field.display_name }}</label>
                                        <input id="@{{ index }}_param" class="form-control" type="text" name="@{{ index }}">
                                    </div>
                                    <div class="form-group" v-if="field.editor == 'select'" v-bind:class="[adErrors[index] ? 'has-error' : '']">
                                        <label for="@{{ index }}_param">@{{ field.display_name }}</label>
                                        <select class="selectpicker form-control" data-none-selected-text="Ничего не выбрано..." id="@{{ index }}_param" class="form-control" type="text" name="@{{ index }}">
                                            <option value="">Ничего не выбрано...</option>
                                            <option data-divider="true"></option>
                                            <option v-for="(i,value) in field.values" v-bind:value="i">
                                                @{{ value }}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group" v-if="field.editor == 'group'">
                                        <label for="@{{ index }}_param">@{{ field.display_name }}</label>
                                        <select class="selectpicker form-control" data-none-selected-text="Ничего не выбрано..." multiple="multiple" id="@{{ index }}_param" class="form-control" type="text" name="group_field[@{{ index }}][]">
                                            <option v-for="(i,value) in field.values" v-bind:value="i">
                                                @{{ value.display_name }}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-20 select-place-block" v-show="adTypeFields.address && adTypeFields.latitude && adTypeFields.longitude">
                    <div class="col-xs-12 relative">
                        <div class="loader" v-if="showPlaceLoader"></div>
                        <h2>Местоположение</h2>
                        <p class="margin-bottom-20" v-bind:class="[adErrors.primary_place ? 'text-danger' : '']">Укажите адрес объекта.</p>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="gray-panel padding-15" style="height: 240px;"  v-bind:style="[adErrors.primary_place ? {backgroundColor : '#a94442 !important'} : '']">
                                    <div class="form-group">
                                        <select name="place_region" v-selectpicker="placeRegion" class="form-control selectpicker place-select-region" data-url="{{route('getPlacesByParentId')}}" data-target-list="[name=place_city]">
                                            <option value="" data-hidden="true">Выберите регион...</option>
                                            @foreach($places_regions as $region)
                                                <option value="{{$region->id}}">{{$region->display_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select id="place-city" v-selectpicker="placeCity" name="place_city" class="form-control selectpicker place-select-region" disabled="disabled" data-url="{{route('getPlacesByParentId')}}" data-target-list="[name=place_district]">
                                            <option value="" data-hidden="true">Выберите город...</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select id="place-district" v-selectpicker="placeDistrict" name="place_district" class="form-control selectpicker place-select-region" disabled="disabled" data-url="{{route('getPlacesByParentId')}}" data-target-list="[name=place_address]">
                                            <option value="" data-hidden="true">Выберите район или станцию метро...</option>
                                        </select>
                                    </div>
                                    <div class="form-group margin-top-20 place-select-address">
                                        {!! Form::text('place_address', null, ['class' => 'form-control address-map', 'id' => 'place-address', 'disabled' => 'disabled', 'v-model' => 'placeAddress', 'placeholder' => 'Укажите адрес...']) !!}
                                    </div>
                                </div>
                                {!! Form::hidden('searchPlaceData','',['class' => 'searchPlaceData', 'v-model' => 'placeCity.text'])!!}
                                {!! Form::hidden('primary_place','',['class' => 'primary-place']) !!}
                                {!! Form::hidden('latitude','',['class' => 'latitude']) !!}
                                {!! Form::hidden('longitude','',['class' => 'longitude']) !!}
                            </div>
                            <div class="col-xs-6">
                                <div class="relative">
                                    <div class="text-center map-message" v-show="placeAddress">
                                        <i class="fa fa-forward"></i> Передвиньте маркер на карте в нужное место, если он расположен неверно.
                                    </div>
                                    <div class="map-hover" v-show="!placeAddress">
                                        <div class="map-hover-inner text-center">
                                            <i class="fa fa-home"></i><br>
                                            сначала укажите адрес
                                        </div>
                                    </div>
                                    <div id="ad-select-place-map" class="map-canvas" style="height: 240px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-20 select-place-block">
                    <div class="col-xs-12 relative">
                        <h2>Публикация объявления</h2>
                        <p class="margin-bottom-20">Выберите дополнительные опции публикации и укажите количество дней.</p>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="border-gray radius ad-publication-type"  v-bind:class="adTypeFields?'drop-shadow':''">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="border-gray ad-type-item">
                                                <div class="ad-type-item-header">
                                                    <span class="ad-type-item-header-icon"><img src="/images/publication_type_icon_1.png" width="45" height="45"></span><span class="ad-type-item-header-text">Выделить цветом</span>
                                                </div>
                                                <div class="ad-type-item-content gray-panel">
                                                    <ul class="fa-ul">
                                                        <li><i class="fa-li fa fa-angle-right"></i>Объявление выделено цветом.</li>
                                                        <li><i class="fa-li fa fa-angle-right"></i>Повышено количество просмотров объявления.</li>
                                                    </ul>
                                                </div>
                                                <div class="ad-type-item-slider">
                                                    <input class="data-slider" name="ad_promo[color_marker]" v-range="adPublicationColorMarker" type="text" value="0" data-slider-value="0" data-slider-ticks="[0, 1, 2, 3, 4, 5, 6, 7]" data-slider-ticks-snap-bounds="1" data-slider-ticks-labels='["0 дней", "", "", "", "", "", "", "7 дней"]' data-slider-tooltip="always"/>
                                                </div>
                                                <div class="ad-type-item-footer">
                                                    <div class="row">
                                                        <div class="col-xs-7">
                                                            <div class="ad-type-item-count">
                                                                За <input type="text" class="form-control" v-model="adPublicationColorMarker" disabled="disabled"> дней
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5">
                                                            <div class="ad-type-item-price">
                                                                <span class="ad-type-item-total" v-text="adPublicationColorMarker * {{App\AdPremiumRate::getRateByName('color_marker')->price}}"></span><i class="fa fa-rub"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="border-gray ad-type-item">
                                                <div class="ad-type-item-header">
                                                    <span class="ad-type-item-header-icon"><img src="/images/publication_type_icon_2.png" width="45" height="45"></span><span class="ad-type-item-header-text">Премиум размещение</span>
                                                </div>
                                                <div class="ad-type-item-content gray-panel">
                                                    <ul class="fa-ul">
                                                        <li><i class="fa-li fa fa-angle-right"></i>Премиум объявления отображаются в начале списка.</li>
                                                        <li><i class="fa-li fa fa-angle-right"></i>Повышено количество просмотров объявления.</li>
                                                    </ul>
                                                </div>
                                                <div class="ad-type-item-slider">
                                                    <input class="data-slider" name="ad_promo[publication_premium]" v-range="adPublicationPremium" type="text" value="0" data-slider-value="0" data-slider-ticks="[0, 1, 2, 3, 4, 5, 6, 7]" data-slider-ticks-snap-bounds="1" data-slider-ticks-labels='["0 дней", "", "", "", "", "", "", "7 дней"]' data-slider-tooltip="always"/>
                                                </div>
                                                <div class="ad-type-item-footer">
                                                    <div class="row">
                                                        <div class="col-xs-7">
                                                            <div class="ad-type-item-count">
                                                                За <input type="text" class="form-control" v-model="adPublicationPremium" disabled="disabled"> дней
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5">
                                                            <div class="ad-type-item-price">
                                                                <span class="ad-type-item-total" v-text="adPublicationPremium * {{App\AdPremiumRate::getRateByName('publication_premium')->price}}"></span><i class="fa fa-rub"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="border-gray ad-type-item">
                                                <div class="ad-type-item-header">
                                                    <span class="ad-type-item-header-icon"><img src="/images/publication_type_icon_3.png" width="45" height="45"></span><span class="ad-type-item-header-text">Поднять объявление</span>
                                                </div>
                                                <div class="ad-type-item-content gray-panel">
                                                    <ul class="fa-ul">
                                                        <li><i class="fa-li fa fa-angle-right"></i>Объявление автоматически попадает в начало списка раз в сутки.</li>
                                                        <li><i class="fa-li fa fa-angle-right"></i>Повышено количество просмотров объявления.</li>
                                                    </ul>
                                                </div>
                                                <div class="ad-type-item-slider">
                                                    <input class="data-slider" name="ad_promo[publication_up]" v-range="adPublicationUp" type="text" value="0" data-slider-value="0" data-slider-ticks="[0, 1, 2, 3, 4, 5, 6, 7]" data-slider-ticks-snap-bounds="1" data-slider-ticks-labels='["0 дней", "", "", "", "", "", "", "7 дней"]' data-slider-tooltip="always"/>
                                                </div>
                                                <div class="ad-type-item-footer">
                                                    <div class="row">
                                                        <div class="col-xs-7">
                                                            <div class="ad-type-item-count">
                                                                За <input type="text" class="form-control" v-model="adPublicationUp" disabled="disabled"> дней
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5">
                                                            <div class="ad-type-item-price">
                                                                <span class="ad-type-item-total" v-text="adPublicationUp * {{App\AdPremiumRate::getRateByName('publication_up')->price}}"></span><i class="fa fa-rub"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-top-50">
                    <div class="col-xs-12">
                        <div class="alert alert-danger text-center" v-if="adErrorsStatus == 403">
                            <p>Подтвердите адрес электронной почты или номер телефона в верхней части страницы, после этого вы сможете опубликовать объявление.</p>
                        </div>
                        <div class="alert alert-danger text-center" v-if="adErrors.length != 0">
                            <p>При заполнении формы объявления допущены ошибки, исправьте их, после этого вы сможете опубликовать объявление.</p>
                        </div>
                        <div class="alert alert-danger text-center" v-if="adPayError">
                            <p>@{{{adPayError}}}</p>
                        </div>
                    </div>
                    <div class="col-xs-3 col-xs-offset-3">
                        <button type="button" class="btn btn-primary btn-block" v-on:click="saveAd('{{route('postCreateAd')}}','just_save', $event)">сохранить черновик</button>
                    </div>
                    <div class="col-xs-3">
                        <button type="button" v-on:click="saveAd('{{route('postCreateAd')}}','publish', $event)" class="btn btn-success btn-block" v-text="(adPublicationColorMarker+adPublicationPremium+adPublicationUp>0)?'опубликовать за '+(adPublicationColorMarker*{{App\AdPremiumRate::getRateByName('color_marker')->price}}+adPublicationPremium*{{App\AdPremiumRate::getRateByName('publication_premium')->price}}+adPublicationUp*{{App\AdPremiumRate::getRateByName('publication_up')->price}})+' руб.':'опубликовать бесплатно'"></button>
                    </div>
                </div>
                <div class="row margin-top-20">
                    <div class="col-xs-4 col-xs-offset-4">
                        <button type="button" class="btn btn-link btn-block"><i class="fa fa-times"></i> удалить объявление</button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    {!! Captcha::scriptWithCallback(['captcha1', 'captcha2']) !!}
    <div class="margin-top-20"></div>
@endsection