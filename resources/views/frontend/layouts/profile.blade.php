@extends('frontend.layouts.main')

@section('breadcrumbs')
    {!! Breadcrumbs::render('getProfile') !!}
@endsection

@section('title')
    Личный кабинет
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 margin-bottom-30">
            <ul class="nav nav-pills sub-nav">
                <li role="presentation"><a href="{{ route('getProfile') }}"><i class="fa fa-user"></i> Личный кабинет</a></li>
                <li role="presentation"><a href="{{ route('getProfileAds') }}"><i class="fa fa-bullhorn"></i> Мои объявления</a></li>
                <li role="presentation"><a href="{{ route('getProfileBookmarks') }}"><i class="fa fa-bookmark"></i> Мои закладки и сохраненный поиск</a></li>
                <li role="presentation"><a href="#" data-toggle="modal" data-target="#paymentModal"><i class="fa fa-money"></i> Пополнить баланс</a></li>
                <li role="presentation"><a href="{{route('getProfileSettings')}}"><i class="fa fa-cog"></i> Настройки</a></li>
                <li role="presentation"><a href="{{route('getLogOut')}}"><i class="fa fa-sign-out"></i> Выйти</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-8">
            <div class="row">
                <div class="col-xs-12 page-messages">
                    {!! Notification::showAll() !!}
                    @if($errors->all())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                {{$error}}<br>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            @yield('profile_content')
        </div>
        <div class="col-xs-4">
            <div class="margin-bottom-30 profile-money">
                <div class="bg-info-light-2 profile-money-title">
                    Ваш баланс
                </div>
                <div class="bg-primary profile-money-count">
                    <span class="count">{{Auth::user()->money}}</span> <i class="fa fa-rub"></i>
                </div>
                <div class="bg-info-light-2 profile-money-add">
                    <div class="profile-money-add-title">
                        Пополнение баланса:
                    </div>
                    <div class="profile-money-add-form">
                        На сумму
                        {!! Form::text('money', null, ['class' => 'form-control', 'v-model' => 'paymentSum']) !!}
                        руб.
                        <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#paymentModal">Пополнить</button>
                    </div>
                </div>
            </div>
            @if(\Request::route()->getName() !== 'getProfileAgencySettings')
                <div class="margin-bottom-30 profile-add-agency bg-info-light">
                    @include('frontend.parts.promo_create_agency')
                </div>
            @endif
        </div>
    </div>
    @include('frontend.parts.modal',[
                    'id' => 'paymentModal',
                    'title' => 'Пополнить баланс',
                    'content' => view()->make('frontend.profile.parts.payment'),
        ])
    @include('frontend.parts.modal',[
                  'id' => 'deleteBookmarkModal',
                  'title' => 'Удалить закладку',
                  'content' => 'Вы действительно хотите удалить эту закладку?',
                  'haveCancel' => true,
                  'buttons' => [
                    '<a href="#" class="btn btn-danger modal-url-btn">Удалить</a>'
                  ]
        ])
@endsection