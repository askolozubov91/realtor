<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $pageTitle }}</title>
    <meta name="description" content="{{ $pageDescription }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    {!! Html::style(elixir("css/frontend.css")) !!}
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    {!! Html::script(elixir("js/frontend.js")) !!}
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYg6N_Li2uIHEEGh80I7eXZ1Blmnou4_4&libraries=places&callback=initialize_maps">
    </script>
</head>
<body>
<div class="content-wrapper margin-bottom-30">
    <div class="container-fluid header-top-nav">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation"><a href="{{ route('getProfileBookmarks') }}"><i class="fa fa-bookmark"></i> Закладки</a></li>
                        <li role="presentation"><a href="{{route('getProfile')}}"><i class="fa fa-sign-in"></i> Личный кабинет</a></li>
                        <li role="presentation">
                            <a href="#"
                               @if($globalCity->type->name == 'city')
                                 v-on:click="getCities('{{route('getPlacesByParentId')}}',{{$globalCity->parent->id}},'{{$globalCity->parent->display_name}}', '{{$globalCity->parent->name}}')"
                               @else
                               v-on:click="getCities('{{route('getPlacesByParentId')}}',{{$globalCity->id}},'{{$globalCity->display_name}}', '{{$globalCity->name}}')"
                               @endif
                               class="bg-success"
                               data-toggle="modal"
                               data-target="#citySelectModal"><i class="fa fa-map-marker"></i> {{$globalCity->display_name}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-primary header-nav">
        <div class="container">
            <div class="row">
                <div class="col-xs-4">
                    <a href="/"><img src="/images/logo.png" alt="domberem.ru" width="238" height="40"></a>
                </div>
                <div class="col-xs-8">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation"><a href="#">Главная</a></li>
                        <li role="presentation"><a href="{{route('getCatalog','real_estate')}}">Объявления</a></li>
                        <li role="presentation"><a href="{{route('getAgenciesCatalog')}}">Агентства</a></li>
                        <li role="presentation"><a href="#">Статьи</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid header-page-title">
        <div class="container">
            <div class="row">
                <div class="col-xs-9">
                    @yield('breadcrumbs')
                    <h1>@yield('title')</h1>
                </div>
                <div class="col-xs-3">
                    @if(Request::route()->getName() != 'getCreateAd')
                        <div class="btn-split">
                            <a href="{{ route('getCreateAd') }}" class="btn btn-success btn-block"><i class="fa fa-plus-circle"></i> Добавить объявление</a>
                            <span class="btn-split-help">Это бесплатно быстро и удобно</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        @yield('content')
    </div>
</div>
<div class="container-fluid footer-wrapper">
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <h4>Аренда недвижимости</h4>
                    <ul>
                        <li><a href="#">Аренда квартир - Хабаровск</a></li>
                        <li><a href="#">Аренда комнат - Хабаровск</a></li>
                        <li><a href="#">Аренда домов, коттеджей, дач - Хабаровск</a></li>
                        <li><a href="#">Аренда офисов, коммерческой недвижимости - Хабаровск</a></li>
                        <li><a href="#">Аренда Земельных участков - Хабаровск</a></li>
                        <li><a href="#">Аренда гаражей и парковочных мест - Хабаровск</a></li>
                    </ul>
                </div>
                <div class="col-xs-3">
                    <h4>Продажа недвижимости</h4>
                    <ul>
                        <li><a href="#">Продажа квартир - Хабаровск</a></li>
                        <li><a href="#">Продажа комнат - Хабаровск</a></li>
                        <li><a href="#">Продажа домов, коттеджей, дач - Хабаровск</a></li>
                        <li><a href="#">Продажа офисов, коммерческой недвижимости - Хабаровск</a></li>
                        <li><a href="#">Продажа Земельных участков - Хабаровск</a></li>
                        <li><a href="#">Продажа гаражей и парковочных мест - Хабаровск</a></li>
                    </ul>
                </div>
                <div class="col-xs-3">
                    <h4>Услуги</h4>
                    <ul>
                        <li><a href="#">Все услуги</a></li>
                        <li><a href="#">Продажа комнат - Хабаровск</a></li>
                        <li><a href="#">Ведение сделок с недвижимостью - Хабаровск</a></li>
                        <li><a href="#">Государственная регистрация недвижимости - Хабаровск</a></li>
                        <li><a href="#">Приватизация недвижимости - Хабаровск</a></li>
                        <li><a href="#">Ипотека - Хабаровск</a></li>
                    </ul>
                </div>
                <div class="col-xs-3">
                    <h4>Прочее</h4>
                    <ul>
                        <li><a href="#">Агентства недвижимости - Хабаровск</a></li>
                        <li><a href="#">Статьи о недвижимости</a></li>
                        <li><a href="#">Пользовательское соглашение</a></li>
                        <li><a href="#">Рекламодателям</a></li>
                        <li><a href="#">Помощь</a></li>
                        <li><a href="#">О сайте</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <p><a href="#">Domberem.com</a>. Все права защищены. <a href="#">Публичная оферта</a>.</p>
                </div>
                <div class="col-xs-6">
                    <div class="text-right">
                        Подпишитесь на наши сообщества:
                        <ul class="social-icons">
                            <li><a href="#" style="background-color: #4c75a3;"><i class="fa fa-vk"></i></a></li>
                            <li><a href="#" style="background-color: #41abe1;"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" style="background-color: #3c5899;"><i class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hidden"
     id="vue-urls"
     data-registration-url="{{route('postSignUp')}}"
     data-auth-url="{{route('postLogIn')}}"
     data-confirm-contacts-url="{{route('postConfirmContacts')}}"
     data-send-confirm-email-url="{{route('getSendConfirmEmail')}}"
     data-send-confirm-phone-url="{{route('getSendConfirmPhone')}}"
     data-get-auth-user-url="{{route('getAuthUser')}}"
     >
</div>
@include('frontend.parts.modal',[
                    'id' => 'paymentModal',
                    'title' => 'Пополнить баланс',
                    'content' => view()->make('frontend.profile.parts.payment'),
        ])
@include('frontend.parts.modal',[
   'id' => 'citySelectModal',
   'title' => 'Выбор города',
   'content' => view()->make('frontend.parts.select_city_modal'),
])
</body>