@extends('frontend.layouts.main')

@section('breadcrumbs')
    {!! Breadcrumbs::render('getHome') !!}
@endsection

@section('title')
    {{$title}}
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-offset-3 col-xs-6 margin-top-50 margin-bottom-50">
            <div class="info-message">
                <p class="text-center">{{$message}}</p>
                <div class="row">
                    <div class="col-xs-6 margin-top-50">
                        <a href="{{route('getHome')}}" class="btn btn-success btn-block">На главную</a>
                    </div>
                    <div class="col-xs-6 margin-top-50">
                        <a href="{{route('getProfile')}}" class="btn btn-success btn-block">В личный кабинет</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection