<div class="profile-add-agency-title">
    <i class="fa fa-building-o profile-add-agency-title-icon"></i>
    <span>Создайте агентство недвижимости в каталоге</span>
</div>
<div class="profile-add-agency-content">
    <ul class="fa-ul">
        <li><i class="fa-li fa  fa-angle-right"></i> Подробная информация о вашей компании в каталоге.</li>
        <li><i class="fa-li fa  fa-angle-right"></i> Персональная страница агентства.</li>
        <li><i class="fa-li fa  fa-angle-right"></i> Разные контактные данные для разных объявлений.</li>
    </ul>
</div>
<div class="profile-add-agency-button">
    <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#addAgencyModal">Создать агентство</button>
</div>
@include('frontend.parts.modal',[
          'id' => 'addAgencyModal',
          'title' => 'Создание агентства',
          'content' => view()->make('frontend.agencies.parts.edit_agency')
])