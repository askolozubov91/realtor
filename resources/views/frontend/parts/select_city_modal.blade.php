<div class="row margin-top-20 relative">
    <div class="loader" v-if="showGlobalCityLoader"></div>
    <div class="col-xs-6">
        <div class="city-select-block border-gray radius">
            <h2>Россия</h2>
            @foreach($regions as $groupKey => $region)
                <p class="city-select-marker"><span>{{$groupKey}}</span></p>
                <ul class="city-select-list">
                    @foreach($region as $district)
                        <li><a href="#" v-on:click="getCities('{{route('getPlacesByParentId')}}',{{$district->id}}, '{{$district->display_name}}', '{{$district->name}}')" v-bind:class="[(globalRegionId == {{$district->id}})?'active':'']" >{{$district->display_name}}</a></li>
                    @endforeach
                </ul>
            @endforeach
        </div>
    </div>
    <div class="col-xs-6">
        <div class="city-select-block border-gray radius">
            <h3>@{{globalRegionName}}</h3>
            <ul class="city-select-list">
                <li><a href="http://@{{ globalRegionSlug }}.{{Config::get('session.domain')}}/{{Request::path()}}" v-bind:class="[(globalRegionId == {{$globalCity->id}})?'active':'']">Весь регион</a></li>
                <li><hr></li>
                <li v-for="city in globalCities"><a href="http://@{{ city.name }}.{{Config::get('session.domain')}}/{{Request::path()}}" v-bind:class="[(city.id == {{$globalCity->id}})?'active':'']" v-text="city.display_name"></a></li>
            </ul>
        </div>
    </div>
</div>