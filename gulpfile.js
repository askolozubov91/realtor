var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
 mix.less("frontend/frontend.less");
 mix.less("backend/backend.less");
 mix.scripts(
     [
      '../vendor/jquery/jquery.js',
      '../vendor/datatables/datatables.js',
      '../vendor/bootstrap/js/bootstrap.js',
      '../vendor/bootstrap-select/js/bootstrap-select.js',
      '../vendor/bootstrap-ajax-select/js/ajax-bootstrap-select.min.js',
      '../vendor/bootstrap-ajax-select/js/ajax-bootstrap-select.ru.js',
      '../vendor/file-upload/js/jquery.ui.widget.js',
      '../vendor/file-upload/js/jquery.iframe-transport.js',
      '../vendor/file-upload/js/jquery.fileupload.js',
      '../vendor/moments/moments.js',
      '../vendor/project/dateformat/dateformat.js',
      '../vendor/project/datatables/render-datatables.js',
      '../vendor/project/maps/maps.js',
      '../vendor/froala/js/froala_editor.min.js',
      '../vendor/froala/js/plugins/align.min.js',
      '../vendor/froala/js/plugins/font_family.min.js',
      '../vendor/froala/js/plugins/lists.min.js',
      '../vendor/froala/js/plugins/char_counter.min.js',
      '../vendor/froala/js/plugins/font_size.min.js',
      '../vendor/froala/js/plugins/paragraph_format.min.js',
      '../vendor/froala/js/plugins/code_beautifier.min.js',
      '../vendor/froala/js/plugins/fullscreen.min.js',
      '../vendor/froala/js/plugins/paragraph_style.min.js',
      '../vendor/froala/js/plugins/code_view.min.js',
      '../vendor/froala/js/plugins/image.min.js',
      '../vendor/froala/js/plugins/image_manager.min.js',
      '../vendor/froala/js/plugins/quote.min.js',
      '../vendor/froala/js/plugins/colors.min.js',
      '../vendor/froala/js/plugins/save.min.js',
      '../vendor/froala/js/plugins/emoticons.min.js',
      '../vendor/froala/js/plugins/inline_style.min.js',
      '../vendor/froala/js/plugins/table.min.js',
      '../vendor/froala/js/plugins/entities.min.js',
      '../vendor/froala/js/plugins/line_breaker.min.js',
      '../vendor/froala/js/plugins/url.min.js',
      '../vendor/froala/js/plugins/file.min.js',
      '../vendor/froala/js/plugins/link.min.js',
      '../vendor/froala/js/plugins/video.min.js',
      '../vendor/froala/js/languages/ru.js',
      'backend/backend.js'
     ],
     'public/js/backend.js'
 );
 mix.scripts(
     [
      '../vendor/jquery/jquery.js',
      '../vendor/project/maps/maps.js',
      '../vendor/moments/moments.js',
      '../vendor/mask/jquery.mask.min.js',
      '../vendor/bootstrap/js/bootstrap.js',
      '../vendor/bootstrap-select/js/bootstrap-select.js',
      '../vendor/bootstrap-slider/bootstrap-slider.min.js',
      '../vendor/file-upload/js/jquery.ui.widget.js',
      '../vendor/file-upload/js/jquery.iframe-transport.js',
      '../vendor/file-upload/js/jquery.fileupload.js',
      '../vendor/project/dateformat/dateformat.js',
      '../vendor/vue/vue.min.js',
      '../vendor/project/vue/vue_init.js',
      'frontend/frontend.js'
     ],
     'public/js/frontend.js'
 );

 mix.copy('resources/assets/vendor/fontawesome/fonts', 'public/fonts/fontawesome');
 mix.copy('resources/assets/vendor/bootstrap/fonts', 'public/fonts/bootstrap');

 mix.version([
     "js/backend.js",
     "js/frontend.js",
     "css/backend.css",
     "css/frontend.css"
 ]);
});
