<?php

use App\Helpers\ClearData;

class ClearDataTest extends TestCase{


    public function testClearPhone(){
        $data = new ClearData();
        $this->assertEquals('79098231010',$data->clearPhone('79098231010'));
    }

    public function testClearPhone2(){
        $data = new ClearData();
        $this->assertEquals('79098231010',$data->clearPhone('7 (909) 823 - 10 - 10'));
    }

    public function testClearPhone3(){
        $data = new ClearData();
        $this->assertEquals('79098231010',$data->clearPhone('8 (909) 823 - 10 - 10'));
    }

    public function testClearPhone4(){
        $data = new ClearData();
        $this->assertEquals(false,$data->clearPhone('123'));
    }


}