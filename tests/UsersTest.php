<?php

use App\User;

class UsersTest extends TestCase{

    public function testCreateUser(){
        User::createUser([
            'name'          => 'Foo Bar',
            'company_name'  => 'FOOBAR LTD',
            'user_type'     =>  1,
            'email'         => 'foo@bar.com',
            'phone'         => '79241112233',
            'password'      => 'password'
        ], 'owner');
        $this->seeInDatabase('users', ['email' => 'foo@bar.com']);
    }

}