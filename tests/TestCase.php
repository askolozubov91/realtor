<?php

use Illuminate\Support\Facades\Artisan;

class TestCase extends Illuminate\Foundation\Testing\TestCase
{

    public static $setupDatabase = true;

    public function setUp()
    {
        parent::setUp();
        if(self::$setupDatabase)
        {
            $this->setupDatabase();
        }
    }

    public function setupDatabase()
    {
        Artisan::call('migrate:refresh');
        Artisan::call('db:seed');
        self::$setupDatabase = false;
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }
}
