<?php

class AuthTest extends TestCase{


    public function testGetLogin(){
        $this->call('GET', route('getLogIn'));
        $this->assertResponseOk();
    }

    public function testGetLoginGetIfAuth(){
        $user = factory('App\User')->create(['phone'=>'7909'.rand(1000000,9999999), 'user_type' => 1]);
        $this->actingAs($user)->call('GET', route('getLogIn'));
        $this->assertRedirectedToRoute('getProfile');
    }

    public function testPostLoginByEmailWithTrueData(){
        $this->call('POST', route('postLogIn'));
        $this->assertResponseStatus(302);
    }

    public function testGetSignUp(){
        $this->call('GET', route('getSignUp'));
        $this->assertResponseOk();
    }

    public function testGetSignUpIfAuth(){
        $user = factory('App\User')->create(['phone'=>'7909'.rand(1000000,9999999), 'user_type' => 1]);
        $this->actingAs($user)->call('GET', route('getSignUp'));
        $this->assertRedirectedToRoute('getProfile');
    }

    public function testGetLogout(){
        $this->call('GET', route('getLogOut'));
        $this->assertRedirectedToRoute('getLogIn');
    }

    public function testGetLogoutIfAuth(){
        $user = factory('App\User')->create(['phone'=>'7909'.rand(1000000,9999999), 'user_type' => 1]);
        $this->actingAs($user)->call('GET', route('getLogOut'));
        $this->assertRedirectedToRoute('getHome');
    }

}