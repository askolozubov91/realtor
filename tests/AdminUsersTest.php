<?php

use App\User;

class AdminUsersTest extends TestCase{

    public function testGetAdminUsers(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
            ->visit(route('getAdminUsers'))
            ->see('Пользователи');
    }

    public function testGetAdminDeletedUsers(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
            ->visit(route('getAdminUsersDeleted'))
            ->see('Удаленные пользователи');
    }

    public function testGetAdminUser(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
            ->visit(route('getAdminUser', $user->id))
            ->see('Пользователь №1');
    }

    public function testGetAdminUserForm(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
            ->visit(route('getAdminUser', $user->id))
            ->type('Иванов Иван Иванович', 'name')
            ->press('Сохранить')
            ->seePageIs(route('getAdminUser', $user->id))
            ->see('Иванов Иван Иванович');
    }

    public function testGetAdminUserFormRolesLevel(){
        $user = User::findOrFail(2);
        $response = $this->actingAs($user)
                         ->call('POST', route('postAdminUpdateUser',1), ['name' => 'Иванов Иван Иванович']);
        $this->assertEquals(403, $response->status());
    }

    public function testPostAdminUserActionDelete(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
             ->call('POST', route('postAdminUsersSelectedAction'), ['action' => 'delete', 'actionId' => [2]]);
        $deleted = User::onlyTrashed()->where('id',2)->first();
        $this->assertEquals(2, $deleted->id);
    }

    public function testPostAdminUserActionRestore(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
            ->call('POST', route('postAdminUsersSelectedAction'), ['action' => 'restore', 'actionId' => [2]]);
        $restored = User::findOrFail(2);
        $this->assertEquals(2, $restored->id);
    }

    public function testPostAdminUserActionBlock(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
            ->call('POST', route('postAdminUsersSelectedAction'), ['action' => 'block', 'actionId' => [2]]);
        $user2 = User::with('roles')->findOrFail(2);
        $this->assertEquals('locked', $user2->roles[0]['name']);
    }

    public function testPostAdminUserActionUnBlock(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
            ->call('POST', route('postAdminUsersSelectedAction'), ['action' => 'unblock', 'actionId' => [2]]);
        $user2 = User::with('roles')->findOrFail(2);
        $this->assertEquals('user', $user2->roles[0]['name']);
    }

    public function testPostAdminUserActionDeleteForever(){
        $user = User::findOrFail(1);
        User::destroy(2);
        $this->actingAs($user)
            ->call('POST', route('postAdminUsersSelectedAction'), ['action' => 'deleteForever', 'actionId' => [2]]);
        $this->assertEquals(false, User::where('id',2)->exists());
    }

    public function testGetAdminUserDelete(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
            ->visit(route('getAdminDeleteUser', 3))
            ->seePageIs(route('getAdminUsers'))
            ->see('Пользователь №3 успешно удален.');
    }

    public function testGetAdminUserDeleteForever(){
        $user = User::findOrFail(1);
        $this->actingAs($user)
            ->visit(route('getAdminDeleteUserForever', 3))
            ->seePageIs(route('getAdminUsersDeleted'))
            ->see('Пользователь №3 успешно удален навсегда.');
    }

    public function testGetAdminUserRestore(){
        $user = User::findOrFail(1);
        User::destroy(4);
        $this->actingAs($user)
            ->visit(route('getAdminRestoreUser', 4))
            ->seePageIs(route('getAdminUser',4))
            ->see('Пользователь №4 восстановлен.');
    }

}